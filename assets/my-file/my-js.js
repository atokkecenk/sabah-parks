// Display the countdown timer in an element
// Set a valid end date
// var countDownDate = new Date("May 4, 2022 23:59:00").getTime();

// // Update the count down every 1 second
// var x = setInterval(function () {
// 	// Get today's date and time
// 	var now = new Date().getTime();

// 	// Find the distance between now and the countdown date
// 	var distance = countDownDate - now;

// 	// Calculate Remaining Time
// 	var days = Math.floor(distance / (1000 * 60 * 60 * 24));
// 	var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
// 	var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
// 	var seconds = Math.floor((distance % (1000 * 60)) / 1000);

// 	// Display the result in the element with id="demo"
// 	document.getElementById("deadline").innerHTML =
// 		"<div class='time'><span class='square'>" +
// 		days +
// 		"</span><span class='caption'>DAYS</span></div> : <div class='time'><span class='square'>" +
// 		hours +
// 		"</span><span class='caption'>HOURS</span></div> : <div class='time'><span class='square'>" +
// 		minutes +
// 		"</span><span class='caption'>MINUTES</span></div> : <div class='time'><span class='square'>" +
// 		seconds +
// 		"</span><span class='caption'>SECOND</span></div>";

// 	// If the countdown is finished, write some text
// 	if (distance < 0) {
// 		clearInterval(x);
// 		document.getElementById("deadline").innerHTML = "EXPIRED";
// 	}
// }, 1000);

$(function () {
	$('[data-toggle="tooltip"]').tooltip();
});

$(document).ready(function () {
	console.log("Developtment by Firdaus Zulkarnain (atokkecenk@gmail.com)");
	// active menu
	var id_menu = $('[name="id_menu"]').val();
	var id_root = $('[name="id_root"]').val();
	if (parseInt(id_menu) !== 0) {
		$(".mn" + id_root).addClass("active");
		$("." + id_menu).addClass("active");
		$(".ul" + id_root)
			.removeClass("collapse submenu list-unstyled")
			.addClass(
				"collapse submenu recent-submenu mini-recent-submenu list-unstyled show"
			);
	}

	$(".alert")
		.fadeTo(2000, 600)
		.slideUp(950, function () {
			$(".alert").slideUp(950);
		});

	var year = $('[name="year"]').val();
	var dtg = $('[name="date_togo"]').val();
	var date_togo = dtg == "" ? 0 : dtg;

	$("#tbMember").DataTable({
		responsive: true,
		autoWidth: false,
		processing: true,
		serverSide: true,
		ordering: true,
		// sorting: [[0, "DESC"]],
		ajax: {
			url: "get-datamember",
			type: "POST",
		},
		columnDefs: [
			{
				targets: [0, 6],
				className: "text-center",
			},
			{
				orderable: false,
				targets: [6],
			},
		],
		language: {
			zeroRecords: "No Data Found !!",
		},
		oLanguage: {
			oPaginate: {
				sPrevious:
					'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
				sNext:
					'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
			},
			sInfo: "Showing page _PAGE_ of _PAGES_",
			sSearch:
				'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
			sSearchPlaceholder: "Search...",
			sLengthMenu: "Results :  _MENU_",
		},
		stripeClasses: [],
		lengthMenu: [10, 25, 50],
		pageLength: 10,
	});

	// REPORT
	$("#tbReportMember").DataTable({
		responsive: true,
		autoWidth: false,
		processing: true,
		serverSide: true,
		ordering: true,
		// sorting: [[1, "DESC"]],
		ajax: {
			url: "rpt-data-member",
			type: "POST",
		},
		columnDefs: [
			{
				targets: [0],
				className: "text-center",
			},
		],
		language: {
			zeroRecords: "No Data Found !!",
		},
		oLanguage: {
			oPaginate: {
				sPrevious:
					'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
				sNext:
					'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
			},
			sInfo: "Showing page _PAGE_ of _PAGES_",
			sSearch:
				'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
			sSearchPlaceholder: "Search...",
			sLengthMenu: "Results :  _MENU_",
		},
		stripeClasses: [],
		lengthMenu: [10, 25, 50],
		pageLength: 10,
	});

	var tbOrder = $(".tbOrder");
	if (tbOrder.length == "1") {
		$(".tbOrder").attr("id", "tbOrder");
		if (year != "0" || date_togo != "0") {
			$("#tbOrder").DataTable({
				responsive: true,
				autoWidth: true,
				processing: true,
				serverSide: true,
				ordering: true,
				// sorting: [[0, "DESC"]],
				ajax: {
					url: "get-dataorder/" + year + "/" + date_togo,
					type: "POST",
				},
				columnDefs: [
					{
						targets: [0, 1, 2, 3, 5, 6, 9],
						className: "text-center",
					},
					{
						orderable: false,
						targets: [1, 9],
					},
				],
				language: {
					zeroRecords: "No Data Found !!",
				},
				oLanguage: {
					oPaginate: {
						sPrevious:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
						sNext:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
					},
					sInfo: "Showing page _PAGE_ of _PAGES_",
					sSearch:
						'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
					sSearchPlaceholder: "Search...",
					sLengthMenu: "Results :  _MENU_",
				},
				stripeClasses: [],
				lengthMenu: [10, 25, 50],
				pageLength: 10,
			});
		} else {
			$(".tbOrder tbody").html(
				'<tr><td colspan="10" align="center" class="text-danger">Set filter to show data..</td></tr>'
			);
		}
	}

	// REPORT
	var tbReportOrder = $(".tbReportOrder");
	if (tbReportOrder.length == "1") {
		$(".tbReportOrder").attr("id", "tbReportOrder");
		if (date_togo != "0") {
			$("#tbReportOrder").DataTable({
				responsive: true,
				autoWidth: true,
				processing: true,
				serverSide: true,
				ordering: true,
				// sorting: [[1, "DESC"]],
				ajax: {
					url: "rpt-data-booking/" + date_togo,
					type: "POST",
				},
				columnDefs: [
					{
						targets: [0, 2, 4, 5, 8],
						className: "text-center",
					},
				],
				language: {
					zeroRecords: "No Data Found !!",
				},
				oLanguage: {
					oPaginate: {
						sPrevious:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
						sNext:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
					},
					sInfo: "Showing page _PAGE_ of _PAGES_",
					sSearch:
						'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
					sSearchPlaceholder: "Search...",
					sLengthMenu: "Results :  _MENU_",
				},
				stripeClasses: [],
				lengthMenu: [10, 25, 50],
				pageLength: 10,
			});
		} else {
			$(".tbReportOrder tbody").html(
				'<tr><td colspan="9" align="center" class="text-danger">Set filter to show data..</td></tr>'
			);
		}
	}

	$(document).on("click", "#reportOrder", function (e) {
		e.preventDefault();
		var dtg = $('[name="date_togo"]').val();
		if (dtg !== "") {
			window.open("excel-order/" + dtg, "_blank");
		} else {
			alert("Please select date range !!");
		}
	});

	// REPORT
	var opt = $('[name="option"]').val();
	var option = opt == "" ? 0 : opt;
	var tbReportPaid = $(".tbReportPaid");
	if (tbReportPaid.length == "1") {
		$(".tbReportPaid").attr("id", "tbReportPaid");
		if (option != "0" || date_togo != "0") {
			$("#tbReportPaid").DataTable({
				responsive: true,
				autoWidth: true,
				processing: true,
				serverSide: true,
				sorting: [[1, "DESC"]],
				ajax: {
					url: "rpt-data-paid/" + option + "/" + date_togo,
					type: "POST",
				},
				columnDefs: [
					{
						targets: [0, 2, 4, 5, 8],
						className: "text-center",
					},
				],
				language: {
					zeroRecords: "No Data Found !!",
				},
				oLanguage: {
					oPaginate: {
						sPrevious:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
						sNext:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
					},
					sInfo: "Showing page _PAGE_ of _PAGES_",
					sSearch:
						'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
					sSearchPlaceholder: "Search...",
					sLengthMenu: "Results :  _MENU_",
				},
				stripeClasses: [],
				lengthMenu: [10, 25, 50],
				pageLength: 10,
			});
		} else {
			$(".tbReportPaid tbody").html(
				'<tr><td colspan="9" align="center" class="text-danger">Set filter to show data..</td></tr>'
			);
		}
	}

	$(document).on("click", "#reportPaid", function (e) {
		e.preventDefault();
		// var dtg = $('[name="date_togo"]').val();
		if (date_togo == "" && option == "") {
			alert("Please select option or date range !!");
		} else {
			window.open("excel-paid/" + option + "/" + date_togo, "_blank");
		}
	});

	var tbHoldAppv = $(".tbHoldAppv");
	if (tbHoldAppv.length == "1") {
		$(".tbHoldAppv").attr("id", "tbHoldAppv");
		if (year != "0" || date_togo != "0") {
			$("#tbHoldAppv").DataTable({
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				ordering: true,
				// sorting: [[5, "DESC"]],
				ajax: {
					url: "get-dataholder-appv/" + year + "/" + date_togo,
					type: "POST",
				},
				columnDefs: [
					{
						targets: [0, 1, 3, 5, 6],
						className: "text-center",
					},
				],
				language: {
					zeroRecords: "No Data Found !!",
					processing: "Please wait, get data from server..",
				},
				oLanguage: {
					oPaginate: {
						sPrevious:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
						sNext:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
					},
					sInfo: "Showing page _PAGE_ of _PAGES_",
					sSearch:
						'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
					sSearchPlaceholder: "Search...",
					sLengthMenu: "Results :  _MENU_",
				},
				stripeClasses: [],
				lengthMenu: [10, 25, 50],
				pageLength: 10,
			});
		} else {
			$(".tbHoldAppv tbody").html(
				'<tr><td colspan="9" align="center" class="text-danger">Set filter to show data..</td></tr>'
			);
		}
	}

	// REPORT
	var tbReportHoldAppv = $(".tbReportHoldAppv");
	if (tbReportHoldAppv.length == "1") {
		$(".tbReportHoldAppv").attr("id", "tbReportHoldAppv");
		if (date_togo != "0") {
			$("#tbReportHoldAppv").DataTable({
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				ordering: true,
				// sorting: [[5, "DESC"]],
				ajax: {
					url: "rpt-data-pr-holder/" + date_togo,
					type: "POST",
				},
				columnDefs: [
					{
						targets: [0, 2, 4, 5, 8],
						className: "text-center",
					},
				],
				language: {
					zeroRecords: "No Data Found !!",
					processing: "Please wait, get data from server..",
				},
				oLanguage: {
					oPaginate: {
						sPrevious:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
						sNext:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
					},
					sInfo: "Showing page _PAGE_ of _PAGES_",
					sSearch:
						'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
					sSearchPlaceholder: "Search...",
					sLengthMenu: "Results :  _MENU_",
				},
				stripeClasses: [],
				lengthMenu: [10, 25, 50],
				pageLength: 10,
			});
		} else {
			$(".tbReportHoldAppv tbody").html(
				'<tr><td colspan="9" align="center" class="text-danger">Set filter to show data..</td></tr>'
			);
		}
	}

	$(document).on("click", "#reportBooking", function (e) {
		e.preventDefault();
		var dtg = $('[name="date_togo"]').val();
		if (dtg !== "") {
			window.open("excel-booking/" + dtg, "_blank");
		} else {
			alert("Please select date range !!");
		}
	});

	var tbBilling = $(".tbBilling");
	if (tbBilling.length == "1") {
		var dest = $("#selectDestBilling").val();
		var status = $("#selectStatusBilling").val();
		$(".tbBilling").attr("id", "tbBilling");
		if (year != "0" || date_togo != "0") {
			$("#tbBilling").DataTable({
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				ordering: true,
				// sorting: [[0, "DESC"]],
				ajax: {
					url:
						"get-databilling/" +
						year +
						"/" +
						date_togo +
						"/" +
						dest +
						"/" +
						status,
					type: "POST",
				},
				columnDefs: [
					{
						targets: [0, 1, 3, 6, 12, 15],
						className: "text-center",
					},
					{
						orderable: false,
						targets: [1, 15],
					},
				],
				language: {
					zeroRecords: "No Data Found !!",
					processing: "Please wait, get data from server..",
				},
				oLanguage: {
					oPaginate: {
						sPrevious:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
						sNext:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
					},
					sInfo: "Showing page _PAGE_ of _PAGES_",
					sSearch:
						'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
					sSearchPlaceholder: "Search...",
					sLengthMenu: "Results :  _MENU_",
				},
				stripeClasses: [],
				lengthMenu: [10, 25, 50],
				pageLength: 10,
			});
		} else {
			$(".tbBilling tbody").html(
				'<tr><td colspan="15" align="center" class="text-danger">Set filter to show data..</td></tr>'
			);
		}
	}

	// REPORT
	var tbReportBilling = $(".tbReportBilling");
	if (tbReportBilling.length == "1") {
		$(".tbReportBilling").attr("id", "tbReportBilling");
		if (date_togo != "0") {
			$("#tbReportBilling").DataTable({
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				ordering: true,
				// sorting: [[1, "DESC"]],
				ajax: {
					url: "rpt-data-billing/" + date_togo,
					type: "POST",
				},
				columnDefs: [
					{
						targets: [0, 2, 10, 12, 13],
						className: "text-center",
					},
					{
						orderable: false,
						targets: [1, 13],
					},
				],
				language: {
					zeroRecords: "No Data Found !!",
					processing: "Please wait, get data from server..",
				},
				oLanguage: {
					oPaginate: {
						sPrevious:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
						sNext:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
					},
					sInfo: "Showing page _PAGE_ of _PAGES_",
					sSearch:
						'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
					sSearchPlaceholder: "Search...",
					sLengthMenu: "Results :  _MENU_",
				},
				stripeClasses: [],
				lengthMenu: [10, 25, 50],
				pageLength: 10,
			});
		} else {
			$(".tbReportBilling tbody").html(
				'<tr><td colspan="14" align="center" class="text-danger">Set filter to show data..</td></tr>'
			);
		}
	}

	$(document).on("click", "#reportBilling", function (e) {
		e.preventDefault();
		var dtg = $('[name="date_togo"]').val();
		if (dtg !== "") {
			window.open("excel-billing/" + dtg, "_blank");
		} else {
			alert("Please select date range !!");
		}
	});

	// REPORT SALES
	var tbReportSales = $(".tbReportSales");
	if (tbReportSales.length == "1") {
		$(".tbReportSales").attr("id", "tbReportSales");
		if (date_togo != "0") {
			$("#tbReportSales").DataTable({
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				ordering: true,
				// sorting: [[1, "DESC"]],
				ajax: {
					url: "rpt-data-sales/" + date_togo,
					type: "POST",
				},
				columnDefs: [
					{
						targets: [0],
						className: "text-center",
					},
					{
						targets: [7],
						className: "text-right",
					},
				],
				language: {
					zeroRecords: "No Data Found !!",
					processing: "Please wait, get data from server..",
				},
				oLanguage: {
					oPaginate: {
						sPrevious:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
						sNext:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
					},
					sInfo: "Showing page _PAGE_ of _PAGES_",
					sSearch:
						'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
					sSearchPlaceholder: "Search...",
					sLengthMenu: "Results :  _MENU_",
				},
				stripeClasses: [],
				lengthMenu: [10, 25, 50],
				pageLength: 10,
			});
		} else {
			$(".tbReportSales tbody").html(
				'<tr><td colspan="9" align="center" class="text-danger">Set filter to show data..</td></tr>'
			);
		}
	}

	$(document).on("click", "#reportSales", function (e) {
		e.preventDefault();
		var dtg = $('[name="date_togo"]').val();
		if (dtg !== "") {
			window.open("excel-sales/" + dtg, "_blank");
		} else {
			alert("Please select date range !!");
		}
	});

	// REPORT IPAY88
	var tbReportIpay = $(".tbReportIpay");
	if (tbReportIpay.length == "1") {
		$(".tbReportIpay").attr("id", "tbReportIpay");
		if (date_togo != "0") {
			$("#tbReportIpay").DataTable({
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				sorting: [[1, "DESC"]],
				ajax: {
					url: "rpt-data-ipay/" + date_togo,
					type: "POST",
				},
				columnDefs: [
					{
						targets: [0, 13],
						className: "text-center",
					},
					{
						targets: [2, 3],
						className: "text-right",
					},
				],
				language: {
					zeroRecords: "No Data Found !!",
					processing: "Please wait, get data from server..",
				},
				oLanguage: {
					oPaginate: {
						sPrevious:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
						sNext:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
					},
					sInfo: "Showing page _PAGE_ of _PAGES_",
					sSearch:
						'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
					sSearchPlaceholder: "Search...",
					sLengthMenu: "Results :  _MENU_",
				},
				stripeClasses: [],
				lengthMenu: [10, 25, 50],
				pageLength: 10,
			});
		} else {
			$(".tbReportIpay tbody").html(
				'<tr><td colspan="14" align="center" class="text-danger">Set filter to show data..</td></tr>'
			);
		}
	}

	$(document).on("click", "#reportIpay", function (e) {
		e.preventDefault();
		var dtg = $('[name="date_togo"]').val();
		if (dtg !== "") {
			window.open("excel-ipay/" + dtg, "_blank");
		} else {
			alert("Please select date range !!");
		}
	});

	var tbTicket = $(".tbTicket");
	if (tbTicket.length == "1") {
		$(".tbTicket").attr("id", "tbTicket");
		if (year != "0" || date_togo != "0") {
			$("#tbTicket").DataTable({
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				ordering: true,
				// sorting: [[1, "DESC"]],
				ajax: {
					url: "get-dataticket/" + year + "/" + date_togo,
					type: "POST",
				},
				columnDefs: [
					{
						targets: [0, 1, 10, 11, 12],
						className: "text-center",
					},
					{
						targets: [2],
						className: "text-center inline-full",
					},
					{
						orderable: false,
						targets: [1],
					},
				],
				language: {
					zeroRecords: "No Data Found !!",
					processing: "Please wait, get data from server..",
				},
				oLanguage: {
					oPaginate: {
						sPrevious:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
						sNext:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
					},
					sInfo: "Showing page _PAGE_ of _PAGES_",
					sSearch:
						'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
					sSearchPlaceholder: "Search...",
					sLengthMenu: "Results :  _MENU_",
				},
				stripeClasses: [],
				lengthMenu: [10, 25, 50],
				pageLength: 10,
			});
		} else {
			$(".tbTicket tbody").html(
				'<tr><td colspan="13" align="center" class="text-danger">Set filter to show data..</td></tr>'
			);
		}
	}

	var tbVisitor = $(".tbVisitor");
	if (tbVisitor.length == "1") {
		$(".tbVisitor").attr("id", "tbVisitor");
		if (year != "0") {
			$("#tbVisitor").DataTable({
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				ordering: true,
				// sorting: [[0, "DESC"]],
				ajax: {
					url: "get-datavisitor/" + year,
					type: "POST",
				},
				columnDefs: [
					{
						targets: [0, 1, 6, 7, 8],
						className: "text-center",
					},
					{
						orderable: false,
						targets: [1, 8],
					},
				],
				language: {
					zeroRecords: "No Data Found !!",
				},
				oLanguage: {
					oPaginate: {
						sPrevious:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
						sNext:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
					},
					sInfo: "Showing page _PAGE_ of _PAGES_",
					sSearch:
						'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
					sSearchPlaceholder: "Search...",
					sLengthMenu: "Results :  _MENU_",
				},
				stripeClasses: [],
				lengthMenu: [10, 25, 50],
				pageLength: 10,
			});
		} else {
			$(".tbVisitor tbody").html(
				'<tr><td colspan="9" align="center" class="text-danger">Set filter to show data..</td></tr>'
			);
		}
	}

	var tbLockquota = $(".tbLockquota");
	if (tbLockquota.length == "1") {
		$(".tbLockquota").attr("id", "tbLockquota");
		if (year != "0") {
			$("#tbLockquota").DataTable({
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				sorting: [[0, "DESC"]],
				ajax: {
					url: "get-lock-quota/" + year,
					type: "POST",
				},
				columnDefs: [
					{
						targets: [0, 1, 6, 7, 8],
						className: "text-center",
					},
					{
						orderable: false,
						targets: [1],
					},
				],
				language: {
					zeroRecords: "No Data Found !!",
				},
				oLanguage: {
					oPaginate: {
						sPrevious:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
						sNext:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
					},
					sInfo: "Showing page _PAGE_ of _PAGES_",
					sSearch:
						'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
					sSearchPlaceholder: "Search...",
					sLengthMenu: "Results :  _MENU_",
				},
				stripeClasses: [],
				lengthMenu: [10, 25, 50],
				pageLength: 10,
			});
		} else {
			$(".tbLockquota tbody").html(
				'<tr><td colspan="9" align="center" class="text-danger">Set filter to show data..</td></tr>'
			);
		}
	}

	var tbCheckin = $(".tbCheckin");
	var ticketId = $('[name="ticketId"]').val();
	var tkId = ticketId == "" || ticketId == null ? 0 : ticketId;
	if (tbCheckin.length == "1") {
		$(".tbCheckin").attr("id", "tbCheckin");
		if (tkId != "0") {
			$("#tbCheckin").DataTable({
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				// sorting: [[1, "DESC"]],
				ajax: {
					url: "get-datacheckin/" + tkId,
					type: "POST",
				},
				columnDefs: [
					{
						targets: [0, 1, 2, 9],
						className: "text-center",
					},
					{
						orderable: false,
						targets: [1],
					},
				],
				language: {
					zeroRecords: "No Data Found !!",
				},
				oLanguage: {
					oPaginate: {
						sPrevious:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
						sNext:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
					},
					sInfo: "Showing page _PAGE_ of _PAGES_",
					sSearch:
						'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
					sSearchPlaceholder: "Search...",
					sLengthMenu: "Results :  _MENU_",
				},
				stripeClasses: [],
				lengthMenu: [10, 25, 50],
				pageLength: 10,
			});
		} else {
			$(".tbCheckin tbody").html(
				'<tr><td colspan="10" align="center" class="text-danger">Set filter to show data..</td></tr>'
			);
		}
	}

	var tbRequery = $(".tbRequery");
	if (tbRequery.length == "1") {
		$(".tbRequery").attr("id", "tbRequery");
		if (date_togo != "0") {
			$("#tbRequery").DataTable({
				responsive: true,
				autoWidth: false,
				processing: true,
				serverSide: true,
				ordering: true,
				// sorting: [[11, "DESC"]],
				ajax: {
					url: "get-data-requery/" + date_togo,
					type: "POST",
				},
				columnDefs: [
					{
						targets: [0, 1, 3, 11, 12],
						className: "text-center",
					},
					{
						orderable: false,
						targets: [1, 12],
					},
				],
				language: {
					zeroRecords: "No Data Found !!",
				},
				oLanguage: {
					oPaginate: {
						sPrevious:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
						sNext:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
					},
					sInfo: "Showing page _PAGE_ of _PAGES_",
					sSearch:
						'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
					sSearchPlaceholder: "Search...",
					sLengthMenu: "Results :  _MENU_",
				},
				stripeClasses: [],
				lengthMenu: [10, 25, 50],
				pageLength: 10,
			});
		} else {
			$(".tbRequery tbody").html(
				'<tr><td colspan="13" align="center" class="text-danger">Set filter to show data..</td></tr>'
			);
		}
	}

	// REPORT CLIMBER CHANGE
	var tbReportClimbChange = $(".tbReportClimbChange");
	if (tbReportClimbChange.length == "1") {
		$(".tbReportClimbChange").attr("id", "tbReportClimbChange");
		if (date_togo != "0") {
			$("#tbReportClimbChange").DataTable({
				responsive: true,
				autoWidth: true,
				processing: true,
				serverSide: true,
				sorting: [[1, "DESC"]],
				ajax: {
					url: "rpt-data-climbchange/" + date_togo,
					type: "POST",
				},
				columnDefs: [
					{
						targets: [0, 1, 2, 4, 5, 7],
						className: "text-center",
					},
				],
				language: {
					zeroRecords: "No Data Found !!",
				},
				oLanguage: {
					oPaginate: {
						sPrevious:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
						sNext:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
					},
					sInfo: "Showing page _PAGE_ of _PAGES_",
					sSearch:
						'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
					sSearchPlaceholder: "Search...",
					sLengthMenu: "Results :  _MENU_",
				},
				stripeClasses: [],
				lengthMenu: [10, 25, 50],
				pageLength: 10,
			});
		} else {
			$(".tbReportClimbChange tbody").html(
				'<tr><td colspan="7" align="center" class="text-danger">Set filter to show data..</td></tr>'
			);
		}
	}

	$(document).on("click", "#reportClimbchange", function (e) {
		e.preventDefault();
		if (date_togo == "") {
			alert("Please select option or date range !!");
		} else {
			window.open("excel-cilmberchange/" + date_togo, "_blank");
		}
	});
	// END REPORT CLIMBER CHANGE

	// REPORT CANCEL CLIMBER
	var tbReportCancel = $(".tbReportCancel");
	if (tbReportCancel.length == "1") {
		$(".tbReportCancel").attr("id", "tbReportCancel");
		if (date_togo != "0") {
			$("#tbReportCancel").DataTable({
				responsive: true,
				autoWidth: true,
				processing: true,
				serverSide: true,
				sorting: [[1, "DESC"]],
				ajax: {
					url: "rpt-data-cancelclimb/" + date_togo,
					type: "POST",
				},
				columnDefs: [
					{
						targets: [0, 1, 2, 4, 8],
						className: "text-center",
					},
				],
				language: {
					zeroRecords: "No Data Found !!",
				},
				oLanguage: {
					oPaginate: {
						sPrevious:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
						sNext:
							'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
					},
					sInfo: "Showing page _PAGE_ of _PAGES_",
					sSearch:
						'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
					sSearchPlaceholder: "Search...",
					sLengthMenu: "Results :  _MENU_",
				},
				stripeClasses: [],
				lengthMenu: [10, 25, 50],
				pageLength: 10,
			});
		} else {
			$(".tbReportCancel tbody").html(
				'<tr><td colspan="9" align="center" class="text-danger">Set filter to show data..</td></tr>'
			);
		}
	}

	$(document).on("click", "#reportCancelps", function (e) {
		e.preventDefault();
		if (date_togo == "") {
			alert("Please select option or date range !!");
		} else {
			window.open("excel-cancelps/" + date_togo, "_blank");
		}
	});
	// END REPORT CANCEL CLIMBER

	$(document).on("click", ".memberDetail", function () {
		var id = $(this).data("id");
		$.ajax({
			url: "detail-member/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data) {
				$(".memberDetailBody").html(
					'<div class="col-sm-6">' +
						"<p><b>FIRST NAME :</b> " +
						data.first_name +
						"</p>" +
						"<p><b>LAST NAME :</b> " +
						data.last_name +
						"</p>" +
						"<p><b>PHONE :</b> " +
						data.phone +
						"</p>" +
						"<p><b>EMAIL :</b> " +
						data.email +
						"</p>" +
						"</div>" +
						'<div class="col-sm-6">' +
						"<p><b>ADDRESS :</b> " +
						data.address +
						"</p>" +
						"<p><b>CITY :</b> " +
						data.city +
						"</p>" +
						"<p><b>POSTAL CODE :</b> " +
						data.postcode +
						"</p>" +
						"<p><b>NATIONALITY :</b> " +
						data.country +
						"</p>" +
						"</div>"
				);
			},
			error: function () {
				console.log("Error.");
			},
		});
		$("#mdMember").modal("show");
	});

	var carts = [];
	var sesDestId = [];
	$(document).on("click", ".orderDetail", function () {
		var id = $(this).data("id");
		var destid = $(this).data("destid");
		var idupd = $(this).data("idupd");
		var status = $(this).data("status");
		var datebefore = $(this).data("datetogo");
		var billId = $(this).data("bill");
		var sts =
			status == "UNPAID" || status == "PAID IPAY88" || status == "PAID (Manual)"
				? '<badge class="badge badge-danger ml-1 changeDateTogo" role="button" title="Change date to go"><i class="fas fa-edit modalChangeDateTogo"></i></badge>'
				: "";
		$('[name="idDatetogo"]').val(idupd);
		$('[name="destId"]').val(destid);
		$('[name="dateBefore"]').val(datebefore);
		$('[name="billId"]').val(billId);
		sesDestId.push(destid);
		$.ajax({
			url: "detail-order/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data) {
				var paidManual =
					data.status == "UNPAID"
						? '<badge class="badge badge-success ml-2" role="button" title="Paid manual"><i class="fas fa-dollar-sign"></i> Paid (Manual)</badge>'
						: "";
				var rcp =
					data.receipt != null
						? '<badge class="badge badge-info ml-2 viewReceipt" role="button" title="View receipt" data-img="' +
						  data.receipt +
						  '" data-note="' +
						  data.note +
						  '"><i class="fas fa-eye"></i></badge>'
						: "";
				var remkCancel = data.note_cancel != null ? data.note_cancel : "-";
				var remark;
				if (remkCancel != "-") {
					remark =
						"<p><b>REMARK CANCEL :</b> <span class='text-danger'>" +
						remkCancel +
						"</span></p>";
				} else {
					remark = "";
				}
				$(".orderDetailBody").html(
					'<div class="col-sm-6">' +
						"<p><b>ORDER ID :</b> " +
						data.billing_id +
						"</p>" +
						"<p><b>ORDER DATE :</b> " +
						data.createdate +
						"</p>" +
						"<p><b>DESTINATION :</b> " +
						data.destname +
						"</p>" +
						"<p><b>DATE TO GO :</b> " +
						data.ticketdatefrom +
						'<badge class="badge badge-warning ml-2 historyChangeDate" role="button" title="History change date to go"><i class="fas fa-history"></i></badge>' +
						sts +
						"</p>" +
						"</div>" +
						'<div class="col-sm-6">' +
						"<p><b>QTY :</b> " +
						data.total_pax +
						"</p>" +
						"<p><b>FIRST NAME :</b> " +
						data.first_name +
						"</p>" +
						"<p><b>LAST NAME :</b> " +
						data.last_name +
						"</p>" +
						"<p><b>STATUS :</b> " +
						data.status +
						rcp +
						"</p>" +
						remark +
						"</div>"
				);
			},
			error: function () {
				console.log("Error.");
			},
		});
		$("#mdOrder").modal("show");
	});

	var datego_billing;
	$(document).on("input", "#date_togo", function () {
		datego_billing = $(this).val();
		// console.log(test);
	});

	// $(document).on("change", ".getAllClimber", function () {
	// 	console.log("get climber");
	// });

	$("#radioSelect").click(function () {
		// var billing = $('[name="billId"]').val();
		var billing = $("#billIdBilling").val();
		var bill = billing.replace("#", "");
		if ($(this).is(":checked")) {
			$.ajax({
				type: "POST",
				url: "get-climber/" + bill,
				// data: order,
				contentType: "application/json",
				dataType: "json",
				success: function (data) {
					var sts,
						inp,
						html = "";
					// console.log(data);
					for (var i = 0; i < data.length; i++) {
						inp =
							data[i]["status"] !== "3"
								? '<input type="checkbox" name="climber[' +
								  i +
								  ']" class="climberData climber' +
								  i +
								  ' mt-2" value="' +
								  data[i].id +
								  '"> Update this'
								: "";
						css = data[i]["status"] == "3" ? "#ff7777" : "#e5e5e5";
						sts = data[i]["status"] == "3" ? "CANCEL" : "ACTIVE";
						html +=
							'<div style="font-size: 12px;background-color: ' +
							css +
							';color: #000;padding: 3px 2px 2px 20px;border-radius: 4px;margin-bottom: 5px;margin-right: 3px;"><b>' +
							(i + 1) +
							").</b> " +
							inp +
							"<br><b>Visitor Name :</b> " +
							data[i].visitor_name +
							"<br><b>Email :</b> " +
							data[i].id_number +
							"<br><b>Date To Go :</b> " +
							data[i].ticketdatefrom +
							"<br><b>Status :</b> " +
							sts +
							"</div>";
					}
					$("#showDataClimber").addClass("scroll-climber");
					$("#showDataClimber").html(html);
				},
			});
		} else {
			$("#showDataClimber").html("");
		}
	});

	$("#radioAll").click(function () {
		if ($(this).is(":checked")) {
			$("#showDataClimber").removeClass("scroll-climber");
			$("#showDataClimber").html("");
			$("#personid").val("");
		}
	});

	$(document).on("click", ".cekSek", function () {
		if ($("#radioAll").is(":checked") || $("#radioSelect").is(":checked")) {
			var personid = [];
			var climberData = $(".climberData");
			for (let i = 0; i < climberData.length; i++) {
				if ($(".climber" + i).is(":checked")) {
					// console.log($('.climber'+i).val());
					personid.push($(".climber" + i).val());
				}
			}
			$("#personid").val(personid);

			var destId = $('[name="destId"]').val();
			var bulan = $(".flatpickr-monthDropdown-months").find(":selected").val();
			var bln = parseInt(bulan) + 1;
			var tahun = $(".cur-year").val();
			var tgl = $(".selected").text();

			var nbln = ("0" + bln).slice(-2);
			var ntgl = ("0" + tgl).slice(-2);
			var date = tahun + "-" + nbln + "-" + ntgl;

			var arr = [];
			$.ajax({
				type: "GET",
				url:
					"https://apisptix.sabahparks.org.my/public/order/get-event-new?destid=" +
					destId +
					"&month=" +
					bln +
					"&year=" +
					tahun,
				// data: order,
				contentType: "application/json",
				dataType: "json",
				// beforeSend: function () {
				// 	$(".loading").html("Processing checking full date..");
				// },
				success: function (row) {
					// st = 0, ADA SLOT
					var st = "open";
					var lg = row.data;
					// console.log(row);
					var arr = [];
					for (let i = 0; i < lg.length; i++) {
						var d1 = row.data[i].date;
						// var d2 = d1.split("-");
						arr.push(d1);
						if (date == d1 && row.data[i].status == 0) {
							// st = 1, FULL
							st = "full";
						}
					}
					// console.log(date);
					// console.log(arr);
					// console.log(st);
					var dd = date.split("-");
					if (dd[2] == "0") {
						alert(
							"MESSAGES SYSTEM : \r\n\r\n1). PLEASE SELECT DATE FIRST\r\n2). CHECK ON VIEW DATE (DATE FREE/FULL ON THIS DAY)\r\n     - RED COLORS, DATE FULL (NOT AVAILABLE QUOTA)\r\n     - GREEN COLORS, DATE FREE (DATE WITH AVAILABLE QUOTA)\r\n3). CLICK SAVE"
						);
					} else {
						if (st == "full") {
							$(".alertCekDate").html(
								'<div class="alert alert-danger mt-3 mb-2" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>The date <b>' +
									date +
									"</b> is full !!</div>"
							);
						} else {
							$(".alertCekDate").html("");
							$('[name="dateAfter"]').val(datego_billing);
							$("#formBilling").submit();
							// var xx = $("#formBilling").serializeArray();
							// console.log(xx);
							// $(".submitBilling").click();
						}
					}
				},
			});
		} else {
			$(".info-cdtg").css("border-color", "2px solid red");
			alert(
				"PLEASE SELECT ONE RADIO BUTTON UPDATE TYPE CLIMBER !!\r\n1. UPDATE ALL\r\n2. UPDATE BY SELECT CLIMBER"
			);
		}
	});

	$(document).on("click", ".viewReceipt", function () {
		var img = $(this).data("img");
		var note = $(this).data("note");
		$("#changeDate").hide();
		$(".historyChange").hide();
		$("#radioAll").prop("checked", false);
		$("#radioSelect").prop("checked", false);
		$("#showDataClimber").html("");
		$("#showDataClimber").removeClass("scroll-climber");
		$("#receipt")
			.show()
			.html(
				"<span class='badge badge-info'><b>Note Resit : </b>" +
					note +
					'</span><br><img src="assets/img/receipt/' +
					img +
					'" style="width: 100%;margin-top: 8px;">'
			);
	});

	$(document).on("click", ".historyChangeDate", function () {
		$("#changeDate").hide();
		$("#receipt").hide();
		$(".historyChange").show();
		$("#radioAll").prop("checked", false);
		$("#radioSelect").prop("checked", false);
		$("#showDataClimber").html("");
		$("#showDataClimber").removeClass("scroll-climber");
		var id = $('[name="billId"]').val();
		var id2 = id.replace("#", "");
		$.ajax({
			type: "POST",
			url: "get-hist-changedate/" + id2,
			contentType: "application/json",
			dataType: "json",
			success: function (data) {
				var html = "";
				var i = 1;
				// console.log(data);
				$.each(data, function (key, value) {
					html +=
						"<tr><td align='center'>" +
						i +
						"</td><td align='center'>" +
						value.date_before +
						"</td><td align='center'>" +
						value.date_togo +
						"</td><td>" +
						value.nama_lengkap +
						"</td><td align='right'>" +
						value.tahun +
						" " +
						value.time +
						"</td></tr>";
					i++;
				});
				$("#historyChgTable").html(html);
			},
		});
	});

	$(document).on("click", ".changeDateTogo", function () {
		var destId = $('[name="destId"]').val();
		var bulan = $(".flatpickr-monthDropdown-months").find(":selected").val();
		var bln = parseInt(bulan) + 1;
		var tahun = $(".cur-year").val();
		$("#changeDate").show();
		$(".historyChange").hide();
		$("#receipt").hide();
		$.ajax({
			type: "GET",
			url:
				"https://apisptix.sabahparks.org.my/public/order/get-event-new?destid=" +
				destId +
				"&month=" +
				bln +
				"&year=" +
				tahun,
			contentType: "application/json",
			dataType: "json",
			beforeSend: function () {
				$(".loading").html("Processing checking full date..");
			},
			success: function (row) {
				$(".loading").html("");
				var lg = row.data;
				var badge, text;
				for (let i = 0; i < lg.length; i++) {
					var d1 = row.data[i].date;
					var st = row.data[i].status;
					var d2 = d1.split("-");
					if (st == 0) {
						badge = "badge-danger";
						text = "";
					} else {
						badge = "badge-success";
						text = "<span class='sub-badge'>" + row.data[i].value + "</span>";
					}
					$(".loading").append(
						'<div class="for-date"><span class="badge ' +
							badge +
							' badge-date-full" style="margin-bottom: 10px;min-width: 30px !important;">' +
							d2[2] +
							"</span>" +
							text +
							"</div>" +
							"&nbsp;"
					);
					carts.push(row.data[i].date);
				}

				// $('[name="fullDatetogo"]').val(carts);
			},
		});
	});

	$(document).on("click", ".billingDetail", function () {
		$("#mdBilling").modal("show");
		var id = $(this).data("id");
		var destid = $(this).data("destid");
		var idupd = $(this).data("idupd");
		var status = $(this).data("status");
		var datebefore = $(this).data("datetogo");
		var billId = $(this).data("bill");
		$('[name="idDatetogo"]').val(idupd);
		// $('[name="destId"]').val(destid);
		$('[name="dateBefore"]').val(datebefore);
		$("#destIdBilling").val(destid);
		$("#billIdBilling").val(billId);
		sesDestId.push(destid);
		$.ajax({
			url: "detail-billing/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data) {
				var note_lock =
					data.note_lock_quota != null
						? "<p><b>REMARK LOCK QUOTA :</b><span class='text-danger'> " +
						  data.note_lock_quota +
						  "</span></p>"
						: "";
				var rcp =
					data.receipt != null
						? '<badge class="badge badge-info ml-2 viewReceipt" role="button" title="View receipt" data-img="' +
						  data.receipt +
						  '" data-note="' +
						  data.note +
						  '"><i class="fas fa-eye"></i></badge>'
						: "";
				var remkCancel = data.note_cancel != null ? data.note_cancel : "-";
				var remark;
				if (remkCancel != "-") {
					remark =
						"<p><b>REMARK CANCEL :</b> <span class='text-danger'>" +
						remkCancel +
						"</span></p>";
				} else {
					remark = "";
				}
				var sts =
					data.ket_status == "UNPAID" ||
					data.ket_status == "PAID IPAY88" ||
					data.ket_status == "PAID (Manual)"
						? '<badge class="badge badge-danger ml-1 changeDateTogo" role="button" title="Change date to go"><i class="fas fa-edit modalChangeDateTogo"></i></badge>'
						: "";
				$(".billingDetailBody").html(
					'<div class="col-sm-6">' +
						"<p><b>ORDER ID :</b> " +
						data.billing_id +
						"</p>" +
						"<p><b>ORDER DATE :</b> " +
						data.createdate +
						"</p>" +
						"<p><b>DESTINATION :</b> " +
						data.destname +
						"</p>" +
						"<p><b>DATE TO GO :</b> " +
						data.ticketdatefrom +
						'<badge class="badge badge-warning ml-2 historyChangeDate" role="button" title="History change date to go"><i class="fas fa-history"></i></badge>' +
						sts +
						"</p>" +
						note_lock +
						"</div>" +
						'<div class="col-sm-6">' +
						"<p><b>PAYMENT REF.NO :</b> " +
						data.receipt_id +
						"</p>" +
						"<p><b>FIRST NAME :</b> " +
						data.first_name +
						"</p>" +
						"<p><b>LAST NAME :</b> " +
						data.last_name +
						"</p>" +
						"<p><b>STATUS :</b> " +
						data.ket_status +
						rcp +
						"</p>" +
						remark +
						"</div>"
				);
			},
			error: function () {
				console.log("Error.");
			},
		});
	});

	$(document).on("click", ".visitorDetail", function () {
		var id = $(this).data("id");
		console.log(id);
		$.ajax({
			url: "detail-visitor/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data) {
				var pr_holder = data.pr_holder == "1" ? "Y" : "N";
				$(".visitorDetailBody").html(
					'<div class="col-sm-6">' +
						"<p><b>NAME :</b> " +
						data.visitor_name +
						"</p>" +
						"<p><b>PHONE :</b> " +
						data.phone +
						"</p>" +
						"<p><b>EMAIL :</b> " +
						data.email +
						"</p>" +
						"</div>" +
						'<div class="col-sm-6">' +
						"<p><b>NATIONALITY :</b> " +
						data.nationality +
						"</p>" +
						"<p><b>PR. STATUS :</b> " +
						pr_holder +
						"</p>" +
						"<p><b>QTY ATTEND :</b> " +
						data.total_attend +
						"</p>" +
						"</div>"
				);
				// $('#headVisitor').html($(this).data('status'));
			},
			error: function () {
				console.log("Error.");
			},
		});
		$("#mdVisitor").modal("show");
	});

	$(".flatpickr").flatpickr({ static: true });

	$(".rangeCalendarFlatpickr").flatpickr({ mode: "range" });

	$(document).on("click", ".modalApprove", function () {
		var id = $(this).data("id");
		var status = $(this).data("status");
		$("#idHolderApprove").val(id);
		$("#statusHolderApprove").val(status);
		if (status == "2") {
			$("#headHolderApprove").html("Approved");
		} else if (status == "3") {
			$("#headHolderApprove").html("Rejected");
		} else if (status == "0") {
			$("#headHolderApprove").html("Canceled");
		}
		$("#modalApprove").modal("show");
	});

	$(document).on("click", ".pdfBilling", function () {
		var billing_id = $(this).data("billid");
		window
			.open(
				"https://www.apisptix.sabahparks.org.my/public/download/receipt?billing_id=" +
					billing_id,
				"_blank"
			)
			.focus();
	});

	// $(document).on("click", ".modalChangeDateTogo", function () {
	// 	var destId = $(this).data("destid");
	// 	var datetogo = $(this).data("datetogo");
	// 	var bill = $(this).data("bill");
	// 	var idupd = $(this).data("idupd");
	// 	$('[name="idDatetogo"]').val(idupd);
	// 	$("#date_togo").val(datetogo);
	// 	sesDestId.push(destId);

	// 	var bulan = $(".flatpickr-monthDropdown-months").find(":selected").val();
	// 	var bln = parseInt(bulan) + 1;
	// 	var tahun = $(".cur-year").val();
	// 	$.ajax({
	// 		type: "GET",
	// 		url:
	// 			"https://www.apisptix.sabahparks.org.my/public/order/get-event-new?destid=" +
	// 			destId +
	// 			"&month=" +
	// 			bln +
	// 			"&year=" +
	// 			tahun,
	// 		// data: order,
	// 		contentType: "application/json",
	// 		dataType: "json",
	// 		beforeSend: function () {
	// 			$(".loading").html("Processing checking full date..");
	// 		},
	// 		success: function (row) {
	// 			$(".loading").html("");
	// 			var lg = row.data;
	// 			for (let i = 0; i < lg.length; i++) {
	// 				var d1 = row.data[i].date;
	// 				var d2 = d1.split("-");
	// 				$(".loading").append(
	// 					'<span class="badge badge-danger badge-date-full" style="margin-bottom: 10px;min-width: 30px !important;">' +
	// 						d2[2] +
	// 						"</span>&nbsp;"
	// 				);
	// 				carts.push(row.data[i].date);
	// 			}

	// 			$('[name="fullDatetogo"]').val(carts);
	// 		},
	// 	});
	// 	$(".dtgBillingId").html(bill);
	// 	$("#modalChangeDateTogo").modal("show");
	// });

	$("#date_togo").flatpickr({
		static: true,
		inline: true,
		// disable: date,
		dateFormat: "Y-m-d",
	});

	$(document).on("change", ".flatpickr-monthDropdown-months", function () {
		var n = $(this).find(":selected").val();
		var bln = parseInt(n) + 1;
		var tahun = $(".cur-year").val();
		$.ajax({
			type: "GET",
			url:
				"https://apisptix.sabahparks.org.my/public/order/get-event-new?destid=" +
				sesDestId[0] +
				"&month=" +
				bln +
				"&year=" +
				tahun,
			// data: order,
			contentType: "application/json",
			dataType: "json",
			beforeSend: function () {
				$(".loading").html("Processing checking full date..");
			},
			success: function (row) {
				// console.log(row);
				$(".loading").html("");
				var lg = row.data;
				var badge, text;
				if (lg.length == 0) {
					$(".loading").html(
						"<span class='text-danger'>No full date to go !!</span>"
					);
				} else {
					for (let i = 0; i < lg.length; i++) {
						var d1 = row.data[i].date;
						var st = row.data[i].status;
						var d2 = d1.split("-");
						if (st == 0) {
							badge = "badge-danger";
							text = "";
						} else {
							badge = "badge-success";
							text = "<span class='sub-badge'>" + row.data[i].value + "</span>";
						}
						$(".loading").append(
							'<div class="for-date"><span class="badge ' +
								badge +
								' badge-date-full" style="margin-bottom: 10px;min-width: 30px !important;">' +
								d2[2] +
								"</span>" +
								text +
								"</div>" +
								"&nbsp;"
						);
					}
				}
			},
		});
	});

	$(document).on("click", ".arrowUp", function () {
		var bulan = $(".flatpickr-monthDropdown-months").find(":selected").val();
		var bln = parseInt(bulan) + 1;
		var tahun = $(".cur-year").val();
		$.ajax({
			type: "GET",
			url:
				"https://apisptix.sabahparks.org.my/public/order/get-event-new?destid=" +
				sesDestId[0] +
				"&month=" +
				bln +
				"&year=" +
				tahun,
			// data: order,
			contentType: "application/json",
			dataType: "json",
			beforeSend: function () {
				$(".loading").html("Processing checking full date..");
			},
			success: function (row) {
				$(".loading").html("");
				var lg = row.data;
				var badge, text;
				if (lg.length == 0) {
					$(".loading").html(
						"<span class='text-danger'>No full date to go !!</span>"
					);
				} else {
					for (let i = 0; i < lg.length; i++) {
						var d1 = row.data[i].date;
						var st = row.data[i].status;
						var d2 = d1.split("-");
						if (st == 0) {
							badge = "badge-danger";
							text = "";
						} else {
							badge = "badge-success";
							text = "<span class='sub-badge'>" + row.data[i].value + "</span>";
						}
						$(".loading").append(
							'<div class="for-date"><span class="badge ' +
								badge +
								' badge-date-full" style="margin-bottom: 10px;min-width: 30px !important;">' +
								d2[2] +
								"</span>" +
								text +
								"</div>" +
								"&nbsp;"
						);
					}
				}
			},
		});
	});

	$(document).on("click", ".arrowDown", function () {
		var bulan = $(".flatpickr-monthDropdown-months").find(":selected").val();
		var bln = parseInt(bulan) + 1;
		var tahun = $(".cur-year").val();
		$.ajax({
			type: "GET",
			url:
				"https://apisptix.sabahparks.org.my/public/order/get-event-new?destid=" +
				sesDestId[0] +
				"&month=" +
				bln +
				"&year=" +
				tahun,
			// data: order,
			contentType: "application/json",
			dataType: "json",
			beforeSend: function () {
				$(".loading").html("Processing checking full date..");
			},
			success: function (row) {
				$(".loading").html("");
				var lg = row.data;
				var badge, text;
				if (lg.length == 0) {
					$(".loading").html(
						"<span class='text-danger'>No full date to go !!</span>"
					);
				} else {
					for (let i = 0; i < lg.length; i++) {
						var d1 = row.data[i].date;
						var st = row.data[i].status;
						var d2 = d1.split("-");
						if (st == 0) {
							badge = "badge-danger";
							text = "";
						} else {
							badge = "badge-success";
							text = "<span class='sub-badge'>" + row.data[i].value + "</span>";
						}
						$(".loading").append(
							'<div class="for-date"><span class="badge ' +
								badge +
								' badge-date-full" style="margin-bottom: 10px;min-width: 30px !important;">' +
								d2[2] +
								"</span>" +
								text +
								"</div>" +
								"&nbsp;"
						);
					}
				}
			},
		});
	});

	$(document).on("click", ".flatpickr-next-month", function () {
		var bulan = $(".flatpickr-monthDropdown-months").find(":selected").val();
		var bln = parseInt(bulan) + 1;
		var tahun = $(".cur-year").val();
		$.ajax({
			type: "GET",
			url:
				"https://apisptix.sabahparks.org.my/public/order/get-event-new?destid=" +
				sesDestId[0] +
				"&month=" +
				bln +
				"&year=" +
				tahun,
			// data: order,
			contentType: "application/json",
			dataType: "json",
			beforeSend: function () {
				$(".loading").html("Processing checking full date..");
			},
			success: function (row) {
				$(".loading").html("");
				var lg = row.data;
				var badge, text;
				if (lg.length == 0) {
					$(".loading").html(
						"<span class='text-danger'>No full date to go !!</span>"
					);
				} else {
					for (let i = 0; i < lg.length; i++) {
						var d1 = row.data[i].date;
						var st = row.data[i].status;
						var d2 = d1.split("-");
						if (st == 0) {
							badge = "badge-danger";
							text = "";
						} else {
							badge = "badge-success";
							text = "<span class='sub-badge'>" + row.data[i].value + "</span>";
						}
						$(".loading").append(
							'<div class="for-date"><span class="badge ' +
								badge +
								' badge-date-full" style="margin-bottom: 10px;min-width: 30px !important;">' +
								d2[2] +
								"</span>" +
								text +
								"</div>" +
								"&nbsp;"
						);
					}
				}
			},
		});
	});

	$(document).on("click", ".flatpickr-prev-month", function () {
		var bulan = $(".flatpickr-monthDropdown-months").find(":selected").val();
		var bln = parseInt(bulan) + 1;
		var tahun = $(".cur-year").val();
		$.ajax({
			type: "GET",
			url:
				"https://apisptix.sabahparks.org.my/public/order/get-event-new?destid=" +
				sesDestId[0] +
				"&month=" +
				bln +
				"&year=" +
				tahun,
			// data: order,
			contentType: "application/json",
			dataType: "json",
			beforeSend: function () {
				$(".loading").html("Processing checking full date..");
			},
			success: function (row) {
				$(".loading").html("");
				var lg = row.data;
				var badge, text;
				if (lg.length == 0) {
					$(".loading").html(
						"<span class='text-danger'>No full date to go !!</span>"
					);
				} else {
					for (let i = 0; i < lg.length; i++) {
						var d1 = row.data[i].date;
						var st = row.data[i].status;
						var d2 = d1.split("-");
						if (st == 0) {
							badge = "badge-danger";
							text = "";
						} else {
							badge = "badge-success";
							text = "<span class='sub-badge'>" + row.data[i].value + "</span>";
						}
						$(".loading").append(
							'<div class="for-date"><span class="badge ' +
								badge +
								' badge-date-full" style="margin-bottom: 10px;min-width: 30px !important;">' +
								d2[2] +
								"</span>" +
								text +
								"</div>" +
								"&nbsp;"
						);
					}
				}
			},
		});
	});

	function getDaysInMonth(year, month) {
		return new Date(year, month, 0).getDate();
	}

	const date = new Date();
	const currentYear = date.getFullYear();
	const currentMonth = date.getMonth() + 1; // 👈️ months are 0-based

	// 👇️ Current Month
	const daysInCurrentMonth = getDaysInMonth(currentYear, currentMonth);
	// console.log(daysInCurrentMonth);

	const days = getDaysInMonth(2022, 2);
	// console.log(days);

	$(".zero-config").DataTable({
		oLanguage: {
			oPaginate: {
				sPrevious:
					'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
				sNext:
					'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
			},
			sInfo: "Showing page _PAGE_ of _PAGES_",
			sSearch:
				'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
			sSearchPlaceholder: "Search...",
			sLengthMenu: "Results :  _MENU_",
		},
		stripeClasses: [],
		lengthMenu: [10, 20, 50],
		pageLength: 10,
	});

	function uploadGallery(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$(".galleryView").html(
					'<img src="' +
						e.target.result +
						'" style="width: 100%;margin-top: 8px;"><br>' +
						'<span class="text-success"><i class="fas fa-check-circle"></i> Ready to upload..</span>'
				);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}

	$(".galleryView").html("");
	$("#uploadGallery").change(function () {
		uploadGallery(this);
	});

	$(document).on("click", ".editGallery", function () {
		var id = $(this).data("id");
		$.ajax({
			type: "POST",
			url: "edit-gallery/" + id,
			// data: order,
			contentType: "application/json",
			dataType: "json",
			success: function (data) {
				$("#idGallery").val(data.id);
				$(".galleryView").html(
					'<img src="assets/img/' +
						data.path +
						'" style="width: 100%;margin-top: 8px;">'
				);
				$('[name="desc"]').val(data.description);
				$("#type option[value='" + data.type + "']").prop("selected", true);
				if (data.status == "1") {
					$('[name="active"]').attr("checked", true);
				} else {
					$('[name="active"]').attr("checked", false);
				}
			},
		});
		$("#modalAddGallery").modal("show");
	});

	$(document).on("click", ".editUser", function () {
		var id = $(this).data("id");
		$.ajax({
			type: "POST",
			url: "edit-user/" + id,
			contentType: "application/json",
			dataType: "json",
			success: function (data) {
				$('[name="iduser"]').val(data.id_user);
				$("#role option[value='" + data.id_role + "']").prop("selected", true);
				$('[name="username"]').val(data.username);
				$('[name="fullname"]').val(data.nama_lengkap);
				$('[name="email"]').val(data.email);
				$('[name="phone"]').val(data.no_telpon);
				$('[name="password"]').val(data.new_password);
				if (data.aktif == "Y") {
					$('[name="active"]').attr("checked", true);
				} else {
					$('[name="active"]').attr("checked", false);
				}
				$(".lbPass").addClass("hidden");
			},
		});
		$("#modalAddUser").modal("show");
	});

	$(document).on("click", ".editUserRole", function () {
		var id = $(this).data("id");
		$.ajax({
			type: "POST",
			url: "edit-user-role/" + id,
			contentType: "application/json",
			dataType: "json",
			success: function (data) {
				$('[name="iduserRole"]').val(data.id);
				$('[name="userrole"]').val(data.role_name);
			},
		});
		$("#modalAddUser").modal("show");
	});

	$(document).on("click", ".menuByRole", function () {
		var id_role = $(this).data("id");
		var menu = $(this).data("menu");
		$(".role_menu").html("<b>" + menu + "</b>");
		$.ajax({
			type: "POST",
			url: "data-user",
			contentType: "application/json",
			dataType: "json",
			success: function (data) {
				$('[name="id_role"]').val(id_role);
				$("#tbUserRole tr").addClass("hidden");
				var root;
				var i = 1;
				$.each(data, function (key, value) {
					// console.log(value);
					$.post("cek-active-menu", {
						id_role: id_role,
						id_menu: value.id,
					}).done(function (get) {
						root =
							value.id_root == null
								? "<b>" +
								  value.menu +
								  "</b><span class='text-danger'> (Main Menu)</span>"
								: "<span class='text-danger'>(sub)</span> " + value.menu;
						if (get.hitung == "0") {
							$("#tbUserRole").append(
								"<tr>" +
									'<td align="center">' +
									i +
									"</td>" +
									'<td><input type="hidden" name="menu[' +
									i +
									']" value="' +
									value.id +
									'">' +
									root +
									"</td>" +
									'<td align="center"><div class="custom-control custom-checkbox">' +
									'<input type="checkbox" class="custom-control-input" name="active[' +
									i +
									']" id="customCheck' +
									i +
									'">' +
									'<label class="custom-control-label" for="customCheck' +
									i +
									'"> Yes</label>' +
									"</div></td>" +
									"</tr>"
							);
							i++;
						} else {
							$("#tbUserRole").append(
								"<tr>" +
									'<td align="center">' +
									i +
									"</td>" +
									'<td><input type="hidden" name="menu[' +
									i +
									']" value="' +
									value.id +
									'">' +
									root +
									"</td>" +
									'<td align="center"><div class="custom-control custom-checkbox">' +
									'<input type="checkbox" class="custom-control-input" name="active[' +
									i +
									']" id="customCheck' +
									i +
									'" checked>' +
									'<label class="custom-control-label" for="customCheck' +
									i +
									'"> Yes</label>' +
									"</div></td>" +
									"</tr>"
							);
							i++;
						}
					});
				});
			},
		});
		$("#tbUserRole").html("");
	});

	// console.log(date_togo);

	$(document).on("click", ".paidManual", function () {
		var id = $(this).data("id");
		var bill_id = $(this).data("bill");
		$("#modalUploadResit").modal("show");
		$("#idResit").val(id);
		$("#billidResit").val(bill_id);
	});

	$("#modalAddGallery, #modalAddUser, #mdOrder, #modalRequery").on(
		"hidden.bs.modal",
		function (e) {
			$(this)
				.find("input,textarea")
				.val("")
				.end()
				.find("input[type=checkbox], input[type=radio]")
				.prop("checked", "")
				.end();
			$("#type option[value='0']").prop("selected", true);
			$("#role option[value='0']").prop("selected", true);
			$(".galleryView").html("");
			$(".lbPass").removeClass("hidden");
			$("#changeDate").hide();
			$("#historyChgTable").html("");
			$(".historyChange").hide();
			$("#receipt, #msgRequery").html("");
		}
	);

	$(document).on("click", ".cancelBooking", function () {
		var id = $(this).data("id");
		var bill_id = $(this).data("bill");
		$('[name="id"]').val(id);
		$("#billIdCancel").val(bill_id);
		$("#orderIdCancel").html(bill_id);
		$("#modalCancelBooking").modal("show");
	});

	$("#modalCancelBooking").on("hidden.bs.modal", function (e) {
		e.preventDefault();
		// $("#cPersonal, #cAll").html("");
		$("#cPersonal, #cAll").css({ display: "none" });
	});

	$(document).on("click", ".checkIn", function () {
		var id = $(this).data("id");
		$('[name="id"]').val(id);
		$("#modalCheckin").modal("show");
	});

	$(document).on("click", ".modalRequery", function () {
		var billid = $(this).data("billid");
		$('[name="billing_id"]').val(billid);
		$("#modalRequery").modal("show");
	});

	$(document).on("click", "#btnRequeryYes", function () {
		$("#msgRequery").html(
			'<center><span class="text-danger">Processing checking on ipay88...</span></center>'
		);
		setTimeout(function () {
			$("#msgRequery").html(
				'<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>Err: Record not found</button></div>'
			);
			// $("#modalRequery").modal("hide");
		}, 2000);
	});

	$(document).on("click", ".deleteNews", function () {
		var id = $(this).data("id");
		$('[name="id"]').val(id);
		$("#modalDelete").modal("show");
	});

	$(document).on("click", ".editNews", function () {
		var id = $(this).data("id");
		$("#modalEditNews").modal("show");
		$.ajax({
			type: "POST",
			url: "edit-news/" + id,
			contentType: "application/json",
			dataType: "json",
			success: function (data) {
				$("#type option[value='" + data.type + "']").attr("selected", true);
				$('[name="date"]').val(data.createdate);
				$('[name="title"]').val(data.blogtitle);
				$('[name="desc"]').val(data.description);
				$(".galleryView").html(
					'<img src="https://www.apisptix.sabahparks.org.my/public/images/' +
						data.name_file +
						"/" +
						data.extention +
						'" style="width: 100%;margin-top: 8px;">'
				);
				if (parseInt(data.status) == 1) {
					// $('#customCheck1').prop("checked");
					$(".checkNews").html(
						'<input type="checkbox" name="active" checked> Active'
					);
				} else {
					// $('#customCheck1').attr("checked", false);
					$(".checkNews").html('<input type="checkbox" name="active"> Active');
				}
			},
		});
		$('[name="id"]').val(id);
	});

	$("#mdBilling").on("hidden.bs.modal", function (e) {
		$("#changeDate, #receipt").hide();
		$(".historyChange").hide();
		$("#radioAll").prop("checked", false);
		$("#radioSelect").prop("checked", false);
		$("#showDataClimber").html("");
		$("#showDataClimber").removeClass("scroll-climber");
	});

	$("#modalEditNews, #modalAddNews").on("hidden.bs.modal", function (e) {
		$(this)
			.find("input,textarea")
			.val("")
			.end()
			.find("input[type=checkbox], input[type=radio]")
			.prop("checked", "")
			.end();
		$('[name="title"]').val("");
		$('[name="date"]').val("");
		$('[name="desc"]').val("");
		$(".galleryView").html("");
	});

	$(document).on("click", ".deleteBroadcast", function () {
		var id = $(this).data("id");
		$('[name="id"]').val(id);
		$("#modalDelete").modal("show");
	});

	$(document).on("click", ".editBroadcast", function () {
		var id = $(this).data("id");
		$.ajax({
			type: "POST",
			url: "edit-broadcast/" + id,
			contentType: "application/json",
			dataType: "json",
			success: function (data) {
				$('[name="date"]').val(data.new_date);
				$('[name="title"]').val(data.title);
				$('[name="desc"]').val(data.content);
				$(".galleryView").html(
					'<img src="assets/img/broadcast/' +
						data.path +
						'" style="width: 100%;margin-top: 8px;">'
				);
				if (data.status == "Y") {
					$('[name="active"]').attr("checked", true);
				} else {
					$('[name="active"]').attr("checked", false);
				}
			},
		});
		$('[name="id"]').val(id);
		$("#modalEditBroadcast").modal("show");
	});

	$("#modalAddBroadcast, #modalEditBroadcast").on(
		"hidden.bs.modal",
		function (e) {
			$(this)
				.find("input,textarea")
				.val("")
				.end()
				.find("input[type=checkbox], input[type=radio]")
				.prop("checked", "")
				.end();
			$('[name="title"]').val("");
			$('[name="date"]').val("");
			$('[name="desc"]').val("");
			$(".galleryView").html("");
			$('[name="active"]').attr("checked", false);
		}
	);

	$(".editordata").summernote({
		height: 300, //set editable area's height
		codemirror: {
			// codemirror options
			theme: "monokai",
		},
	});

	$(document).on("click", ".editDest", function () {
		var id = $(this).data("id");
		var destid = $(this).data("destid");
		$.ajax({
			type: "POST",
			url: "edit-destination/" + id,
			contentType: "application/json",
			dataType: "json",
			success: function (data) {
				$(".note-editable").html(data.description);
			},
		});
		console.log(id);
		if (id == "1") {
			$("#modalEditDest01").modal("show");
			$("#dest_id1").val(destid);
		} else {
			$("#modalEditDest02").modal("show");
			$("#dest_id2").val(destid);
		}
	});

	// $("#modalEditDest01").on("hidden.bs.modal", function (e) {
	// 	$(".note-editable").html("");
	// });

	function changeThumb(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$(".view-thumb").hide();
				$(".view-thumb2").html(
					'<img src="' +
						e.target.result +
						'" class="mt-2" style="width: 100%;"><br>' +
						'<span class="text-success"><i class="fas fa-check-circle"></i> Ready to upload new thumbnail..</span>'
				);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}

	$("#changeThumb").change(function () {
		changeThumb(this);
	});

	$(document).on("click", ".formDest", function (e) {
		var formData = {
			name: $("#nm_dst1").val(),
			nominal1: $("#rm_dst1_a").val(),
			nominal2: $("#rm_dst1_b").val(),
			destid: $("#dest_id1").val(),
			loc_quota: $("#loc_q_dst1").val(),
			int_quota: $("#int_q_dst1").val(),
			desc: $(".editordata").summernote("code"),
			accomodation: {
				a: $(".738a").val(),
				b: $(".738b").val(),
			},
			permit: {
				a: $(".739a").val(),
				b: $(".739b").val(),
			},
			insurance: {
				a: $(".740a").val(),
				b: $(".740b").val(),
			},
			service: {
				a: $(".741a").val(),
				b: $(".741b").val(),
			},
		};
		// console.log(formData);
		$.ajax({
			type: "POST",
			url: "save-edit-dest",
			data: formData,
			// contentType: "application/json",
			dataType: "html",
			success: function (response) {
				var json = $.parseJSON(response);
				if (json.msg == "success") {
					$("#modalEditDest01").modal("hide");
					$(".alert-after-update").html(
						'<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>Update data destination successfully..</button></div>'
					);
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			},
		});
		e.preventDefault();
	});

	$(document).on("click", ".formDest2", function (e) {
		var formData = {
			name: $("#nm_dst2").val(),
			nominal1: $("#rm_dst2_a").val(),
			nominal2: $("#rm_dst2_b").val(),
			nominal3: $("#rm_dst2_c").val(),
			nominal4: $("#rm_dst2_d").val(),
			destid: $("#dest_id2").val(),
			loc_quota: $("#loc_q_dst2").val(),
			int_quota: $("#int_q_dst2").val(),
			desc: $(".editordata").summernote("code"),
			accomodation: {
				a: $(".734a").val(),
				b: $(".734b").val(),
				c: $(".734c").val(),
				d: $(".734d").val(),
			},
			permit: {
				a: $(".735a").val(),
				b: $(".735b").val(),
				c: $(".735c").val(),
				d: $(".735d").val(),
			},
			insurance: {
				a: $(".736a").val(),
				b: $(".736b").val(),
				c: $(".736c").val(),
				d: $(".736d").val(),
			},
			service: {
				a: $(".737a").val(),
				b: $(".737b").val(),
				c: $(".737c").val(),
				d: $(".737d").val(),
			},
		};
		// console.log(formData);
		$.ajax({
			type: "POST",
			url: "save-edit-dest",
			data: formData,
			// contentType: "application/json",
			dataType: "html",
			success: function (response) {
				var json = $.parseJSON(response);
				if (json.msg == "success") {
					$("#modalEditDest02").modal("hide");
					$(".alert-after-update").html(
						'<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>Update data destination successfully..</button></div>'
					);
					setTimeout(function () {
						location.reload();
					}, 1500);
				}
			},
		});
		e.preventDefault();
	});

	$(document).on("click", ".modalSendToInbox", function () {
		var id = $(this).data("id");
		$('[name="id"]').val(id);
		$("#modalSendToInbox").modal("show");
	});

	$(document).on("click", ".lockQuota", function () {
		var id = $(this).data("id");
		var destid = $(this).data("destid");
		var avb_quota = $(this).data("avb");
		var destname = $(this).data("destname");
		var sumquota = $(this).data("sumquota");
		// var html = '<option value="">- Select One -</option>';
		// for (let i = 1; i <= parseInt(avb_quota); i++) {
		// 	html += '<option value="' + i + '">' + i + "</option>";
		// }
		$('[name="id"]').val(id);
		$('[name="destid"]').val(destid);
		$('[name="sumquota"]').val(sumquota);
		// $("#destName").html("<b>" + destname + "</b>");
		// $("#avb").html('<b>"' + avb_quota + '"</b>');
		// $('[name="jumlah"]').html(html);
		$("#modalLockquota").modal("show");
	});

	// $(document).on("click", ".flatpickr-day", function () {
	// 	console.log("x");
	// });

	$(document).on("change", ".destination", function () {
		var id = $(this).val();
		if (id !== "") {
			$.ajax({
				type: "POST",
				url: "hitung-quota-dest/" + id,
				contentType: "application/json",
				dataType: "json",
				success: function (data) {
					$(".sum_dest").val(data.sum);
				},
			});
		}
	});

	$(document).on("input", ".cariTgl", function () {
		var date = $(this).val();
		var destid = $('[name="dest"]').val();
		if (destid == "") {
			$(this).val("");
			alert("Please select destination first !!");
		} else {
			$.ajax({
				type: "POST",
				url: "hitung-avb/" + destid + "/" + date,
				contentType: "application/json",
				dataType: "json",
				success: function (data) {
					var html = '<option value="">- Select One -</option>';
					avb = parseInt(data.tot_quota) - parseInt(data.hitung);
					for (let i = 1; i <= avb; i++) {
						html += '<option value="' + i + '">' + i + "</option>";
					}

					$('[name="jumlah"]').html(html);
					$("#avb").html('<b>"' + avb + '"</b>');
					$("#dateAvb").html('<b>"' + data.ticketdatefrom + '"</b>');
				},
			});
		}
	});

	$("#modalLockquota").on("hidden.bs.modal", function () {
		$('[name="date"]').val("");
		$("#jumlah option[value='']").attr("selected", true);
	});

	$(document).on("click", ".editVisitor", function () {
		var id = $(this).data("id");
		$.ajax({
			type: "POST",
			url: "edit-visitor/" + id,
			contentType: "application/json",
			dataType: "json",
			success: function (data) {
				$('[name="id"]').val(id);
				$('[name="nama"]').val(data.visitor_name);
				$('[name="email"]').val(data.email);
				$('[name="phone"]').val(data.phone);
				$('[name="national"]').val(data.nationality);
			},
		});
		$("#mdEditVisitor").modal("show");
	});

	$(document).on("click", ".editBilling", function () {
		var id = $(this).data("id");
		$.ajax({
			type: "POST",
			url: "edit-billing/" + id,
			contentType: "application/json",
			dataType: "json",
			success: function (data) {
				console.log(data);
				$('[name="id"]').val(id);
				$('[name="fname"]').val(data.first_name);
				$('[name="lname"]').val(data.last_name);
				$('[name="email"]').val(data.email);
				$('[name="phone"]').val(data.phone);
				$('[name="country"]').val(data.country);
				$('[name="address"]').val(data.address);
			},
		});
		$("#mdEditBilling").modal("show");
	});

	$(document).on("click", ".editProfile", function () {
		$(this).hide();
		$(".editPassword").hide();
		$(".profile").removeClass("hide").addClass("show");
		$(".lb-profile").removeClass("show").addClass("hide");
		$(".cancelProfile").html(
			'<span role="button" class="badge badge-danger ml-2 cancProfile"><i class="fas fa-times"></i> Cancel</span>'
		);
		$(".btnProfile").html(
			'<label class="col-sm-2 col-form-label" style="color: transparent;">Phone</label><div class="col-sm-5"><button type="submit" class="btn btn-primary">Update</button></div>'
		);
	});

	$(document).on("click", ".cancProfile", function () {
		$(this).addClass("hide");
		$(".profile").removeClass("show").addClass("hide");
		$(".lb-profile").addClass("show");
		$(".btnProfile").html("");
		$(".editProfile, .editPassword").show();
	});

	$(document).on("click", ".editPassword", function () {
		$(".profile-page").hide();
		$(".password-page").show();
	});

	$(document).on("click", ".cancPassword", function () {
		$(".password-page").hide();
		$(".profile-page").show();
	});

	$.ajax({
		type: "POST",
		url: "get-datamember-json",
		contentType: "application/json",
		dataType: "json",
		success: function (data) {
			console.log(data);
		},
	});
});

function openChat() {
	window.open(
		"./assets/plugin/chatapp/index.php",
		"_blank",
		"toolbar=yes,scrollbars=yes,resizable=yes,top=120,left=400,width=700,height=750"
	);
}
