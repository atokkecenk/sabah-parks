<?php
session_start();
include_once "php/config.php";
if (!isset($_SESSION['unique_id'])) {
  header("location: login.php");
}

$cek = mysqli_fetch_assoc(mysqli_query($conn, "SELECT
                                            * 
                                            FROM
                                            users 
                                            WHERE
                                            unique_id = '$_SESSION[unique_id]'"));
$cek2 = mysqli_fetch_assoc(mysqli_query($conn, "SELECT
                                                COUNT( id ) AS hitung 
                                                FROM
                                                `tb_cs` 
                                                WHERE
                                                email = '$cek[email]' 
                                                AND na = 1"));
$image = (int) $cek2['hitung'] == 1 ? 'support.png' : 'user.png';
?>
<?php include_once "header.php"; ?>

<body>
  <div class="wrapper">
    <section class="users">
      <header>
        <div class="content">
          <?php
          $sql = mysqli_query($conn, "SELECT * FROM users WHERE unique_id = {$_SESSION['unique_id']}");
          if (mysqli_num_rows($sql) > 0) {
            $row = mysqli_fetch_assoc($sql);
          }
          ?>
          <!-- <img src="php/images/<?php echo $row['img']; ?>" alt=""> -->
          <img src="http://localhost/project/booking-front/img/<?= $image ?>" alt="">
          <div class="details">
            <span><?php echo $row['fname'] . " " . $row['lname'] ?></span>
            <p><?php echo $row['status']; ?></p>
          </div>
        </div>
        <a href="php/logout.php?logout_id=<?php echo $row['unique_id']; ?>" class="logout">Logout</a>
      </header>
      <div class="search">
        <span class="text">Select an user to start chat</span>
        <input type="text" placeholder="Enter name to search...">
        <button><i class="fas fa-search"></i></button>
      </div>
      <div class="users-list">

      </div>
    </section>
  </div>

  <script src="javascript/users.js"></script>

</body>

</html>