<?php
session_start();
if (isset($_SESSION['unique_id'])) {
    include_once "config.php";
    $outgoing_id = $_SESSION['unique_id'];
    $incoming_id = mysqli_real_escape_string($conn, $_POST['incoming_id']);
    $output = "";
    $cek = mysqli_fetch_assoc(mysqli_query($conn, "SELECT
                                            * 
                                            FROM
                                            users 
                                            WHERE
                                            unique_id = '$outgoing_id'"));
    $cek2 = mysqli_fetch_assoc(mysqli_query($conn, "SELECT
                                                COUNT( id ) AS hitung 
                                                FROM
                                                `tb_cs` 
                                                WHERE
                                                email = '$cek[email]' 
                                                AND na = 1"));
    $image = (int) $cek2['hitung'] == 1 ? 'support.png' : 'user.png';
    $sql = "SELECT * FROM messages LEFT JOIN users ON users.unique_id = messages.outgoing_msg_id
                WHERE (outgoing_msg_id = {$outgoing_id} AND incoming_msg_id = {$incoming_id})
                OR (outgoing_msg_id = {$incoming_id} AND incoming_msg_id = {$outgoing_id}) ORDER BY msg_id";
    $query = mysqli_query($conn, $sql);
    if (mysqli_num_rows($query) > 0) {
        while ($row = mysqli_fetch_assoc($query)) {
            if ($row['outgoing_msg_id'] === $outgoing_id) {
                $output .= '<div class="chat outgoing">
                                <div class="details">
                                    <p>' . $row['msg'] . '</p>
                                </div>
                                </div>';
            } else {
                $output .= '<div class="chat incoming">
                                <img src="http://localhost/project/booking-front/img/' . $image . '" alt="">
                                <div class="details">
                                    <p>' . $row['msg'] . '</p>
                                </div>
                                </div>';
            }
        }
    } else {
        $output .= '<div class="text">No messages are available. Once you send message they will appear here.</div>';
    }
    echo $output;
} else {
    header("location: ../login.php");
}
