<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_report', 'mRept');
    }

    function all_member()
    {
        $list = $this->mRept->get_datatables_member();
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->first_name;
            $row[] = $item->last_name;
            $row[] = $item->country;
            $row[] = $item->email;
            $row[] = $item->phone;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mRept->count_all(),
            "recordsFiltered" => $this->mRept->count_filtered(),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function all_order($date_togo)
    {
        $list = $this->mRept->get_datatables_odr($date_togo);
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            switch ($item->status) {
                case 'PAID IPAY88':
                case 'PAID (Manual)':
                    $sts = '<span class="text-success">' . $item->status . '</span>';
                    break;
                case 'UNPAID':
                    $sts = '<span class="text-warning">' . $item->status . '</span>';
                    break;
                case 'CANCEL':
                    $sts = '<span class="text-danger">' . $item->status . '</span>';
                    break;
            }
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '<b>'.$item->billing_id.'</b>';
            $row[] = $item->createdate;
            $row[] = $item->destname;
            $row[] = $item->ticketdatefrom;
            $row[] = $item->total_pax;
            $row[] = $item->first_name;
            $row[] = $item->last_name;
            $row[] = $sts;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mRept->count_all_odr(),
            "recordsFiltered" => $this->mRept->count_filtered_odr($date_togo),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function all_holder_approve($date_togo)
    {
        $list = $this->mRept->get_datatables_happv($date_togo);
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            switch ($item->ket_approve) {
                case 'APPROVED':
                    $sts = '<span class="text-success">' . $item->ket_approve . '</span>';
                    break;
                case '-':
                    $sts = '<i class="fas fa-check-square fa-2x mr-2 text-success modalApprove" role="button" title="Approve" data-status="2" data-id=' . $item->id . '></i><i class="fas fa-times-circle fa-2x text-danger modalApprove" role="button" title="Rejected" data-status="3" data-id=' . $item->id . '></i>';
                    break;
                case 'REJECTED':
                    $sts = '<span class="text-danger">' . $item->ket_approve . '</span>';
                    break;
            }
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '<b>'.$item->billing_id.'</b>';
            $row[] = $item->createdate;
            $row[] = $item->destname;
            $row[] = $item->ticketdatefrom;
            $row[] = $item->total_pax;
            $row[] = $item->first_name;
            $row[] = $item->last_name;
            $row[] = $sts;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mRept->count_all_happv(),
            "recordsFiltered" => $this->mRept->count_filtered_happv($date_togo),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function all_billing($date_togo)
    {
        $list = $this->mRept->get_datatables_bill($date_togo);
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            switch ($item->ket_status) {
                case 'PAID':
                    $sts = '<span class="text-success">' . $item->ket_status . '</span>';
                    break;
                case '-':
                    $sts = '<span class="text-warning">' . $item->ket_status . '</span>';
                    break;
                case 'CANCEL':
                    $sts = '<span class="text-danger">' . $item->ket_status . '</span>';
                    break;
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '<b>'.$item->billing_id.'</b>';
            $row[] = $item->createdate;
            $row[] = $item->first_name;
            $row[] = $item->last_name;
            $row[] = $item->country;
            $row[] = $item->address;
            $row[] = $item->email;
            $row[] = $item->phone;
            $row[] = $item->destname;
            $row[] = $item->ticketdatefrom;
            $row[] = $item->receipt_id;
            $row[] = $item->pay_date;
            $row[] = $sts;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mRept->count_all_bill(),
            "recordsFiltered" => $this->mRept->count_filtered_bill($date_togo),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function all_sales($date_togo)
    {
        $list = $this->mRept->get_datatables_sales($date_togo);
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '<b>'.$item->billing_id.'</b>';
            $row[] = $item->receipt_id;
            $row[] = $item->destname;
            $row[] = $item->bank_name;
            $row[] = $item->bank_no;
            $row[] = $item->bank_acc;
            $row[] = $item->amount;
            $row[] = $item->remark;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mRept->count_all_sales(),
            "recordsFiltered" => $this->mRept->count_filtered_sales($date_togo),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function all_ipay($date_togo)
    {
        $list = $this->mRept->get_datatables_ipay88($date_togo);
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            switch ($item->ket_status) {
                case 'SUCCESS':
                    $sts = '<span class="text-success">' . $item->ket_status . '</span>';
                    break;
                case 'FAILED':
                    $sts = '<span class="text-danger">' . $item->ket_status . '</span>';
                    break;
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->RefNo;
            $row[] = $item->Amount;
            $row[] = $item->Currency;
            $row[] = $item->Remark;
            $row[] = $item->TransId;
            $row[] = $item->AuthCode;
            $row[] = $item->ErrDesc;
            $row[] = $item->Signature;
            $row[] = $item->CCName;
            $row[] = $item->CCNo;
            $row[] = $item->S_bankname;
            $row[] = $item->S_country;
            $row[] = $sts;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mRept->count_all_ipay88(),
            "recordsFiltered" => $this->mRept->count_filtered_ipay88($date_togo),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function all_paid($option, $date_togo)
    {
        $list = $this->mRept->get_datatables_paid($option, $date_togo);
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            switch ($item->status) {
                case 'PAID IPAY88':
                case 'PAID (Manual)':
                    $sts = '<span class="text-success">' . $item->status . '</span>';
                    break;
                case 'UNPAID':
                    $sts = '<span class="text-warning">' . $item->status . '</span>';
                    break;
                case 'CANCEL':
                    $sts = '<span class="text-danger">' . $item->status . '</span>';
                    break;
            }
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '<b>'.$item->billing_id.'</b>';
            $row[] = $item->createdate;
            $row[] = $item->destname;
            $row[] = $item->ticketdatefrom;
            $row[] = $item->total_pax;
            $row[] = $item->first_name;
            $row[] = $item->last_name;
            $row[] = $sts;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mRept->count_all_paid(),
            "recordsFiltered" => $this->mRept->count_filtered_paid($option, $date_togo),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function all_climber($date_togo)
    {
        $list = $this->mRept->get_datatables_change($date_togo);
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            switch ($item->ticket_status) {
                case 'OPEN':
                    $sts = '<span class="text-success">' . $item->ticket_status . '</span>';
                    break;
                case 'CHECK IN':
                    $sts = '<span class="text-primary">' . $item->ticket_status . '</span>';
                    break;
                case 'EXPIRED':
                    $sts = '<span class="text-danger">' . $item->ticket_status . '</span>';
                    break;
            }
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '<b>'.$item->billing_id.'</b>';
            $row[] = $item->createdate;
            $row[] = $item->destname;
            $row[] = $item->date_before;
            $row[] = $item->date_togo;
            $row[] = $item->n_visname;
            $row[] = $sts;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mRept->count_all_change(),
            "recordsFiltered" => $this->mRept->count_filtered_change($date_togo),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function all_cancel_ps($date_togo)
    {
        $list = $this->mRept->get_datatables_cancel($date_togo);
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
         
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '<b>'.$item->billing_id.'</b>';
            $row[] = $item->date_create;
            $row[] = $item->destname;
            $row[] = $item->date_togo;
            $row[] = $item->new_name;
            $row[] = $item->note;
            $row[] = $item->nama_lengkap;
            $row[] = $item->upd_time;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mRept->count_all_cancel(),
            "recordsFiltered" => $this->mRept->count_filtered_cancel($date_togo),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    public function rp_member()
    {
        $this->renderPage('modul/report/rp-member');
    }

    public function rp_order()
    {
        $this->renderPage('modul/report/rp-booking');
    }

    public function rp_booking_holder()
    {
        $this->renderPage('modul/report/rp-pr-holder');
    }

    public function rp_billing()
    {
        $this->renderPage('modul/report/rp-billing');
    }

    public function rp_sales()
    {
        $this->renderPage('modul/report/rp-sales');
    }

    public function rp_ipay()
    {
        $this->renderPage('modul/report/rp-ipay');
    }

    public function rp_paid()
    {
        $this->renderPage('modul/report/rp-paid');
    }

    public function rp_climb_change()
    {
        $this->renderPage('modul/report/rp-climb-change');
    }

    public function rp_cancel_climber()
    {
        $this->renderPage('modul/report/rp-cancel-climber');
    }
}
