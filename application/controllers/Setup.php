<?php
ini_set('max_execution_time', 0);
ini_set('memory_limit', '2048M');
defined('BASEPATH') or exit('No direct script access allowed');

class Setup extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_setup', 'mSett');
    }

    public function vw_destination()
    {
        $data['data'] = $this->mSett->get_destination();
        $data['avb1'] = $this->mSett->avalaible_quota('SB001', date('Y-m-d'))->row()->hitung;
        $data['avb2'] = $this->mSett->avalaible_quota('SB002', date('Y-m-d'))->row()->hitung;
        $data['SB001'] = $this->db->get_where('stp_dest', ['destid' => 'SB001'])->row();
        $data['SB002'] = $this->db->get_where('stp_dest', ['destid' => 'SB002'])->row();
        $data['detailsb1'] = $this->db->get_where('stp_dest_price', ['destid' => 'SB001'])->result();
        $data['detailsb2'] = $this->db->get_where('stp_dest_price', ['destid' => 'SB002'])->result();
        $this->renderPage('modul/setup/destination', $data);
    }

    public function vw_gallery()
    {
        $data['data'] = $this->mSett->get_gallery();
        $data['type'] = $this->db->get('stp_dest_type');
        $this->renderPage('modul/setup/gallery', $data);
    }

    public function vw_user()
    {
        $data['data'] = $this->mSett->get_users();
        $data['role'] = $this->db->get('rb_users_role');
        $this->renderPage('modul/setup/user', $data);
    }
    public function vw_user_role()
    {
        $data['data_role'] = $this->db->get('rb_users_role')->result();
        $data['role'] = $this->db->get('rb_users_role');
        $this->renderPage('modul/setup/user-role', $data);
    }

    public function vw_profile()
    {
        $id = $this->input->get('iu');
        $data['data'] = $this->db->query("SELECT
                                    b.role_name,
                                    a.* 
                                FROM
                                    `rb_users` a
                                    LEFT JOIN rb_users_role b ON a.id_role = b.id 
                                WHERE
                                    SHA1( a.id_user ) = '$id'")->row();
        $this->renderPage('modul/setup/profile', $data);
    }

    public function update_profile()
    {
        $iu = $this->session->id_user;
        $category = $this->input->post('category');
        $nama = $this->input->post('nama');
        $email = $this->input->post('email');
        $telp = $this->input->post('telp');

        if ($category == 'pwd') {
            $pwd = $this->input->post('password');
            $cfpwd = $this->input->post('cfpassword');

            if ($pwd !== $cfpwd) {
                $this->session->set_flashdata('error', 'Password and confirm password must be the same !!');
                redirect('profile?iu=' . sha1($iu) . '&act=2', 'refresh');
            } else {
                $pass = md5($pwd);
                $upd = $this->db->query("UPDATE rb_users SET new_password='$pass' WHERE id_user='$iu'");
                if ($upd == true) {
                    $this->session->set_flashdata('success', 'Update password successfully..');
                    redirect('logout', 'refresh');
                } else {
                    $this->session->set_flashdata('error', 'Oops, something wrong !!');
                    redirect('logout', 'refresh');
                }
            }
        } else {

            $config['upload_path']          = 'assets/img/photo/';
            $config['allowed_types']        = 'jpeg|jpg|png|JPEG|JPG|PNG';
            $config['max_size']             = 0;
            $config['max_width']            = 0;
            $config['max_height']           = 0;
            $config['remove_space']         = TRUE;
            $config['encrypt_name']         = TRUE;
            $this->load->library('upload', $config);

            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
            $this->form_validation->set_rules('nama', 'Full Name', 'required');
            $this->form_validation->set_rules('email', 'Email Address', 'required');
            $this->form_validation->set_rules('telp', 'Telp', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', validation_errors());
                redirect('profile?iu=' . sha1($iu), 'refresh');
            } else {
                if (!$this->upload->do_upload('foto')) {
                    $this->db->where('id_user', $iu);
                    $upd = $this->db->update('rb_users', [
                        'nama_lengkap' => $nama,
                        'email' => $email,
                        'no_telpon' => $telp
                    ]);
                } else {
                    $upload_data = $this->upload->data();
                    $file_name = $upload_data['file_name'];
                    $this->db->where('id_user', $iu);
                    $upd = $this->db->update('rb_users', [
                        'nama_lengkap' => $nama,
                        'email' => $email,
                        'no_telpon' => $telp,
                        'foto' => $file_name
                    ]);
                }

                if ($upd == true) {
                    $this->session->set_flashdata('success', 'Update personal data successfully..');
                    redirect('profile?iu=' . sha1($iu), 'refresh');
                } else {
                    $this->session->set_flashdata('error', 'Oops, something wrong !!');
                    redirect('profile?iu=' . sha1($iu), 'refresh');
                }
            }
        }
    }

    function set_user_role()
    {
        $id_role = $this->input->post('id_role');
        $list = $this->mSett->get_datatables();
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {

            $cek = $this->mSett->cek_active_menu($item->id, $id_role)->row()->hitung;
            $cklist = ($cek == '1') ? 'checked' : null;

            $no++;
            $row = array();
            $row[] = '<input type="text" name="id_role" value="' . $id_role . '">' . $no;
            $row[] = '<input type="text" name="menu[]" value="' . $item->id . '">' . $item->menu;
            $row[] = '<div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="active[]" id="customCheck' . $no . '" ' . $cklist . '>
                        <label class="custom-control-label" for="customCheck' . $no . '"> Yes</label>
                    </div>';
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mSett->count_all(),
            "recordsFiltered" => $this->mSett->count_filtered(),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    public function upload_gallery()
    {
        $id = $this->input->post('id');
        $type = $this->input->post('type');
        $desc = $this->input->post('desc');
        $active = $this->input->post('active');
        $act = ((int) $active == 1) ? 1 : 0;

        $config['upload_path']          = 'assets/img/';
        $config['allowed_types']        = 'jpeg|jpg|png|JPEG|JPG|PNG';
        $config['max_size']             = 0;
        $config['remove_space']         = TRUE;
        $config['encrypt_name']         = TRUE;
        $this->load->library('upload', $config);
        if ($id == '') {
            if (!$this->upload->do_upload('file')) {
                $this->session->set_flashdata('error', 'Oops, something wrong !!');
                redirect('gallery', 'refresh');
            } else {
                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                $this->db->insert('stp_gallery', [
                    'path' => $file_name,
                    'description' => $desc,
                    'type' => $type,
                    'createdate' => date('Y-m-d'),
                    'updateid' => 1,
                    'status' => $act
                ]);
            };

            $this->session->set_flashdata('success', 'Upload to gallery successfully..');
            redirect('gallery', 'refresh');
        } else {
            if (!$this->upload->do_upload('file')) {
                $this->db->where('id', $id);
                $this->db->update('stp_gallery', [
                    'description' => $desc,
                    'type' => $type,
                    'createdate' => date('Y-m-d'),
                    'updateid' => 1,
                    'status' => $act
                ]);

                $this->session->set_flashdata('success', 'Edit data gallery successfully..');
                redirect('gallery', 'refresh');
            } else {
                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                $this->db->where('id', $id);
                $this->db->update('stp_gallery', [
                    'path' => $file_name,
                    'description' => $desc,
                    'type' => $type,
                    'createdate' => date('Y-m-d'),
                    'updateid' => 1,
                    'status' => $act
                ]);

                $this->session->set_flashdata('success', 'Edit data gallery successfully..');
                redirect('gallery', 'refresh');
            };
        }
    }

    public function save_menu()
    {
        $id_role = $this->input->post('id_role');
        $idmenu = $this->input->post('menu');
        $active = $this->input->post('active');

        $this->db->where('id_role', $id_role);
        $this->db->delete('stp_role_menu');
        foreach ($idmenu as $key => $value) {
            $act = (isset($active[$key]) && $active[$key] !== '') ? '1' : '0';
            // echo $idmenu[$key].' --> '.$act.'<br>';
            $this->db->insert('stp_role_menu', [
                'id_role' => $id_role,
                'id_menu' => $idmenu[$key],
                'status' => $act
            ]);
        }
        $this->session->set_flashdata('success', 'Setting credential menu successfully..');
        redirect('user-role', 'refresh');
    }

    public function save_user()
    {
        $id = $this->input->post('iduser');
        $role = $this->input->post('role');
        $username = $this->input->post('username');
        $fullname = $this->input->post('fullname');
        $active = $this->input->post('active');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $pwd = $this->input->post('password');
        $act = ($active == '1') ? 'Y' : 'N';

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('role', 'User Role', 'required');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('fullname', 'Fullname', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('phone', 'Phone Number', 'required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');


        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('user', 'refresh');
        } else {
            if ($id == '') {
                $ins = $this->db->insert('rb_users', [
                    'username' => $username,
                    'new_password' => md5($pwd),
                    'password' => '-',
                    'nama_lengkap' => $fullname,
                    'email' => $email,
                    'no_telpon' => $phone,
                    'jabatan' => '-',
                    'level' => '-',
                    'aktif' => $act,
                    'id_role' => $role
                ]);

                if ($ins == true) {
                    $this->session->set_flashdata('success', 'Created user successfully..');
                    redirect('user', 'refresh');
                } else {
                    $this->session->set_flashdata('error', 'Oops, something wrong !!');
                    redirect('user', 'refresh');
                }
            } else {
                $this->db->where('id_user', $id);
                $upd = $this->db->update('rb_users', [
                    'username' => $username,
                    'nama_lengkap' => $fullname,
                    'email' => $email,
                    'no_telpon' => $phone,
                    'aktif' => $act,
                    'id_role' => $role
                ]);

                if ($upd == true) {
                    $this->session->set_flashdata('success', 'Updated user successfully..');
                    redirect('user', 'refresh');
                } else {
                    $this->session->set_flashdata('error', 'Oops, something wrong !!');
                    redirect('user', 'refresh');
                }
            }
        }
    }

    function save_edit_destination()
    {
        $name = $this->input->post('name');
        $loc_quota = $this->input->post('loc_quota');
        $int_quota = $this->input->post('int_quota');
        $loc_quota = $this->input->post('loc_quota');
        $int_quota = $this->input->post('int_quota');
        $nominal1 = $this->input->post('nominal1');
        $nominal2 = $this->input->post('nominal2');
        $nominal3 = $this->input->post('nominal3');
        $nominal4 = $this->input->post('nominal4');
        $destid = $this->input->post('destid');
        $desc = $this->input->post('desc');
        $acc_a = $this->input->post('accomodation[a]');
        $acc_b = $this->input->post('accomodation[b]');
        $acc_c = $this->input->post('accomodation[c]');
        $acc_d = $this->input->post('accomodation[d]');
        $perm_a = $this->input->post('permit[a]');
        $perm_b = $this->input->post('permit[b]');
        $perm_c = $this->input->post('permit[c]');
        $perm_d = $this->input->post('permit[d]');
        $insur_a = $this->input->post('insurance[a]');
        $insur_b = $this->input->post('insurance[b]');
        $insur_c = $this->input->post('insurance[c]');
        $insur_d = $this->input->post('insurance[d]');
        $serv_a = $this->input->post('service[a]');
        $serv_b = $this->input->post('service[b]');
        $serv_c = $this->input->post('service[c]');
        $serv_d = $this->input->post('service[d]');

        if ($destid == 'SB001') {
            $upd = $this->db->update('stp_dest', [
                'destname' => $name,
                'description' => $desc,
                'loc_price_18above' => $nominal1,
                'loc_price_18below' => $nominal2,
                'loc_quota' => $loc_quota,
                'int_quota' => $int_quota,
            ], ['destid' => $destid]);

            $upd .= $this->db->update('stp_dest_price', [
                'loc_price_18above' => $acc_a,
                'loc_price_18below' => $acc_b
            ], ['destid' => $destid, 'id' => 738]);

            $upd .= $this->db->update('stp_dest_price', [
                'loc_price_18above' => $perm_a,
                'loc_price_18below' => $perm_b
            ], ['destid' => $destid, 'id' => 739]);

            $upd .= $this->db->update('stp_dest_price', [
                'loc_price_18above' => $insur_a,
                'loc_price_18below' => $insur_b
            ], ['destid' => $destid, 'id' => 740]);

            $upd .= $this->db->update('stp_dest_price', [
                'loc_price_18above' => $serv_a,
                'loc_price_18below' => $serv_b
            ], ['destid' => $destid, 'id' => 741]);

            $arr = [];
            if ($upd == true) {
                $arr['msg'] = 'success';
            } else {
                $arr['msg'] = 'no';
            }
            header('Content-Type: application/json');
            echo json_encode($arr);
        } elseif ($destid == 'SB002') {

            $upd = $this->db->update('stp_dest', [
                'destname' => $name,
                'description' => $desc,
                'loc_price_18above' => $nominal1,
                'loc_price_18below' => $nominal2,
                'int_price_18above' => $nominal3,
                'int_price_18below' => $nominal4,
                'loc_quota' => $loc_quota,
                'int_quota' => $int_quota,
            ], ['destid' => $destid]);

            $upd .= $this->db->update('stp_dest_price', [
                'loc_price_18above' => $acc_a,
                'loc_price_18below' => $acc_b,
                'int_price_18above' => $acc_c,
                'int_price_18below' => $acc_d,
            ], ['destid' => $destid, 'id' => 734]);

            $upd .= $this->db->update('stp_dest_price', [
                'loc_price_18above' => $perm_a,
                'loc_price_18below' => $perm_b,
                'int_price_18above' => $perm_c,
                'int_price_18below' => $perm_d,
            ], ['destid' => $destid, 'id' => 735]);

            $upd .= $this->db->update('stp_dest_price', [
                'loc_price_18above' => $insur_a,
                'loc_price_18below' => $insur_b,
                'int_price_18above' => $insur_c,
                'int_price_18below' => $insur_d,
            ], ['destid' => $destid, 'id' => 736]);

            $upd .= $this->db->update('stp_dest_price', [
                'loc_price_18above' => $serv_a,
                'loc_price_18below' => $serv_b,
                'int_price_18above' => $perm_c,
                'int_price_18below' => $perm_d,
            ], ['destid' => $destid, 'id' => 737]);

            $arr = [];
            if ($upd == true) {
                $arr['msg'] = 'success';
            } else {
                $arr['msg'] = 'no';
            }
            header('Content-Type: application/json');
            echo json_encode($arr);
        }
    }

    function save_user_role()
    {
        $id = $this->input->post('iduserRole');
        $userrole = $this->input->post('userrole');
        if ($id == '' || empty($id)) {
            $this->db->insert('rb_users_role', ['role_name' => $userrole]);
            $this->session->set_flashdata('success', 'Add user role credential successfully..');
            redirect('user-role', 'refresh');
        } else {
            $this->db->where('id', $id);
            $this->db->update('rb_users_role', ['role_name' => $userrole]);
            $this->session->set_flashdata('success', 'Update user role credential successfully..');
            redirect('user-role', 'refresh');
        }
    }

    function delete_user_role($id)
    {
        $this->db->where('id', $id);
        $del = $this->db->delete('rb_users_role');
        if ($del == true) {
            $this->session->set_flashdata('success', 'Delete user role credential successfully..');
            redirect('user-role', 'refresh');
        }
    }

    function hitung_avb($destid, $date)
    {
        $data = $this->mSett->avalaible_quota($destid, $date)->row();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function hitung_total_destbyId($id)
    {
        $data = $this->db->query("SELECT destid, destname, loc_quota, int_quota,( loc_quota + int_quota ) AS `sum`
        FROM
            `stp_dest` 
        WHERE
            destid = '$id'")->row();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function edit_gallery($id)
    {
        $data = $this->mSett->edit_gallery($id)->row();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function edit_destination($id)
    {
        $data = $this->db->query("SELECT * from stp_dest where id = '$id'")->row();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function edit_user($id)
    {
        $data = $this->mSett->edit_users($id)->row();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function data_user_role()
    {
        $data = $this->db->order_by('order_user_role', 'asc')->get_where('stp_menu', ['status' => 1])->result();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function edit_user_role($id)
    {
        $data = $this->db->get_where('rb_users_role', ['id' => $id])->row();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function cek_active_menu()
    {
        $id_role = $this->input->post('id_role');
        $id_menu = $this->input->post('id_menu');
        $data = $this->mSett->cek_active_menu($id_menu, $id_role)->row();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function delete_gallery($id)
    {
        $get = $this->mSett->edit_gallery($id)->row();
        $file = 'assets/img/' . $get->path;
        unlink($file);
        $this->db->delete('stp_gallery', array('id' => $id));
        $this->session->set_flashdata('success', 'Delete data gallery successfully..');
        redirect('gallery', 'refresh');
    }

    function delete_user($id)
    {
        $this->db->delete('rb_users', ['id_user' => $id]);
        $this->session->set_flashdata('success', 'Delete user successfully..');
        redirect('user', 'refresh');
    }

    function randString($n)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }

    function lock_quota()
    {
        $id = $this->input->post('id');
        $destid = $this->input->post('destid');
        $jumlah = $this->input->post('jumlah');
        $date = $this->input->post('date');
        $rand = '#' . $this->randString(8);

        $a = 0;
        for ($i = 1; $i <= (int) $jumlah; $i++) {
            $cek = $this->db->get_where('trx_billing_visitor', ['billing_id' => $rand])->num_rows();
            if ($cek == '0') {
                $bi = $rand;
            } else {
                $x = '#' . $this->randString(8);
                $cek2 = $this->db->get_where('trx_billing_visitor', ['billing_id' => $x])->num_rows();
                if ($cek2 == '0') {
                    $bi = $x;
                } else {
                    $bi = '#' . date('Y') . date('m') . date('d');
                }
            }

            $this->db->insert('trx_billing_visitor', [
                'billing_id' => $rand,
                'buyerid' => 'lock',
                'destid' => $destid,
                'ticketdatefrom' => $date,
                'ticketdateto' => $date,
                'visitor_name' => 'Sabah Parks',
                'id_number' => '-',
                'email' => '-',
                'phone' => '-',
                'nationality' => 'MALAYSIA',
                'gender' => 0,
                'age' => 0,
                'status' => 1
            ]);
            $a++;
        }

        $this->db->insert('trx_billing', [
            'billing_id' => $rand,
            'buyerid' => 'lock',
            'first_name' => 'Sabah',
            'last_name' => 'Parks',
            'company_name' => 'Sabah Parks',
            'country' => 'MALAYSIA',
            'address' => '-',
            'city' => '-',
            'postcode' => '-',
            'email' => '-',
            'phone' => '-',
            'total_pax' => $jumlah,
            'total_cost' => '0',
            'status' => 1,
            'pr_holder' => 2,
            'status_approved' => 1,
        ]);

        $this->db->insert('trx_ticket', [
            'ticket_id' => '-',
            'receipt_id' => '-',
            'billing_id' => $rand,
            'buyerid' => 'lock',
            'destid' => $destid,
            'ticketdatefrom' => $date,
            'ticketdateto' => $date,
            'visitor_name' => 'Sabah Parks',
            'id_number' => '-',
            'email' => '-',
            'phone' => '-',
            'nationality' => 'MALAYSIA',
            'gender' => '0',
            'age' => '0',
            'status' => 2
        ]);

        if ($a !== 0) {
            $this->session->set_flashdata('success', 'Save lock quota successfully..');
            redirect('destination', 'refresh');
        } else {
            $this->session->set_flashdata('error', 'Oops, something wrong !!');
            redirect('destination', 'refresh');
        }
    }
}
