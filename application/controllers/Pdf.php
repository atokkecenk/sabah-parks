<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pdf extends CI_Controller
{

    public function trial()
    {
        $mpdf = new \Mpdf\Mpdf();
        $html = $this->load->view('modul/pdf/test', [], true);
        $mpdf->WriteHTML($html);
        $mpdf->Output('report.pdf', 'I');
    }
}
