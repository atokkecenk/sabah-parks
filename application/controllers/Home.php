<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_transaction', 'mTrans');
		$this->load->model('M_system', 'mSys');
	}

	public function index()
	{
		// bar chart
		$get = $this->mSys->diagram()->result();
		$destname = '';
		$jumlah = null;
		foreach ($get as $val) {
			$dest = $val->destname;
			$destname .=  "'$dest'" . ", ";
			$jum = $val->total;
			$jumlah .= "$jum" . ", ";
		}
		// bar chart

		// line chart
		error_reporting(0);
		$year = strtotime(date('Y-01-01') . ' -1 year');
		$end_year = strtotime(date('Y-12-31') . ' -1 year');
		$before = date('Y-m-d', $year);
		$before_end = date('Y-m-d', $end_year);
		$current = date('Y-01-01');
		$current_end = date('Y-12-31');
		$before_query = $this->mSys->sales_comparison($before, $before_end);
		$before_data = $before_query->result();
		$current_query = $this->mSys->sales_comparison($current, $current_end);
		$current_data =    $current_query->result();
		$labels = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

		$bef_group = [];
		$bef_month = [];
		$bef_tot = [];
		foreach ($before_data as $bd) {
			$arr = [];
			$bef_month[] = (int) $bd->month - 1;
			$bef_tot[] = (int) $bd->total;
			$arr['month'] = (int) $bd->month - 1;
			$arr['total'] = (int) $bd->total;
			$bef_group[] = $arr;
		}

		$cur_group = [];
		$cur_month = [];
		$cur_tot = [];
		foreach ($current_data as $cd) {
			$arr = [];
			$cur_month[] = (int) $cd->month - 1;
			$cur_tot[] = (int) $cd->total;
			$arr['month'] = (int) $cd->month - 1;
			$arr['total'] = (int) $cd->total;
			$cur_group[] = $arr;
		}
		$fix_bef = [];
		$fix_cur = [];
		$jml = sizeof($labels);
		$a = 0;
		$bf = null;
		// before
		for ($i = 0; $i < (int) $jml; $i++) {
			if (in_array($i, $bef_month)) {
				$fix_bef[] = $bef_group[$a]['total'];
				$bf .=  $bef_group[$a]['total'] . ", ";
				$a++;
			} else {
				$fix_bef[] = 0;
				$bf .= "0" . ", ";
			}
		}

		$b = 0;
		$cr = null;
		// current 
		for ($i = 0; $i < (int) $jml; $i++) {
			if (in_array($i, $cur_month)) {
				$fix_cur[] = $cur_group[$b]['total'];
				$cr .= $cur_group[$b]['total'] . ", ";
				$b++;
			} else {
				$fix_cur[] = 0;
				$cr .= "0" . ", ";
			}
		}
		// line chart

		// pie chart
		$order = $this->db->count_all_results('trx_billing');
		$payment = $this->db->count_all_results('trx_receipt');
		$data['order'] = $order . ', ' . $payment;
		$data['labels_pie'] = '"Order", "Payment"';
		// pie chart

		// visitor chart
		$get = $this->mSys->visitor_chart()->result();
		$vs_labels = '';
		$vs_data = null;
		foreach ($get as $gt) {
			$vs_labels .=  '"' . $gt->country . '", ';
			$vs_data .= (int) $gt->total . ", ";
		}
		// visitor chart

		$data['destname'] = $destname;
		$data['jumlah'] = $jumlah;
		$data['labels'] = $labels;
		$data['before'] = $bf;
		$data['current'] = $cr;
		$data['vs_labels'] = $vs_labels;
		$data['vs_data'] = $vs_data;

		$data['jml_member'] = $this->db->get_where('stp_member', ['status' => 1])->num_rows();
		$data['jml_sales'] = $this->db->select('SUM( amount ) AS total_amount')->from('trx_receipt')->get()->row()->total_amount;
		$data['jml_ticket'] = $this->db->get('trx_ticket')->num_rows();
		$this->renderPage('modul/dashboard', $data);
	}

	public function sales_comparasion()
	{
		error_reporting(0);
		$year = strtotime(date('Y-01-01') . ' -1 year');
		$end_year = strtotime(date('Y-12-31') . ' -1 year');
		$before = date('Y-m-d', $year);
		$before_end = date('Y-m-d', $end_year);
		$current = date('Y-01-01');
		$current_end = date('Y-12-31');
		$before_query = $this->mSys->sales_comparison($before, $before_end);
		$before_data = $before_query->result();
		$current_query = $this->mSys->sales_comparison($current, $current_end);
		$current_data =    $current_query->result();
		$labels = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

		$bef_group = [];
		$bef_month = [];
		$bef_tot = [];
		foreach ($before_data as $bd) {
			$arr = [];
			$bef_month[] = (int) $bd->month - 1;
			$bef_tot[] = (int) $bd->total;
			$arr['month'] = (int) $bd->month - 1;
			$arr['total'] = (int) $bd->total;
			$bef_group[] = $arr;
		}

		$cur_group = [];
		$cur_month = [];
		$cur_tot = [];
		foreach ($current_data as $cd) {
			$arr = [];
			$cur_month[] = (int) $cd->month - 1;
			$cur_tot[] = (int) $cd->total;
			$arr['month'] = (int) $cd->month - 1;
			$arr['total'] = (int) $cd->total;
			$cur_group[] = $arr;
		}
		$fix_bef = [];
		$fix_cur = [];
		$jml = sizeof($labels);
		$a = 0;
		// before
		for ($i = 0; $i < (int) $jml; $i++) {
			if (in_array($i, $bef_month)) {
				$fix_bef[] = $bef_group[$a]['total'];
				$a++;
			} else {
				$fix_bef[] = 0;
			}
		}

		$b = 0;
		// current 
		for ($i = 0; $i < (int) $jml; $i++) {
			if (in_array($i, $cur_month)) {
				$fix_cur[] = $cur_group[$b]['total'];
				$b++;
			} else {
				$fix_cur[] = 0;
			}
		}

		$res['labels'] = $labels;
		$res['before'] = $fix_bef;
		$res['current'] = $fix_cur;
		header('Content-Type: application/json');
		echo json_encode($res);
	}

	function visitor_chart()
	{
		$get = $this->mSys->visitor_chart()->result();
		$labels = [];
		$data = [];
		foreach ($get as $gt) {
			$labels[] = $gt->country;
			$data[] = (int) $gt->total;
		}
		header('Content-Type: application/json');
		echo json_encode($data);
	}
}
