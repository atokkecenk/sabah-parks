<?php
ini_set('max_execution_time', 0);
ini_set('memory_limit', '2048M');
defined('BASEPATH') or exit('No direct script access allowed');

class SendEmail extends CI_Controller
{
    public function send()
    {
        $this->load->library('email');
        $config = array();
        $config['protocol']     = 'smtp';
        $config['smtp_host']    = 'mail.contohsite.com';
        $config['smtp_user']    = 'noreply@contohsite.com';
        $config['smtp_pass']    = 'IgRELxMDSNO+';
        $config['smtp_crypto']  = 'ssl';
        $config['mailtype']     = 'text';
        // $config['mailtype']     = 'html';
        $config['charset']          = 'utf-8';
        $config['send_multipart']   = FALSE;
        $config['wordwrap']         = TRUE;
        $config['priority']         = 1;
        // $config['smtp_timeout']     = 5;
        $config['smtp_port']        = 465;
        $config['crlf']             = "\r\n";
        $config['newline']          = "\r\n";
        $config['validate']         = FALSE;
        $this->email->initialize($config);
        // $this->email->set_newline("\r\n");

        $from_email = "noreply@contohsite.com";
        $to_email = $this->input->post('email');
        $pesan = $this->input->post('pesan');

        $broadcast = $this->db->get_where('trx_broadcast', ['id' => 1])->row();
        $member = $this->db->query("SELECT
                                    a.email,
                                    max( a.visitor_name ) AS visitor_name 
                                FROM
                                    trx_ticket a 
                                WHERE a.email in ('atokkecenk@gmail.com','sobatcoding@gmail.com')
                                -- GROUP BY
                                --     a.email
                                GROUP BY
                                -- 	a.email
                                a.id
                                    limit 2")->result();
        foreach ($member as $mbr) {

            $data = [
                'title' => $broadcast->title,
                'content' => $broadcast->content,
                'name' => $mbr->visitor_name
            ];
            // html
            $template = $this->load->view('modul/email/broadcast', $data, true);

            $this->email->clear(TRUE);
            //Load email library
            $this->email->from('noreply@contohsite.com', 'Contohsite');
            $this->email->to($mbr->email, $mbr->visitor_name);
            $this->email->subject($broadcast->blogtitle);
            $this->email->message($template);
            $this->email->send();
            // Send mail
            if ($this->email->send()) {
                echo 'success';
                // $this->session->set_flashdata("success", "Email send successfully..");
            } else {
                echo 'gagal';
                // $this->session->set_flashdata("error", "Email not send !!");
            }
        }
    }
}
