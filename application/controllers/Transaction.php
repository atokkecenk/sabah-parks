<?php
ini_set('max_execution_time', 0);
ini_set('memory_limit', '2048M');
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_transaction', 'mTrans');
    }

    function all_member()
    {
        $list = $this->mTrans->get_datatables_member();
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->first_name;
            $row[] = $item->last_name;
            $row[] = $item->country;
            $row[] = $item->email;
            $row[] = $item->phone;
            $row[] = '<i class="fas fa-info-circle fa-2x memberDetail" style="color: #f8538d;" role="button" data-id="' . $item->id . '" title="Detail info"></i>';
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mTrans->count_all(),
            "recordsFiltered" => $this->mTrans->count_filtered(),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function all_order($year, $date_togo)
    {
        $list = $this->mTrans->get_datatables_odr($year, $date_togo);
        $data = array();
        $no = @$_POST['start'];


        foreach ($list as $item) {
            switch ($item->status) {
                case 'PAID IPAY88':
                case 'PAID (Manual)':
                    $sts = '<span class="text-success">' . $item->status . '</span>';
                    $edit = '<a href="#"><i class="fas fa-edit text-danger ml-2 modalChangeDateTogo" title="Change Date To Go" data-destid=' . $item->destid . ' data-datetogo=' . $item->ticketdatefrom . ' data-idupd=' . $item->id_update . ' data-bill=' . $item->billing_id . '></i></a>';
                    $pmanual = '';
                    $cancel = '<i class="fas fa-times-circle text-danger ml-2 fa-2x cancelBooking" title="Cancel order" role="button" data-id="' . $item->id . '" data-bill="' . $item->billing_id . '"></i>';
                    break;
                case 'UNPAID':
                    $sts = '<span class="text-warning">' . $item->status . '</span>';
                    $edit = '';
                    $pmanual = '<i class="fas fa-wallet text-success ml-2 fa-2x paidManual" role="button" data-id="' . $item->id . '" title="Paid manual"></i>';
                    $cancel = '<i class="fas fa-times-circle text-danger ml-2 fa-2x cancelBooking" title="Cancel order" role="button" data-id="' . $item->id . '" data-bill="' . $item->billing_id . '"></i>';
                    break;
                case 'CANCEL':
                    $sts = '<span class="text-danger">' . $item->status . '</span>';
                    $edit = '';
                    $pmanual = '';
                    $cancel = '';
                    break;
            }
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '<i class="fas fa-info-circle fa-2x orderDetail" style="color: #f8538d;" role="button" data-id="' . $item->id . '" data-destid="' . $item->destid . '" data-datetogo="' . $item->ticketdatefrom . '" data-idupd="' . $item->id_update . '" data-bill="' . $item->billing_id . '" data-status="' . $item->status . '" title="Detail, History, Change Date, Resit, Cancel remark"></i>' . $pmanual . $cancel;
            $row[] = '<b>' . $item->billing_id . '</b>';
            $row[] = $item->createdate;
            $row[] = $item->destname;
            $row[] = $item->ticketdatefrom;
            $row[] = $item->total_pax;
            $row[] = $item->first_name;
            $row[] = $item->last_name;
            $row[] = $sts;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mTrans->count_all_odr(),
            "recordsFiltered" => $this->mTrans->count_filtered_odr($year, $date_togo),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function all_holder_approve($year, $date_togo)
    {
        $list = $this->mTrans->get_datatables_happv($year, $date_togo);
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            switch ($item->ket_approve) {
                case 'APPROVED':
                    $sts = '<span class="text-success">' . $item->ket_approve . '</span>';
                    break;
                case '-':
                    $sts = '<i class="fas fa-check-square fa-2x mr-2 text-success modalApprove" role="button" title="Approve" data-status="2" data-id=' . $item->id . '></i><i class="fas fa-times-circle fa-2x text-danger modalApprove" role="button" title="Rejected" data-status="3" data-id=' . $item->id . '></i>';
                    break;
                case 'REJECTED':
                    $sts = '<span class="text-danger">' . $item->ket_approve . '</span>';
                    break;
            }
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $sts;
            $row[] = '<b>' . $item->billing_id . '</b>';
            $row[] = $item->createdate;
            $row[] = $item->destname;
            $row[] = $item->ticketdatefrom;
            $row[] = $item->total_pax;
            $row[] = $item->first_name;
            $row[] = $item->last_name;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mTrans->count_all_happv(),
            "recordsFiltered" => $this->mTrans->count_filtered_happv($year, $date_togo),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function all_billing($year, $date_togo, $dest, $status)
    {
        // $dest = $this->input->post('dest');
        // $status = $this->input->post('status');
        // $dst = !empty($dest) ? $dest : '';
        // $sts = !empty($status) ? $status : '';
        $list = $this->mTrans->get_datatables_bill($year, $date_togo, $dest, $status);
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            switch ($item->ket_status) {
                case 'PAID IPAY88':
                case 'PAID (Manual)':
                    $sts = '<span class="text-success">' . $item->ket_status . '</span>';
                    $cancel = '<i class="fas fa-times-circle text-danger fa-2x cancelBooking" title="Cancel order" role="button" data-id="' . $item->id . '" data-bill="' . $item->billing_id . '"></i>';
                    $pmanual = '';
                    break;
                case 'UNPAID':
                    $bill = str_replace('#', '', $item->billing_id);
                    $sts = '<span class="text-warning">' . $item->ket_status . '</span>';
                    $cancel = '<i class="fas fa-times-circle text-danger fa-2x cancelBooking" title="Cancel order" role="button" data-id="' . $item->id . '" data-bill="' . $item->billing_id . '"></i>';
                    // $pmanual = '<i class="fas fa-wallet text-success mr-2 fa-2x paidManual" role="button" data-id="' . $item->id . '" data-bill="' . $bill . '" title="Paid manual"></i>';
                    $pmanual = '<a href="paid-manual" target="_blank"><i class="fas fa-wallet text-success mr-2 fa-2x" role="button"></i></a>';
                    break;
                case 'CANCEL':
                    $sts = '<span class="text-danger">' . $item->ket_status . '</span>';
                    $cancel = '';
                    $pmanual = '';
                    break;
            }

            $bill_id = str_replace('#', '', $item->billing_id);
            $click = "location.href='https://www.apisptix.sabahparks.org.my/public/download/receipt?billing_id=$bill_id'";
            if (empty($item->pay_date) || $item->pay_date == '') {
                $btn = '<i class="fas fa-info-circle fa-2x mr-2 billingDetail" style="color: #f8538d;" role="button" data-id="' . $item->id . '" data-destid="' . $item->destid . '" data-datetogo="' . $item->ticketdatefrom . '" data-idupd="' . $item->id_update . '" data-bill="' . $bill_id . '" data-status="' . $item->status . '" title="Detail, History, Change Date, Resit, Cancel remark"></i><i class="fas fa-edit fa-2x text-secondary mr-2 editBilling" title="Edit" role="button" data-id="' . $item->id . '"></i>';
            } else {
                $btn = '<i class="fas fa-info-circle fa-2x mr-2 billingDetail" style="color: #f8538d;" role="button" data-id="' . $item->id . '" data-destid="' . $item->destid . '" data-datetogo="' . $item->ticketdatefrom . '" data-idupd="' . $item->id_update . '" data-bill="' . $bill_id . '" data-status="' . $item->status . '" title="Detail, History, Change Date, Resit, Cancel remark"></i><i class="fas fa-file-pdf fa-2x mr-2 pdfBilling" data-billid="' . $bill_id . '" style="color: #ff9000;" role="button" data-id="' . $item->id . '" title="Print billing"></i><i class="fas fa-edit fa-2x text-secondary mr-2 editBilling" title="Edit" role="button" data-id="' . $item->id . '"></i>';
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $btn . $pmanual . $cancel;
            $row[] = '<b>' . $item->billing_id . '</b>';
            $row[] = $item->createdate;
            $row[] = $item->first_name;
            $row[] = $item->last_name;
            $row[] = $item->tot_booked;
            $row[] = $item->country;
            $row[] = $item->address;
            $row[] = $item->email;
            $row[] = $item->phone;
            $row[] = $item->destname;
            $row[] = $item->ticketdatefrom;
            $row[] = $item->receipt_id;
            $row[] = $item->pay_date;
            $row[] = $sts;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mTrans->count_all_bill(),
            "recordsFiltered" => $this->mTrans->count_filtered_bill($year, $date_togo, $dest, $status),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function all_ticket($year, $date_togo)
    {
        $list = $this->mTrans->get_datatables_tkt($year, $date_togo);
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            if ($item->ticket_id !== '' || !empty($item->ticket_id)) {
                // $tiket = "<a href='https://www.apisptix.sabahparks.org.my/public/download/ticket?ticket_id=$item->ticket_id' class='btn btn-danger btn-sm' target = '_blank' ><i class='fas fa-ticket-alt'></i></a>";
                $click = "location.href='https://www.apisptix.sabahparks.org.my/public/download/ticket?ticket_id=$item->ticket_id'";
                $tiket = '<i class="fas fa-ticket-alt fa-2x mr-2" style="color: #e7515a;" role="button" title="Print ticket" onclick="' . $click . '"></i>';
            } else {
                $tiket = '';
            }

            switch ($item->status) {
                case 'OPEN':
                    $sts = '<span class="text-success">' . $item->status . '</span>';
                    break;
                case 'CHECK IN':
                    $sts = '<span class="text-primary">' . $item->status . '</span>';
                    break;
                case 'EXPIRED':
                    $sts = '<span class="text-danger">' . $item->status . '</span>';
                    break;
            }


            $pr_sts = ($item->pr_holder == 1) ? 'Y' : 'N';

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $tiket;
            $row[] = $item->date_togo;
            $row[] = $item->ticket_id;
            $row[] = '<b>' . $item->billing_id . '</b>';
            $row[] = $item->destname;
            $row[] = $item->visitor_name;
            $row[] = $item->phone;
            $row[] = $item->email;
            $row[] = $item->nationality;
            $row[] = $pr_sts;
            $row[] = $sts;
            $row[] = $item->ticketdatefrom;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mTrans->count_all_tkt(),
            "recordsFiltered" => $this->mTrans->count_filtered_tkt($year, $date_togo),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function all_visitor($params)
    {
        $list = $this->mTrans->get_datatables_vst($params);
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            $pr_sts = ($item->pr_holder == 1) ? 'Y' : 'N';
            $sts = ($item->status == 1) ? '<span class="text-success">ACTIVE</span>' : '<span class="text-danger">CANCEL</span>';

            if ($item->status == 1) {
                $btn = '<i class="fas fa-info-circle fa-2x mr-2 visitorDetail" style="color: #f8538d;" role="button" data-id="' . $item->id . '" title="Detail info"></i><i class="fas fa-edit fa-2x text-secondary mr-2 editVisitor" title="Edit" role="button" data-id="' . $item->id . '"></i><i class="fas fa-times-circle fa-2x text-danger modalApprove" role="button" title="Canceled Visitor" data-status="0" data-id=' . $item->id . '></i>';
            } else {
                $btn = '<i class="fas fa-info-circle fa-2x mr-2 visitorDetail" style="color: #f8538d;" role="button" data-id="' . $item->id . '" title="Detail info"></i><i class="fas fa-edit fa-2x text-secondary mr-2" title="Edit" role="button"></i>';
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $btn;
            $row[] = $item->visitor_name;
            $row[] = $item->phone;
            $row[] = $item->email;
            $row[] = $item->nationality;
            $row[] = $pr_sts;
            $row[] = $item->total_attend;
            $row[] = $sts;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mTrans->count_all_vst(),
            "recordsFiltered" => $this->mTrans->count_filtered_vst($params),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function all_lock_quota($params)
    {
        $list = $this->mTrans->get_datatables_lock($params);
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            $pr_sts = ($item->pr_holder == 1) ? 'Y' : 'N';
            $sts = ($item->status == 1) ? '<span class="text-success">ACTIVE</span>' : '<span class="text-danger">CANCEL</span>';

            if ($item->status == 1) {
                $btn = '<i class="fas fa-info-circle fa-2x mr-2 visitorDetail" style="color: #f8538d;" role="button" data-id="' . $item->id . '" title="Detail info"></i><i class="fas fa-times-circle fa-2x text-danger modalApprove" role="button" title="Canceled Visitor" data-status="0" data-id=' . $item->id . '></i>';
            } else {
                $btn = '<i class="fas fa-info-circle fa-2x mr-2 visitorDetail" style="color: #f8538d;" role="button" data-id="' . $item->id . '" title="Detail info"></i>';
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $btn;
            $row[] = $item->visitor_name;
            $row[] = $item->phone;
            $row[] = $item->email;
            $row[] = $item->nationality;
            $row[] = $pr_sts;
            $row[] = $item->total_attend;
            $row[] = $sts;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mTrans->count_all_lock(),
            "recordsFiltered" => $this->mTrans->count_filtered_lock($params),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function scan_checkin($params)
    {
        $list = $this->mTrans->get_datatables_chk($params);
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            $pr_sts = ($item->pr_status == 'Y') ? '<span class="text-success">Y</span>' : '<span class="text-danger">N</span>';

            $click = "location.href='https://www.apisptix.sabahparks.org.my//public/download/ticket?ticket_id=$item->ticket_id'";
            $btn = '<i class="fas fa-ticket-alt fa-2x mr-2" style="color: #e7515a;" role="button" title="Print ticket" onclick="' . $click . '"></i>';
            if ($item->status == '1') {
                $cancel = '<i class="fas fa-times-circle text-danger ml-1 fa-2x checkIn" role="button" data-id="' . $item->id . '"></i>';
            } else {
                $cancel = '';
            }


            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $btn . $cancel;
            $row[] = $item->date_togo;
            $row[] = $item->destname;
            $row[] = $item->visitor_name;
            $row[] = $item->id_number;
            $row[] = $item->phone;
            $row[] = $item->email;
            $row[] = $item->nationality;
            $row[] = $pr_sts;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mTrans->count_all_chk(),
            "recordsFiltered" => $this->mTrans->count_filtered_chk($params),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function requery($params)
    {
        $list = $this->mTrans->get_datatables_rqr($params);
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            // $pr_sts = ($item->pr_status == 'Y') ? '<span class="text-success">Y</span>' : '<span class="text-danger">N</span>';

            // $click = "location.href='https://www.apisptix.sabahparks.org.my//public/download/ticket?ticket_id=$item->ticket_id'";
            // $btn = '<i class="fas fa-ticket-alt fa-2x mr-2" style="color: #e7515a;" role="button" title="Print ticket" onclick="' . $click . '"></i>';
            // if ($item->status == '1') {
            //     $cancel = '<i class="fas fa-times-circle text-danger ml-1 fa-2x checkIn" role="button" data-id="' . $item->id . '"></i>';
            // } else {
            //     $cancel = '';
            // }

            switch ($item->status) {
                case '1':
                    $sts = '<span class="text-danger">' . $item->ket_status . '</span>';
                    break;
                case '2':
                    $sts = '<span class="text-success">' . $item->ket_status . '</span>';
                    break;
                case '3':
                    $sts = '<span class="text-dark">' . $item->ket_status . '</span>';
                    break;
            }


            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '<i class="fas fa-sync-alt fa-2x text-danger modalRequery" role="button" title="Re Query Ipay88" data-billid="' . str_replace('#', '', $item->billing_id)  . '" data-totalcost="' . $item->total_cost . '"></i>';
            $row[] = '<b>' . $item->billing_id . '</b>';
            $row[] = $item->createdate;
            $row[] = $item->first_name;
            $row[] = $item->last_name;
            $row[] = $item->country;
            $row[] = $item->address;
            $row[] = $item->email;
            $row[] = $item->phone;
            $row[] = $item->destname;
            $row[] = $item->ticketdatefrom;
            $row[] = $sts;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mTrans->count_all_rqr(),
            "recordsFiltered" => $this->mTrans->count_filtered_rqr($params),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    // data
    public function detailMember_byId($id)
    {
        $data = $this->db->get_where('stp_member', ['id' => $id])->row();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function detailOrder_byId($id)
    {
        $data = $this->mTrans->detailOrder_byId($id)->row();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function detailBilling_byId($id)
    {
        $data = $this->mTrans->detailBilling_byId($id)->row();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function detailVisitor_byId($id)
    {
        $data = $this->mTrans->detailVisitor_byId($id)->row();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function get_history_changedate($bill_id)
    {
        $data = $this->mTrans->get_history_changedate($bill_id)->result();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function get_visitor_byId($id)
    {
        $data = $this->mTrans->get_visitor_byId($id)->row();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function get_billing_byId($id)
    {
        $data = $this->mTrans->get_billing_byId($id)->row();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function get_member_json()
    {
        $data = $this->mTrans->get_member()->result();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function get_climber_by_billingid($billing_id)
    {
        $data = $this->db->query("SELECT * FROM trx_billing_visitor where billing_id = '#$billing_id'")->result();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function holder_approve()
    {
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        $this->db->where('id', $id);
        $upd = $this->db->update('trx_billing', ['status_approved' => $status]);
        if ($upd == true) {
            $msg = ($status == '2') ? 'Approved.' : 'Rejected.';
            $this->session->set_flashdata('success', $msg);
            redirect('booking', 'refresh');
        } else {
            $this->session->set_flashdata('error', 'Oops, something wrong !!');
            redirect('booking', 'refresh');
        }
    }

    public function visitor_cancel()
    {
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        $this->db->where('id', $id);
        $upd = $this->db->update('trx_billing_visitor', ['status' => $status]);
        if ($upd == true) {
            $this->session->set_flashdata('success', 'Canceled Visitor.');
            redirect('visitor', 'refresh');
        } else {
            $this->session->set_flashdata('error', 'Oops, something wrong !!');
            redirect('visitor', 'refresh');
        }
    }

    public function get_visitor_by_billingid($billing_id)
    {
        $get = $this->db->get_where('trx_billing_visitor', ['billing_id' => '#' . $billing_id]);
        $data = [];
        if ($get->num_rows() == 0) {
            $item = [];
            $item['msg'] = 'error';
            $data[] = $item;
        } else {
            $i = 1;
            foreach ($get->result() as $v) {
                $item = [];
                $item['msg'] = 'success';
                $item['id'] = $v->id;
                $item['visitor'] = $v->visitor_name;
                $item['email'] = $v->email;
                $item['datetogo'] = $v->ticketdatefrom;
                $item['status'] = $v->status;
                $data[] = $item;
            }
        }
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function test()
    {
        $a = $this->curl('https://apisptix.sabahparks.org.my/public/order/get-event-new?destid=SB001&month=2&year=2022');
        $data = json_decode($a, true);
        $mydata = $data['data'];
        $jml = 1;
        for ($i = 0; $i < sizeof($mydata); $i++) {
            if ($mydata[$i]['date'] == '2022-02-26') {
                $hit = ($mydata[$i]['available'] - $jml);
                if ((int) $hit < 0) {
                    $avb = $mydata[$i]['available'];
                    $check = 'N';
                } else {
                    $avb = $mydata[$i]['available'];
                    $check = 'Y';
                }
            }
        }
        print $avb;
        print $check;
    }

    public function chg_date_togo()
    {
        $destId = $this->input->post('destId');
        $id = $this->input->post('idDatetogo');
        $datebefore = $this->input->post('dateBefore');
        $dateafter = $this->input->post('dateAfter');
        $personid = $this->input->post('personid');
        $arr = explode(',', $personid);
        $jumlahupdate = sizeof($arr);

        $pecah = explode('-', $dateafter);
        $bulan = (int) $pecah[1];
        $tahun = (int) $pecah[0];
        $get = $this->curl('https://apisptix.sabahparks.org.my/public/order/get-event-new?destid=' . $destId . '&month=' . $bulan . '&year=' . $tahun);
        $data = json_decode($get, true);
        $mydata = $data['data'];
        for ($i = 0; $i < sizeof($mydata); $i++) {
            if ($mydata[$i]['date'] == $dateafter) {
                $hit = ($mydata[$i]['available'] - $jumlahupdate);
                if ((int) $hit < 0) {
                    $avb = $mydata[$i]['available'];
                    $check = 'N';
                } else {
                    $avb = $mydata[$i]['available'];
                    $check = 'Y';
                }
            }
        }

        $get = $this->mTrans->getDetailOrder($id)->row();
        $idb = str_replace('#', '', $get->billing_id);

        // receipt id
        $receiptid = $this->db->get_where('trx_ticket', ['billing_id' => $get->billing_id])->row()->receipt_id;

        if ($check == 'N') {
            $this->session->set_flashdata('error', 'Quantity of people exceeds the available quota !!<br>Available : <b>' . $avb . ' person</b>');
            redirect('billing', 'refresh');
        } else {
            if (!empty($personid) || $personid !== '') {

                $this->db->where('id', $id);
                $upd = $this->db->update('trx_billing_dtl', ['ticketdatefrom' => $dateafter]);

                $tb_vtor = $this->db->get_where('trx_ticket', ['billing_id' => $get->billing_id])->row();

                for ($i = 0; $i < $jumlahupdate; $i++) {
                    $this->db->where('id', $arr[$i]);
                    $upd = $this->db->update('trx_billing_visitor', [
                        'ticketdatefrom' => $dateafter,
                        'ticketdateto' => $dateafter
                    ]);

                    $get1 = $this->db->get_where('trx_billing_visitor', ['id' => $arr[$i]])->row();
                    // delete dulu di trx_ticket
                    $this->db->where('id_number', $get1->id_number);
                    $this->db->where('visitor_name', $get1->visitor_name);
                    $this->db->delete('trx_ticket');

                    $this->db->where('id', $arr[$i]);
                    $upd .= $this->db->update('trx_billing_visitor', [
                        'visitor_name' => $get1->visitor_name . ' (* CHANGE DATE : ' . $dateafter . ')'
                    ]);

                    $this->db->insert('trx_ticket', [
                        'ticket_id' => 'N' . $tb_vtor->receipt_id . $arr[$i],
                        'receipt_id' => $receiptid,
                        'billing_id' => $get->billing_id,
                        'buyerid' => $get1->buyerid,
                        'destid' => $get1->destid,
                        'ticketdatefrom' => $dateafter,
                        'ticketdateto' => $dateafter,
                        'visitor_name' => $get1->visitor_name . ' (* CHANGE DATE : ' . $dateafter . ')',
                        'id_number' => $get1->id_number,
                        'email' => $get1->email,
                        'phone' => $get1->phone,
                        'nationality' => $get1->nationality,
                        'gender' => $get1->gender,
                        'age' => $get1->age,
                        'status' => $get1->status
                    ]);

                    $upd .= $this->db->insert('tb_history', [
                        'billing_id'    => $idb,
                        'category'      => 1,
                        'note'          => 'new-change-date-togo|id-update|' . $arr[$i],
                        'date_before'   => $datebefore,
                        'date_togo'     => $dateafter,
                        'user_update'   => $this->session->id_user,
                        'update_time'   => date('Y-m-d H:i:s')
                    ]);
                }

                if ($upd == true) {
                    $this->session->set_flashdata('success', 'Successfully change date to go by selected climber<br>Automatic to page <b>Send Document</b>');
                    redirect('billing?doc=send&rc=' . $receiptid, 'refresh');
                } else {
                    $this->session->set_flashdata('error', 'Oops, something wrong !!');
                    redirect('billing', 'refresh');
                }
            } else {

                $this->db->where('id', $id);
                $upd = $this->db->update('trx_billing_dtl', ['ticketdatefrom' => $dateafter]);

                $this->db->where('billing_id', $get->billing_id);
                $upd .= $this->db->update('trx_billing_visitor', ['ticketdatefrom' => $dateafter, 'ticketdateto' => $dateafter]);

                $this->db->where('billing_id', $get->billing_id);
                $upd .= $this->db->update('trx_ticket', ['ticketdatefrom' => $dateafter, 'ticketdateto' => $dateafter]);

                if ($upd == true) {
                    $this->db->insert('tb_history', [
                        'billing_id'    => $idb,
                        'category'      => 1,
                        'note'          => 'new-change-date-togo|id-update|' . $id,
                        'date_before'   => $datebefore,
                        'date_togo'     => $dateafter,
                        'user_update'   => $this->session->id_user,
                        'update_time'   => date('Y-m-d H:i:s')
                    ]);

                    $this->session->set_flashdata('success', 'Successfully change date to go');
                } else {
                    $this->session->set_flashdata('error', 'Oops, something wrong !!');
                    redirect('billing', 'refresh');
                }
            }
        }
    }
    // data

    function create_new_billing($billing_id, $data, $jumlah)
    {
        // var_dump($billing_id).'<br>';
        // var_dump($data).'<br>';
        // var_dump($jumlah);
        for ($i = 1; $i <= sizeof($data); $i++) {
            print $data[$i] . '<br>';
        }
        // $new_bill = '#' . $this->randString('string_number', 8);
        // $no = $this->db->query("SELECT
        //                             max(
        //                             RIGHT ( receipt_id, 12 )) AS `no` 
        //                         FROM
        //                             `trx_receipt` 
        //                         WHERE
        //                             receipt_id LIKE 'SB%'")->row()->no;
        // $no1 = '000000000000' . ($no + 1);
        // $hash = 'SB' . substr($no1, -12);

        // // to billing
        // $get1 = $this->db->get_where('trx_billing', ['billing_id' => $billing_id])->row();
        // $ins1 = $this->db->insert('trx_billing', [
        //     'billing_id' => $new_bill,
        //     'buyerid' => $get1->buyerid,
        //     'first_name' => $get1->first_name,
        //     'last_name' => $get1->last_name,
        //     'company_name' => $get1->company_name,
        //     'country' => $get1->country,
        //     'address' => $get1->address,
        //     'city' => $get1->city,
        //     'postcode' => $get1->postcode,
        //     'email' => $get1->email,
        //     'phone' => $get1->phone,
        //     'total_pax' => $jumlah,
        //     'total_cost' => 0,
        //     'status' => 1,
        //     'pr_holder' => 2,
        //     'status_approved' => 1,
        //     'receipt' => $hash,
        //     'note_lock_quota' => null
        // ]);
        // $new_pax1 = ((int) $get1->total_pax - (int) $jumlah);
        // $this->db->update('id', $get1->id);
        // $this->db->update('trx_billing', [
        //     'total_pax' => $new_pax1
        // ]);

        // // to billing_dtl
        // $get2 = $this->db->get_where('trx_billing_dtl', ['billing_id' => $billing_id])->row();
        // $this->db->insert('trx_billing_dtl', [
        //     'billing_id' => $new_bill,
        //     'buyerid' => $get2->buyerid,
        //     'destid' => $get2->destid,
        //     'ticketdatefrom' => $get2->ticketdatefrom,
        //     'ticketdateto' => $get2->ticketdateto,
        //     'loc_qty_18above' => $jumlah,
        //     'loc_qty_18below' => 0,
        //     'int_qty_18above' => 0,
        //     'int_qty_18below' => 0,
        //     'createdate' => date('Y-m-d'),
        //     'status' => 1
        // ]);

        // for ($i = 1; $i <= (int) $jumlah; $i++) {
        //     $this->db->insert('trx_billing_visitor', [
        //         'billing_id' => $new_bill,
        //         'buyerid' => $get2->buyerid,
        //         'destid' => $get2->destid,
        //         'ticketdatefrom' => $get2->ticketdatefrom,
        //         'ticketdateto' => $get2->ticketdateto,
        //         'visitor_name' => 'Sabah Parks - Person ' . $i,
        //         'id_number' => '-',
        //         'email' => '-',
        //         'phone' => '-',
        //         'nationality' => 'MALAYSIA',
        //         'gender' => 0,
        //         'age' => 0,
        //         'status' => 1
        //     ]);

        //     $tid = $hash . $this->randString('number', 4);
        //     $this->db->insert('trx_ticket', [
        //         'ticket_id' => $tid,
        //         'receipt_id' => $hash,
        //         'billing_id' => $rand,
        //         'buyerid' => 'lock',
        //         'destid' => $destid,
        //         'ticketdatefrom' => $date,
        //         'ticketdateto' => $date,
        //         'visitor_name' => 'Sabah Parks - Person ' . $i,
        //         'id_number' => '-',
        //         'email' => '-',
        //         'phone' => '-',
        //         'nationality' => 'MALAYSIA',
        //         'gender' => 0,
        //         'age' => 0,
        //         'status' => 1
        //     ]);
        // }

        // $ins .= $this->db->insert('trx_receipt', [
        //     'receipt_id' => $hash,
        //     'billing_id' => $rand,
        //     'bank_name' => '-',
        //     'bank_no' => '-',
        //     'bank_acc' => '-',
        //     'remark' => '-',
        //     'amount' => '-',
        //     'status' => 1,
        //     'createdate' => date('Y-m-d H:i:s')
        // ]);
    }


    function cancel_booking()
    {
        $id = $this->input->post('id');
        $redirect = $this->input->post('redirect');

        // on menu billing
        $method = $this->input->post('method');
        if ($method == 'personal') {
            $remark = $this->input->post('remarkPersonal');
            if (empty($remark) || $remark == '') {
                $this->session->set_flashdata('error', 'Remarks are required !!');
                redirect($redirect, 'refresh');
            } else {
                $climber = $this->input->post('climber');
                $billing_id = $this->input->post('billing_id');
                $jml = $this->db->get_where('trx_billing_visitor', ['billing_id' => $billing_id])->num_rows();
                if (sizeof($climber) == $jml) {
                    $this->session->set_flashdata('error', 'If cancel all climber, used Cancel All !!');
                    redirect($redirect, 'refresh');
                } else {
                    for ($i = 0; $i < sizeof($climber); $i++) {
                        $get_vst = $this->db->get_where('trx_billing_visitor', ['id' => $climber[$i]])->row();
                        $new_name = $get_vst->visitor_name . ' (CANCEL CLIMBER)';
                        // upd ke trx_ticket
                        $this->db->where('billing_id', $billing_id);
                        $this->db->where('id_number', $get_vst->id_number);
                        $this->db->update('trx_ticket', ['ticket_id' => 'cancel-climber', 'visitor_name' => $new_name, 'status' => '3']);

                        $this->db->where('id', $climber[$i]);
                        $this->db->update('trx_billing_visitor', ['visitor_name' => $new_name, 'status' => '3']);

                        $this->db->insert('tb_history', [
                            'billing_id'    => str_replace('#', '', $billing_id),
                            'category'      => 5,
                            'note'          => $remark,
                            'user_update'   => $this->session->id_user,
                            'update_time'   => date('Y-m-d H:i:s')
                        ]);
                    }

                    $this->session->set_flashdata('success', 'Canceled by personal Climber successfully..');
                    redirect($redirect, 'refresh');
                }
            }
        } else {
            $remark = $this->input->post('remarkAll');
            if (empty($remark) || $remark == '') {
                $this->session->set_flashdata('error', 'Remarks are required !!');
                redirect($redirect, 'refresh');
            } else {
                $this->db->where('id', $id);
                $upd = $this->db->update('trx_billing', [
                    'status' => 3,
                    'note_cancel' => $remark
                ]);

                $get = $this->db->get_where('trx_billing', ['id' => $id])->row();
                $idb = str_replace('#', '', $get->billing_id);
                $this->db->insert('tb_history', [
                    'billing_id'    => $idb,
                    'category'      => 3,
                    'note'          => $remark,
                    'user_update'   => $this->session->id_user,
                    'update_time'   => date('Y-m-d H:i:s')
                ]);

                if ($upd == true) {
                    $this->session->set_flashdata('success', 'Canceled this order successfully..');
                    redirect($redirect, 'refresh');
                } else {
                    $this->session->set_flashdata('error', 'Oops, something wrong !!');
                    redirect($redirect, 'refresh');
                }
            }
        }
    }

    function update_ticket()
    {
        $id = $this->input->post('id');
        $redirect = $this->input->post('redirect');
        $status = $this->input->post('status');
        $note = $this->input->post('note');

        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $upd = $this->db->update('trx_ticket', [
            'status' => 2,
            'check_update' => $status,
            'note' => $note
        ]);

        $get = $this->db->get_where('trx_ticket', ['id' => $id])->row();
        $this->db->insert('tb_history', [
            'billing_id'    => $get->ticket_id,
            'category'      => 4,
            'note'          => $note,
            'user_update'   => $this->session->id_user,
            'update_time'   => date('Y-m-d H:i:s')
        ]);

        if ($upd == true) {
            $this->session->set_flashdata('success', 'Check In ticket successfully..');
            redirect($redirect, 'refresh');
        } else {
            $this->session->set_flashdata('error', 'Oops, something wrong !!');
            redirect($redirect, 'refresh');
        }
    }

    public function upload_receipt()
    {
        $id = $this->input->post('id');
        $redirect = $this->input->post('redirect');
        $note = $this->input->post('note');

        $config['upload_path']          = 'assets/img/receipt/';
        $config['allowed_types']        = 'jpeg|jpg|png|JPEG|JPG|PNG';
        $config['max_size']             = 0;
        $config['max_width']            = 0;
        $config['max_height']           = 0;
        $config['remove_space']         = TRUE;
        $config['encrypt_name']         = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file')) {
            $this->session->set_flashdata('error', 'Oops, something wrong !!');
            redirect($redirect, 'refresh');
        } else {

            $get = $this->db->get_where('trx_billing', ['id' => $id])->row();
            $idb = str_replace('#', '', $get->billing_id);

            $transid = $this->db->get_where('trx_ticket', ['billing_id' => $get->billing_id]);
            if ($transid->num_rows() == 0) {
                $this->session->set_flashdata('error', 'Oops, Please get response email ipay88 from Member to insert Ticket ID number !!');
                redirect($redirect, 'refresh');
            } else {
                $trid = $transid->row()->receipt_id;
                $this->curl('https://booking.sabahparks.org.my/tes-email.php?transid=' . $trid . '&bill=' . $idb);

                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                $this->db->where('id', $id);
                $this->db->update('trx_billing', [
                    'status' => 2,
                    'receipt' => $file_name,
                    'note' => $note
                ]);

                $this->db->insert('tb_history', [
                    'billing_id'    => $idb,
                    'category'      => 2,
                    'note'          => $note,
                    'file'          => $file_name,
                    'user_update'   => $this->session->id_user,
                    'update_time'   => date('Y-m-d H:i:s')
                ]);
            }
        }

        $this->session->set_flashdata('success', 'Upload resit successfully..');
        redirect($redirect, 'refresh');
    }

    function curl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public function upload_news()
    {
        $type = $this->input->post('type');
        $date = $this->input->post('date');
        $title = $this->input->post('title');
        $desc = $this->input->post('desc');
        $active = $this->input->post('active');
        $act = ($active == 'on') ? 1 : 0;

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('date', 'Date', 'required');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('desc', 'Description', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('news', 'refresh');
        } else {
            // $config['upload_path']          = 'assets/img/news/';
            $config['upload_path']          = '../../../img/';
            $config['allowed_types']        = 'jpeg|jpg|png|JPEG|JPG|PNG';
            $config['max_size']             = 0;
            $config['max_width']            = 0;
            $config['max_height']           = 0;
            $config['remove_space']         = TRUE;
            $config['encrypt_name']         = TRUE;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
                $this->session->set_flashdata('error', 'Oops, something wrong !!');
                redirect('news', 'refresh');
            } else {
                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                $blogid = random_int(5, 99999);
                $this->db->insert('trx_blog', [
                    'blogid' => $blogid,
                    'blogtitle' => $title,
                    'description' => $desc,
                    'tag' => $title,
                    'type' => $type,
                    'path_cover' => $file_name,
                    'createdate' => $date,
                    'updateid' => 1,
                    'status' => $act
                ]);

                $this->session->set_flashdata('success', 'Upload news successfully..');
                redirect('news', 'refresh');
            }
        }
    }

    public function save_edit_news()
    {
        $id = $this->input->post('id');
        $type = $this->input->post('type');
        $date = $this->input->post('date');
        $title = $this->input->post('title');
        $desc = $this->input->post('desc');
        $active = $this->input->post('active');
        $act = ($active == 'on') ? 1 : 0;

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('date', 'Date', 'required');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('desc', 'Description', 'required');

        // $config['upload_path']          = 'assets/img/news/';
        $config['upload_path']          = '../../../img/';
        $config['allowed_types']        = 'jpeg|jpg|png|JPEG|JPG|PNG';
        $config['max_size']             = 0;
        $config['max_width']            = 0;
        $config['max_height']           = 0;
        $config['remove_space']         = TRUE;
        $config['encrypt_name']         = TRUE;
        $this->load->library('upload', $config);

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('news', 'refresh');
        } else {
            if (!$this->upload->do_upload('file')) {
                $this->db->where('id', $id);
                $this->db->update('trx_blog', [
                    'blogtitle' => $title,
                    'description' => $desc,
                    'tag' => $title,
                    'type' => $type,
                    'createdate' => $date,
                    'status' => $act
                ]);

                $this->session->set_flashdata('success', 'Update news successfully..');
                redirect('news', 'refresh');
            } else {
                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                $this->db->where('id', $id);
                $this->db->update('trx_blog', [
                    'blogtitle' => $title,
                    'description' => $desc,
                    'tag' => $title,
                    'type' => $type,
                    'path_cover' => $file_name,
                    'createdate' => $date,
                    'status' => $act
                ]);

                $this->session->set_flashdata('success', 'Update news successfully..');
                redirect('news', 'refresh');
            }
        }
    }

    function edit_news($id)
    {
        $data = $this->db->query("SELECT
                            SUBSTRING_INDEX( a.path_cover, '.',- 1 ) AS extention,
	                        SUBSTRING_INDEX( a.path_cover, '.', 1 ) AS name_file,
                            a.*,
                        IF
                            ( a.`status` = 1, 'ACTIVE', 'NON-ACTIVE' ) AS ket_status 
                        FROM
                            `trx_blog` a 
                        WHERE
                            a.id = '$id'")->row();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function delete_news()
    {
        $id = $this->input->post('id');
        $nm_file = $this->db->get_where('trx_blog', ['id' => $id])->row()->path_cover;
        // $file = 'assets/img/news/' . $nm_file;
        $file = '../../../img/' . $nm_file;
        unlink($file);

        $this->db->where('id', $id);
        $del = $this->db->delete('trx_blog');
        if ($del == true) {
            $this->session->set_flashdata('success', 'Delete data successfully..');
            redirect('news', 'refresh');
        } else {
            $this->session->set_flashdata('error', 'Oops, something wrong !!');
            redirect('news', 'refresh');
        }
    }

    public function upload_broadcast()
    {
        $title = $this->input->post('title');
        $date = $this->input->post('date');
        $desc = $this->input->post('desc');
        $active = $this->input->post('active');
        $act = ($active == '1') ? 'Y' : 'N';

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('date', 'Date', 'required');
        $this->form_validation->set_rules('desc', 'Description', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('broadcast', 'refresh');
        } else {
            $config['upload_path']          = 'assets/img/broadcast/';
            $config['allowed_types']        = 'jpeg|jpg|png|JPEG|JPG|PNG';
            $config['max_size']             = 0;
            $config['max_width']            = 0;
            $config['max_height']           = 0;
            $config['remove_space']         = TRUE;
            $config['encrypt_name']         = TRUE;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
                $this->session->set_flashdata('error', 'Oops, something wrong !!');
                redirect('broadcast', 'refresh');
            } else {
                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                $this->db->insert('trx_broadcast', [
                    'id_type' => 1,
                    'title' => $title,
                    'content' => $desc,
                    'date_created' => date('Y-m-d H:i:s'),
                    'expire_date' => $date,
                    'icon' => 'fa fa-newspaper',
                    'path' => $file_name,
                    'status' => $act
                ]);

                $this->session->set_flashdata('success', 'Upload broadcast successfully..');
                redirect('broadcast', 'refresh');
            }
        }
    }

    public function save_edit_broadcast()
    {
        $id = $this->input->post('id');
        $title = $this->input->post('title');
        $date = $this->input->post('date');
        $desc = $this->input->post('desc');
        $active = $this->input->post('active');
        $act = ($active == '1') ? 'Y' : 'N';

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('date', 'Date', 'required');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('desc', 'Description', 'required');

        $config['upload_path']          = 'assets/img/broadcast/';
        $config['allowed_types']        = 'jpeg|jpg|png|JPEG|JPG|PNG';
        $config['max_size']             = 0;
        $config['max_width']            = 0;
        $config['max_height']           = 0;
        $config['remove_space']         = TRUE;
        $config['encrypt_name']         = TRUE;
        $this->load->library('upload', $config);

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('broadcast', 'refresh');
        } else {
            if (!$this->upload->do_upload('file')) {
                $this->db->where('id', $id);
                $this->db->update('trx_broadcast', [
                    'title' => $title,
                    'content' => $desc,
                    'expire_date' => $date,
                    'status' => $act
                ]);

                $this->session->set_flashdata('success', 'Update broadcast successfully..');
                redirect('broadcast', 'refresh');
            } else {
                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                $this->db->where('id', $id);
                $this->db->update('trx_broadcast', [
                    'title' => $title,
                    'content' => $desc,
                    'expire_date' => $date,
                    'path' => $file_name,
                    'status' => $act
                ]);

                $this->session->set_flashdata('success', 'Update broadcast successfully..');
                redirect('broadcast', 'refresh');
            }
        }
    }

    function edit_broadcast($id)
    {
        $data = $this->db->query("SELECT
                                a.*,
                                DATE( a.date_created ) AS new_date,
                            IF
                                (
                                    a.id_type = 1,
                                    'News',
                                IF
                                ( a.id_type = 2, 'Promotion', 'Events' )) AS ket_type 
                            FROM
                                `trx_broadcast` a  
                            WHERE
                                a.id = '$id'")->row();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    function delete_broadcast()
    {
        $id = $this->input->post('id');
        $nm_file = $this->db->get_where('trx_broadcast', ['id' => $id])->row()->path;
        $file = 'assets/img/broadcast/' . $nm_file;
        unlink($file);

        $this->db->where('id', $id);
        $del = $this->db->delete('trx_broadcast');
        if ($del == true) {
            $this->session->set_flashdata('success', 'Delete data successfully..');
            redirect('broadcast', 'refresh');
        } else {
            $this->session->set_flashdata('error', 'Oops, something wrong !!');
            redirect('broadcast', 'refresh');
        }
    }

    public function send_inbox()
    {
        $id = $this->input->post('id');
        $id_blog = $this->db->get_where('trx_blog', ['id' => $id])->row();
        $get = $this->db->query("SELECT
                                    a.* 
                                FROM
                                    `stp_member` a 
                                    -- LIMIT 10
                                ")->result();
        $i = 0;
        foreach ($get as $mbr) {
            $this->db->insert('trx_member_broadcast', [
                'id_member' => $mbr->buyerid,
                'id_broadcast' => 0,
                'id_blog' => $id_blog->id,
                'status' => 1
            ]);
            $i++;
        }

        if ($i > 0) {
            $this->session->set_flashdata('success', 'Send to inbox successfully..');
            redirect('news', 'refresh');
        } else {
            $this->session->set_flashdata('error', 'Oops, something wrong !!');
            redirect('news', 'refresh');
        }
    }

    public function save_edit_visitor()
    {
        $id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $national = $this->input->post('national');

        $this->db->where('id', $id);
        $upd = $this->db->update('trx_billing_visitor', [
            'visitor_name' => $nama,
            'phone' => $phone,
            'email' => $email,
            'nationality' => $national
        ]);

        $billing_id = $this->db->get_where('trx_billing_visitor', ['id' => $id])->row()->billing_id;
        $this->db->where('billing_id', $billing_id);
        $this->db->update('trx_ticket', [
            'visitor_name' => $nama
        ]);

        if ($upd == true) {
            $this->session->set_flashdata('success', 'Update data climber successfully..');
            redirect('visitor', 'refresh');
        } else {
            $this->session->set_flashdata('error', 'Oops, something wrong !!');
            redirect('visitor', 'refresh');
        }
    }

    public function save_edit_billing()
    {
        // test run
        $id = $this->input->post('id');
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $address = $this->input->post('address');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $country = $this->input->post('country');

        $this->db->where('id', $id);
        $upd = $this->db->update('trx_billing', [
            'first_name' => $fname,
            'last_name' => $lname,
            'country' => $country,
            'address' => $address,
            'phone' => $phone,
            'email' => $email
        ]);

        if ($upd == true) {
            $this->session->set_flashdata('success', 'Update data billing successfully..');
            redirect('billing', 'refresh');
        } else {
            $this->session->set_flashdata('error', 'Oops, something wrong !!');
            redirect('billing', 'refresh');
        }
    }

    function randString($tipe, $n)
    {
        if ($tipe == 'number') {
            $characters = '0123456789';
        } elseif ($tipe == 'string_number') {
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }

        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }

    public function save_lock_quota()
    {
        $destid = $this->input->post('dest');
        $date = $this->input->post('date');
        $jumlah = $this->input->post('jumlah');
        $remark = $this->input->post('remark');
        $rmk = ($remark !== '') ? $remark : null;
        $rand = '#' . $this->randString('string_number', 8);

        if ((int) $jumlah == 0) {
            $this->session->set_flashdata('error', 'Oops, cannot create lock quota !!');
            redirect('l-quota', 'refresh');
        } else {
            $no = $this->db->query("SELECT
                                    max(
                                    RIGHT ( receipt_id, 12 )) AS `no` 
                                FROM
                                    `trx_receipt` 
                                WHERE
                                    receipt_id LIKE 'SB%'")->row()->no;
            $no1 = '000000000000' . ($no + 1);
            $hash = 'SB' . substr($no1, -12);

            $ins = $this->db->insert('trx_billing', [
                'billing_id' => $rand,
                'buyerid' => 'lock',
                'first_name' => 'Sabah',
                'last_name' => 'Parks',
                'company_name' => '',
                'country' => 'MALAYSIA',
                'address' => '',
                'city' => '',
                'postcode' => '',
                'email' => '',
                'phone' => '',
                'total_pax' => $jumlah,
                'total_cost' => '',
                'status' => 1,
                'pr_holder' => 2,
                'status_approved' => 1,
                'receipt' => $hash,
                'note_lock_quota' => $rmk
            ]);

            $ins .= $this->db->insert('trx_billing_dtl', [
                'billing_id' => $rand,
                'buyerid' => 'lock',
                'destid' => $destid,
                'ticketdatefrom' => $date,
                'ticketdateto' => $date,
                'loc_qty_18above' => $jumlah,
                'loc_qty_18below' => 0,
                'int_qty_18above' => 0,
                'int_qty_18below' => 0,
                'createdate' => date('Y-m-d'),
                'status' => 1
            ]);

            for ($i = 1; $i <= (int) $jumlah; $i++) {
                // $no_t = $this->db->query("SELECT
                //                             max(
                //                             RIGHT ( ticket_id, 15 )) AS `no` 
                //                         FROM
                //                             `trx_ticket` 
                //                         WHERE
                //                             ticket_id LIKE 'TS%'")->row()->no;
                // $no_t1 = '000000000000000' . ($no + 1);
                // $ticketid = 'TSB' . substr($no1, -15);

                $this->db->insert('trx_billing_visitor', [
                    'billing_id' => $rand,
                    'buyerid' => 'lock',
                    'destid' => $destid,
                    'ticketdatefrom' => $date,
                    'ticketdateto' => $date,
                    'visitor_name' => 'Sabah Parks - Person ' . $i,
                    'id_number' => '-',
                    'email' => '-',
                    'phone' => '-',
                    'nationality' => 'MALAYSIA',
                    'gender' => 0,
                    'age' => 0,
                    'status' => 1
                ]);

                $tid = $hash . $this->randString('number', 4);
                $this->db->insert('trx_ticket', [
                    'ticket_id' => $tid,
                    'receipt_id' => $hash,
                    'billing_id' => $rand,
                    'buyerid' => 'lock',
                    'destid' => $destid,
                    'ticketdatefrom' => $date,
                    'ticketdateto' => $date,
                    'visitor_name' => 'Sabah Parks - Person ' . $i,
                    'id_number' => '-',
                    'email' => '-',
                    'phone' => '-',
                    'nationality' => 'MALAYSIA',
                    'gender' => 0,
                    'age' => 0,
                    'status' => 1
                ]);
            }

            $ins .= $this->db->insert('trx_receipt', [
                'receipt_id' => $hash,
                'billing_id' => $rand,
                'bank_name' => '-',
                'bank_no' => '-',
                'bank_acc' => '-',
                'remark' => '-',
                'amount' => '-',
                'status' => 1,
                'createdate' => date('Y-m-d H:i:s')
            ]);

            if ($ins == true) {
                $this->session->set_flashdata('success', 'Created lock quota successfully..');
                redirect('l-quota', 'refresh');
            } else {
                $this->session->set_flashdata('error', 'Oops, something wrong !!');
                redirect('l-quota', 'refresh');
            }
        }
    }

    public function insert_ticket()
    {
        $transid = $this->input->post('receipt_id');
        $billid = $this->input->post('order_id');
        $date = date('Y-m-d H:i:s');

        $get_billing = $this->db->get_where('trx_billing', ['billing_id' => $billid])->row();
        $this->db->query("INSERT INTO trx_receipt VALUES (null,'$transid','$billid','','','','-','$get_billing->total_cost','1','$date')");

        $get = $this->db->query("SELECT * FROM trx_billing_visitor where billing_id='$billid'")->result();
        foreach ($get as $val) {
            $tid = $transid . $val->id;
            $this->db->query("INSERT INTO trx_ticket VALUES (null, '$tid', '$transid', '$billid', '$val->buyerid','$val->destid','$val->ticketdatefrom','$val->ticketdateto','$val->visitor_name','$val->id_number','$val->email','$val->phone','$val->nationality','$val->gender','$val->age','$val->status',null, null)");
        }

        $this->db->query("UPDATE trx_billing set status='2' where billing_id='$billid'");

        $this->session->set_flashdata('success', 'Process & Save ticket successfully');
        redirect('send-doc');
    }

    public function vw_member()
    {
        $this->renderPage('modul/transaction/member');
    }

    public function vw_booking()
    {
        $this->renderPage('modul/transaction/booking');
    }

    public function vw_holder_appv()
    {
        $this->renderPage('modul/transaction/holder-approve');
    }

    public function vw_billing()
    {
        $data['dest'] = $this->db->query("SELECT * FROM `stp_dest` WHERE id in (1,2)")->result();
        $this->renderPage('modul/transaction/billing', $data);
    }

    public function vw_ticket()
    {
        $this->renderPage('modul/transaction/ticket');
    }

    public function vw_visitor()
    {
        $this->renderPage('modul/transaction/visitor');
    }

    public function vw_checkin()
    {
        $this->renderPage('modul/transaction/checkin');
    }

    public function vw_requery()
    {
        $this->renderPage('modul/transaction/re-query');
    }

    public function vw_send_doc()
    {
        $this->renderPage('modul/transaction/send-doc');
    }

    public function vw_paid_manual()
    {
        $orderid = $this->input->post('orderid');
        if (isset($orderid)) {
            $data['cek'] = $this->db->get_where('trx_billing', ['billing_id' => $orderid])->row();
        } else {
            $data['cek'] = '';
        }
        $this->renderPage('modul/transaction/paid-manual', $data);
    }

    public function vw_lock()
    {
        $arr = [1, 2];
        $data['destination'] = $this->db->from('stp_dest')->where_in('id', $arr)->get()->result();
        $this->renderPage('modul/transaction/lock-quota', $data);
    }

    public function vw_news()
    {
        $data['data'] = $this->db->query("SELECT
                                            a.*,
                                        IF
                                            ( a.`status` = 1, 'ACTIVE', 'NON-ACTIVE' ) AS ket_status 
                                        FROM
                                            `trx_blog` a 
                                        ORDER BY
                                            a.createdate DESC,
                                            a.id DESC");
        $this->renderPage('modul/transaction/news', $data);
    }

    public function vw_broadcast()
    {
        $data['data'] = $this->db->query("SELECT
                                            a.*,
                                            DATE( a.date_created ) AS new_date,
                                            DATE( a.expire_date ) AS new_date_exp,
                                        IF
                                            (
                                                a.id_type = 1,
                                                'News',
                                            IF
                                            ( a.id_type = 2, 'Promotion', 'Events' )) AS ket_type 
                                        FROM
                                            `trx_broadcast` a 
                                        ORDER BY
                                            a.date_created DESC");
        $this->renderPage('modul/transaction/broadcast', $data);
    }
}
