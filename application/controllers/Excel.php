<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Excel extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_report', 'mRept');
    }

    public function trial()
    {
        $spreadsheet = new Spreadsheet;
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'No');
        $sheet->setCellValue('B1', 'Nama');

        $siswa = $this->db->get('stp_menu')->result();
        $no = 1;
        $x = 2;
        foreach ($siswa as $row) {
            $sheet->setCellValue('A' . $x, $no++);
            $sheet->setCellValue('B' . $x, $row->menu);
            $x++;
        }

        $writer = new Xlsx($spreadsheet);
        $filename = 'laporan-siswa';

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }

    public function report_member()
    {
        $spreadsheet = new Spreadsheet;
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->getStyle('A1:F1')->getFont()->setBold(true);
        $sheet->getStyle('A1:F1')->getAlignment()->setHorizontal('center');

        $sheet->setCellValue('A1', 'NO.');
        $sheet->setCellValue('B1', 'FIRST NAME');
        $sheet->setCellValue('C1', 'LAST NAME');
        $sheet->setCellValue('D1', 'NATIONALITY');
        $sheet->setCellValue('E1', 'EMAIL');
        $sheet->setCellValue('F1', 'PHONE');

        $member = $this->db->query("select a.*, b.country_name 
                                    from stp_member a
                                    left join stp_country b on b.id = a.country
                                    where a.`status` = 1")->result();
        $no = 1;
        $x = 2;
        foreach ($member as $row) {
            $sheet->setCellValue('A' . $x, $no++);
            $sheet->setCellValue('B' . $x, $row->first_name);
            $sheet->setCellValue('C' . $x, $row->last_name);
            $sheet->setCellValue('D' . $x, $row->country);
            $sheet->setCellValue('E' . $x, $row->email);
            $sheet->setCellValue('F' . $x, $row->phone);
            $x++;
        }
        foreach (range('A', 'F') as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }
        $writer = new Xlsx($spreadsheet);
        $filename = 'Report-Member-' . date('Y/m/d');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }

    public function report_order($date_togo)
    {
        $spreadsheet = new Spreadsheet;
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->getStyle('A1:I1')->getFont()->setBold(true);
        $sheet->getStyle('A1:I1')->getAlignment()->setHorizontal('center');

        $sheet->setCellValue('A1', 'NO.');
        $sheet->setCellValue('B1', 'ORDER ID');
        $sheet->setCellValue('C1', 'ORDER DATE');
        $sheet->setCellValue('D1', 'DESTINATION');
        $sheet->setCellValue('E1', 'DATE TO GO');
        $sheet->setCellValue('F1', 'QTY');
        $sheet->setCellValue('G1', 'FIRS TNAME');
        $sheet->setCellValue('H1', 'LAST NAME');
        $sheet->setCellValue('I1', 'STATUS');

        $dtg = str_replace(' ', '%20', $date_togo);
        $member = $this->mRept->get_datatables_odr($dtg);
        $no = 1;
        $x = 2;
        foreach ($member as $row) {
            $sheet->setCellValue('A' . $x, $no++);
            $sheet->setCellValue('B' . $x, $row->billing_id);
            $sheet->setCellValue('C' . $x, $row->createdate);
            $sheet->setCellValue('D' . $x, $row->destname);
            $sheet->setCellValue('E' . $x, $row->ticketdatefrom);
            $sheet->setCellValue('F' . $x, $row->total_pax);
            $sheet->setCellValue('G' . $x, $row->first_name);
            $sheet->setCellValue('H' . $x, $row->last_name);
            $sheet->setCellValue('I' . $x, $row->status);
            $x++;
        }
        foreach (range('A', 'F') as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }
        $writer = new Xlsx($spreadsheet);
        $filename = 'Report-Booking-' . date('Y/m/d');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }

    public function report_pr_holder($date_togo)
    {
        $spreadsheet = new Spreadsheet;
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->getStyle('A1:I1')->getFont()->setBold(true);
        $sheet->getStyle('A1:I1')->getAlignment()->setHorizontal('center');

        $sheet->setCellValue('A1', 'NO.');
        $sheet->setCellValue('B1', 'ORDER ID');
        $sheet->setCellValue('C1', 'ORDER DATE');
        $sheet->setCellValue('D1', 'DESTINATION');
        $sheet->setCellValue('E1', 'DATE TO GO');
        $sheet->setCellValue('F1', 'QTY');
        $sheet->setCellValue('G1', 'FIRS TNAME');
        $sheet->setCellValue('H1', 'LAST NAME');
        $sheet->setCellValue('I1', 'APPROVED');

        $dtg = str_replace(' ', '%20', $date_togo);
        $member = $this->mRept->get_datatables_happv($dtg);
        $no = 1;
        $x = 2;
        foreach ($member as $row) {
            $sheet->setCellValue('A' . $x, $no++);
            $sheet->setCellValue('B' . $x, $row->billing_id);
            $sheet->setCellValue('C' . $x, $row->createdate);
            $sheet->setCellValue('D' . $x, $row->destname);
            $sheet->setCellValue('E' . $x, $row->ticketdatefrom);
            $sheet->setCellValue('F' . $x, $row->total_pax);
            $sheet->setCellValue('G' . $x, $row->first_name);
            $sheet->setCellValue('H' . $x, $row->last_name);
            $sheet->setCellValue('I' . $x, $row->ket_approve);
            $x++;
        }
        foreach (range('A', 'F') as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }
        $writer = new Xlsx($spreadsheet);
        $filename = 'Report-PR-Holder-' . date('Y/m/d');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }

    public function report_billing($date_togo)
    {
        $spreadsheet = new Spreadsheet;
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->getStyle('A1:N1')->getFont()->setBold(true);
        $sheet->getStyle('A1:N1')->getAlignment()->setHorizontal('center');

        $sheet->setCellValue('A1', 'NO.');
        $sheet->setCellValue('B1', 'ORDER ID');
        $sheet->setCellValue('C1', 'ORDER DATE');
        $sheet->setCellValue('D1', 'FIRST NAME');
        $sheet->setCellValue('E1', 'LAST NAME');
        $sheet->setCellValue('F1', 'COUNTRY');
        $sheet->setCellValue('G1', 'ADDRESS');
        $sheet->setCellValue('H1', 'EMAIL');
        $sheet->setCellValue('I1', 'PHONE');
        $sheet->setCellValue('J1', 'DESTINATION');
        $sheet->setCellValue('K1', 'DATE TO GO');
        $sheet->setCellValue('L1', 'PAYMENT REF.NO');
        $sheet->setCellValue('M1', 'PAYMENT DATE');
        $sheet->setCellValue('N1', 'STATUS');

        $dtg = str_replace(' ', '%20', $date_togo);
        $member = $this->mRept->get_datatables_bill($dtg);
        $no = 1;
        $x = 2;
        foreach ($member as $row) {
            $sheet->setCellValue('A' . $x, $no++);
            $sheet->setCellValue('B' . $x, $row->billing_id);
            $sheet->setCellValue('C' . $x, $row->createdate);
            $sheet->setCellValue('D' . $x, $row->first_name);
            $sheet->setCellValue('E' . $x, $row->last_name);
            $sheet->setCellValue('F' . $x, $row->country);
            $sheet->setCellValue('G' . $x, $row->address);
            $sheet->setCellValue('H' . $x, $row->email);
            $sheet->setCellValue('I' . $x, $row->phone);
            $sheet->setCellValue('J' . $x, $row->destname);
            $sheet->setCellValue('K' . $x, $row->ticketdatefrom);
            $sheet->setCellValue('L' . $x, $row->receipt_id);
            $sheet->setCellValue('M' . $x, $row->pay_date);
            $sheet->setCellValue('N' . $x, $row->ket_status);
            $x++;
        }
        foreach (range('A', 'N') as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }
        $writer = new Xlsx($spreadsheet);
        $filename = 'Report-Billing-' . date('Y/m/d');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }

    public function report_sales($date_togo)
    {
        $spreadsheet = new Spreadsheet;
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->getStyle('A1:L1')->getFont()->setBold(true);
        $sheet->getStyle('A1:L1')->getAlignment()->setHorizontal('center');

        $sheet->setCellValue('A1', 'NO.');
        $sheet->setCellValue('B1', 'BILLING ID');
        $sheet->setCellValue('C1', 'RECEIPT ID');
        $sheet->setCellValue('D1', 'COUNTRY');
        $sheet->setCellValue('E1', 'AGE 18 ABOVE');
        $sheet->setCellValue('F1', 'AGE 18 BELOW');
        $sheet->setCellValue('G1', 'DESTINATION');
        $sheet->setCellValue('H1', 'BANK');
        $sheet->setCellValue('I1', 'BANK NO.');
        $sheet->setCellValue('J1', 'BANK ACC');
        $sheet->setCellValue('K1', 'AMOUNT');
        $sheet->setCellValue('L1', 'REMARK');

        $dtg = str_replace(' ', '%20', $date_togo);
        $member = $this->mRept->get_datatables_sales($dtg);
        $no = 1;
        $x = 2;
        foreach ($member as $row) {
            $sheet->setCellValue('A' . $x, $no++);
            $sheet->setCellValue('B' . $x, $row->billing_id);
            $sheet->setCellValue('C' . $x, $row->receipt_id);
            $sheet->setCellValue('D' . $x, $row->country);
            $sheet->setCellValue('E' . $x, $row->age_18above);
            $sheet->setCellValue('F' . $x, $row->age_18below);
            $sheet->setCellValue('G' . $x, $row->destname);
            $sheet->setCellValue('H' . $x, $row->bank_name);
            $sheet->setCellValue('I' . $x, $row->bank_no);
            $sheet->setCellValue('J' . $x, $row->bank_acc);
            $sheet->setCellValue('K' . $x, $row->amount);
            $sheet->setCellValue('L' . $x, $row->remark);
            $x++;
        }
        foreach (range('A', 'L') as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }
        $writer = new Xlsx($spreadsheet);
        $filename = 'Report-Sales-' . date('Y/m/d');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }

    public function report_ipay($date_togo)
    {
        $spreadsheet = new Spreadsheet;
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->getStyle('A1:M1')->getFont()->setBold(true);
        $sheet->getStyle('A1:M1')->getAlignment()->setHorizontal('center');

        $sheet->setCellValue('A1', 'NO.');
        $sheet->setCellValue('B1', 'REF.NO');
        $sheet->setCellValue('C1', 'AMOUNT');
        $sheet->setCellValue('D1', 'CURRENCY');
        $sheet->setCellValue('E1', 'REMARK');
        $sheet->setCellValue('F1', 'TRANS. ID');
        $sheet->setCellValue('G1', 'AUTH. CODE');
        $sheet->setCellValue('H1', 'ERROR. DESC');
        $sheet->setCellValue('I1', 'CC.NAME');
        $sheet->setCellValue('J1', 'CC.NO');
        $sheet->setCellValue('K1', 'COUNTRY');
        $sheet->setCellValue('L1', 'SIGNATURE');
        $sheet->setCellValue('M1', 'STATUS');

        $dtg = str_replace(' ', '%20', $date_togo);
        $member = $this->mRept->get_datatables_ipay88($dtg);
        $no = 1;
        $x = 2;
        foreach ($member as $row) {
            $sheet->setCellValue('A' . $x, $no++);
            $sheet->setCellValue('B' . $x, $row->RefNo);
            $sheet->setCellValue('C' . $x, $row->Amount);
            $sheet->setCellValue('D' . $x, $row->Currency);
            $sheet->setCellValue('E' . $x, $row->Remark);
            $sheet->setCellValue('F' . $x, $row->TransId);
            $sheet->setCellValue('G' . $x, $row->AuthCode);
            $sheet->setCellValue('H' . $x, $row->ErrDesc);
            $sheet->setCellValue('I' . $x, $row->CCName);
            $sheet->setCellValue('J' . $x, $row->CCNo);
            $sheet->setCellValue('K' . $x, $row->S_country);
            $sheet->setCellValue('L' . $x, $row->Signature);
            $sheet->setCellValue('M' . $x, $row->ket_status);
            $x++;
        }
        foreach (range('A', 'M') as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }
        $writer = new Xlsx($spreadsheet);
        $filename = 'Report-Response-Ipay88-' . date('Y/m/d');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }

    public function report_paid($option, $date_togo)
    {
        $spreadsheet = new Spreadsheet;
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->getStyle('A1:I1')->getFont()->setBold(true);
        $sheet->getStyle('A1:I1')->getAlignment()->setHorizontal('center');

        $sheet->setCellValue('A1', 'NO.');
        $sheet->setCellValue('B1', 'ORDER ID');
        $sheet->setCellValue('C1', 'ORDER DATE');
        $sheet->setCellValue('D1', 'DESTINATION');
        $sheet->setCellValue('E1', 'DATE TO GO');
        $sheet->setCellValue('F1', 'QTY');
        $sheet->setCellValue('G1', 'FIRSTNAME');
        $sheet->setCellValue('H1', 'LASTNAME');
        $sheet->setCellValue('I1', 'STATUS');

        $dtg = str_replace(' ', '%20', $date_togo);
        $member = $this->mRept->get_datatables_paid($option, $dtg);
        $no = 1;
        $x = 2;
        foreach ($member as $row) {
            $sheet->setCellValue('A' . $x, $no++);
            $sheet->setCellValue('B' . $x, $row->billing_id);
            $sheet->setCellValue('C' . $x, $row->createdate);
            $sheet->setCellValue('D' . $x, $row->destname);
            $sheet->setCellValue('E' . $x, $row->ticketdatefrom);
            $sheet->setCellValue('F' . $x, $row->total_pax);
            $sheet->setCellValue('G' . $x, $row->first_name);
            $sheet->setCellValue('H' . $x, $row->last_name);
            $sheet->setCellValue('I' . $x, $row->status);
            $x++;
        }
        foreach (range('A', 'I') as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }
        $writer = new Xlsx($spreadsheet);
        $filename = 'Report-Paid-Booking-' . date('Y/m/d');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }

    public function report_climbchange($date_togo)
    {
        $spreadsheet = new Spreadsheet;
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->getStyle('A1:H1')->getFont()->setBold(true);
        $sheet->getStyle('A1:H1')->getAlignment()->setHorizontal('center');
      
        $sheet->setCellValue('A1', 'NO.');
        $sheet->setCellValue('B1', 'ORDER ID');
        $sheet->setCellValue('C1', 'ORDER DATE');
        $sheet->setCellValue('D1', 'DESTINATION');
        $sheet->setCellValue('E1', 'DATE TO GO');
        $sheet->setCellValue('F1', 'NEW DATE TO GO');
        $sheet->setCellValue('G1', 'CLIMBER NAME');
        $sheet->setCellValue('H1', 'STATUS');

        $dtg = str_replace(' ', '%20', $date_togo);
        $member = $this->mRept->get_datatables_change($dtg);
        $no = 1;
        $x = 2;
        foreach ($member as $row) {
            $sheet->setCellValue('A' . $x, $no++);
            $sheet->setCellValue('B' . $x, $row->billing_id);
            $sheet->setCellValue('C' . $x, $row->createdate);
            $sheet->setCellValue('D' . $x, $row->destname);
            $sheet->setCellValue('E' . $x, $row->date_before);
            $sheet->setCellValue('F' . $x, $row->date_togo);
            $sheet->setCellValue('G' . $x, $row->n_visname);
            $sheet->setCellValue('H' . $x, $row->ticket_status);
            $x++;
        }
        foreach (range('A', 'H') as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }
        $writer = new Xlsx($spreadsheet);
        $filename = 'Report-Climber-Change-' . date('Y/m/d');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }

    public function report_cancel_ps($date_togo)
    {
        $spreadsheet = new Spreadsheet;
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->getStyle('A1:I1')->getFont()->setBold(true);
        $sheet->getStyle('A1:I1')->getAlignment()->setHorizontal('center');
      
        $sheet->setCellValue('A1', 'NO.');
        $sheet->setCellValue('B1', 'ORDER ID');
        $sheet->setCellValue('C1', 'ORDER DATE');
        $sheet->setCellValue('D1', 'DESTINATION');
        $sheet->setCellValue('E1', 'DATE TO GO');
        $sheet->setCellValue('F1', 'CLIMBER NAME');
        $sheet->setCellValue('G1', 'NOTE CANCEL');
        $sheet->setCellValue('H1', 'USER SUBMITTED');
        $sheet->setCellValue('I1', 'DATE SUBMITTED');

        $dtg = str_replace(' ', '%20', $date_togo);
        $member = $this->mRept->get_datatables_cancel($dtg);
        $no = 1;
        $x = 2;
        foreach ($member as $row) {
            $sheet->setCellValue('A' . $x, $no++);
            $sheet->setCellValue('B' . $x, $row->billing_id);
            $sheet->setCellValue('C' . $x, $row->date_create);
            $sheet->setCellValue('D' . $x, $row->destname);
            $sheet->setCellValue('E' . $x, $row->date_togo);
            $sheet->setCellValue('F' . $x, $row->new_name);
            $sheet->setCellValue('G' . $x, $row->note);
            $sheet->setCellValue('H' . $x, $row->nama_lengkap);
            $sheet->setCellValue('I' . $x, $row->upd_time);
            $x++;
        }
        foreach (range('A', 'I') as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }
        $writer = new Xlsx($spreadsheet);
        $filename = 'Report-Cancel-Climber-' . date('Y/m/d');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }
}
