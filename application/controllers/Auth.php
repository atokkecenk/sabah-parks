<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('login');
    }

    public function cek_login()
    {
        $email = $this->input->post('email');
        $password = md5($this->input->post('password'));

        $user = $this->mUser->get($email);
        if (empty($user)) {
            $this->session->set_flashdata('error', 'Email <b>not found !!</b>');
            redirect('auth');
        } else {
            if ($password == $user->new_password) {
                $session = array(
                    'authenticated' => true,
                    'id_user' => $user->id_user,
                    'id_role' => $user->id_role,
                    'level' => $user->level,
                    'username' => $user->username,
                    'nama_lengkap' => $user->nama_lengkap,
                );

                $this->session->set_flashdata('success', 'Welcome..');
                $this->session->set_userdata($session);
                redirect('dashboard');
            } else {
                $this->session->set_flashdata('error', '<b>Wrong</b> Password !!');
                redirect('auth');
            }
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url());
    }
}
