<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_setup extends CI_Model
{
    function get_destination()
    {
        return $this->db->query("SELECT
                                a.*,
                                ( a.loc_quota + a.int_quota ) AS qplus,
                                b.desttype AS type 
                            FROM
                                stp_dest a
                                LEFT JOIN stp_dest_type b ON a.desttype = b.id
                                LEFT JOIN stp_dest_price c ON a.destid = c.destid 
                            WHERE
                                a.id in (1, 2)
                            GROUP BY
	                            a.destid 
                            ORDER BY 
                                a.id ASC
                                -- AND c.StartPeriod <= NOW() AND c.EndPeriod >= NOW()
                        ");
    }

    function get_gallery()
    {
        return $this->db->query("SELECT
                                    a.*,
                                    b.desttype,
                                    SUBSTRING_INDEX( a.path, '.', 1 ) AS `filename`,
	                                SUBSTRING_INDEX( a.path, '.',- 1 ) AS ext
                                FROM
                                    stp_gallery a
                                    LEFT JOIN stp_dest_type b ON b.id = a.type 
                                ORDER BY
                                    a.createdate DESC
                                ");
    }

    function edit_gallery($id)
    {
        return $this->db->query("SELECT
                                    a.*,
                                    b.desttype,
                                    SUBSTRING_INDEX( a.path, '.', 1 ) AS `filename`,
	                                SUBSTRING_INDEX( a.path, '.',- 1 ) AS ext
                                FROM
                                    stp_gallery a
                                    LEFT JOIN stp_dest_type b ON b.id = a.type 
                                WHERE a.id = '$id'
                                ");
    }

    function get_users()
    {
        return $this->db->query("SELECT
                                    a.role_name,
                                    b.* 
                                FROM
                                    rb_users_role a
                                    LEFT JOIN rb_users b ON a.id = b.id_role
                                    ");
    }

    function edit_users($id)
    {
        return $this->db->query("SELECT
                                    a.role_name,
                                    b.* 
                                FROM
                                    rb_users_role a
                                    LEFT JOIN rb_users b ON a.id = b.id_role
                                WHERE 
                                    b.id_user = '$id'
                                    ");
    }

    function cek_active_menu($id_menu, $id_role)
    {
        return $this->db->query("SELECT
                                    COUNT( b.id_menu ) AS hitung,
                                    b.id_menu,
                                    b.`status`,
                                    a.id,
                                    a.menu,
                                    IF(a.id_root is null,a.menu,CONCAT('(submenu) ', a.menu)) as nm_menu,
                                    a.`status` as sts_menu
                                FROM
                                    `stp_menu` a
                                    JOIN stp_role_menu b ON a.id = b.id_menu 
                                WHERE
                                    a.id = '$id_menu' 
                                    AND b.id_role = '$id_role' 
                                    AND b.`status` = '1'
                                ORDER BY a.id ASC");
    }

    function avalaible_quota($dest_id, $date)
    {
        return $this->db->query("SELECT
                                COUNT( a.id ) AS hitung,
                                a.ticketdatefrom,
                                a.destid,
                                c.destname,
                                c.loc_quota,
                                c.int_quota,
                                ( c.loc_quota + c.int_quota ) AS tot_quota 
                            FROM
                                trx_billing_visitor a
                                LEFT JOIN trx_billing b ON a.billing_id = b.billing_id
                                LEFT JOIN stp_dest c ON a.destid = c.destid 
                                WHERE
                                a.ticketdatefrom = '$date'
                                AND b.`status` != '3' 
                                AND a.destid = '$dest_id' 
                            GROUP BY
                                a.ticketdatefrom");

        // return $this->db->query("SELECT
        //                         COUNT( a.id ) AS hitung,
        //                         a.`status` AS id_status,
        //                     IF
        //                         (
        //                             a.`status` = 1,
        //                             'UNPAID',
        //                         IF
        //                             (
        //                                 a.`status` = 2 && a.receipt IS NULL,
        //                                 'PAID IPAY88',
        //                             IF
        //                                 ( a.`status` = 2 && a.receipt IS NOT NULL, 'PAID (Manual)', 'CANCEL' ) 
        //                             )) AS `status`,
        //                         max( b.ticketdatefrom ) AS ticketdatefrom,
        //                         d.destid,
        //                         max( d.destname ) AS destname 
        //                     FROM
        //                         trx_billing a
        //                         LEFT JOIN trx_billing_dtl b ON a.billing_id = b.billing_id
        //                         LEFT JOIN stp_member c ON a.buyerid = c.buyerid
        //                         LEFT JOIN stp_dest d ON b.destid = d.destid 
        //                     WHERE
        //                         d.destid = '$dest_id' 
        //                         AND a.`status` != 3 
        //                         AND b.ticketdatefrom = '$date' 
        //                     GROUP BY
        //                         d.destid");
    }

    var $col_order = array('a.id', 'a.visitor_name');
    var $col_search = array('a.id', 'a.visitor_name');
    var $order = array('a.id' => 'desc');

    private function _sql_user_role()
    {
        $this->db->select("a.*");
        $this->db->from('stp_menu a');
        // $this->db->join('stp_menu b', 'a.id_menu = b.id', 'left');
        $this->db->where('a.status', 1);

        $i = 0;
        foreach ($this->col_search as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->col_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->col_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables()
    {
        $this->_sql_user_role();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_sql_user_role();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all()
    {
        $this->db->from('stp_menu');
        return $this->db->count_all_results();
    }
}
