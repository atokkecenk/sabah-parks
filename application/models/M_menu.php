<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_menu extends CI_Model
{

    public function __mn_lev1($id_role)
    {
        $q = $this->db->query("SELECT
                                a.*,
                                b.id as id_menu,
                                b.id_root,
                                b.menu,
                                b.url as `route`,
                                b.icon_fa5 AS icon,
                                b.`order` 
                            FROM
                                `stp_role_menu` a
                                LEFT JOIN stp_menu b ON a.id_menu = b.id 
                            WHERE
                                a.id_role = '$id_role' 
                                AND a.`status` = '1' 
                                AND b.`status` = '1'
                                AND b.id_root IS NULL
                            ORDER BY
                                b.`order` ASC");
        return $q;
    }

    public function __mn_lev2($id_role, $id_root)
    {
        $q = $this->db->query("SELECT
                                a.*,
                                b.id as id_menu,
                                b.id_root,
                                b.menu,
                                b.url as `route`,
                                b.icon_fa5 as icon,
                                b.`order` 
                            FROM
                                `stp_role_menu` a
                                LEFT JOIN stp_menu b ON a.id_menu = b.id 
                            WHERE
                                a.id_role = '$id_role' 
                                AND a.`status` = '1' 
                                AND b.`status` = '1'
                                AND b.id_root = '$id_root'
                            ORDER BY
                                b.`order` ASC");
        return $q;
    }
}
