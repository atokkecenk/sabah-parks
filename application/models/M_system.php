<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_system extends CI_Model
{

    function getHistory($limit, $whr)
    {
        if (!empty($limit) || $limit !== '') {
            $lim = 'LIMIT ' . $limit;
        }
        $sql = $this->db->query("SELECT
                                        a.*,
                                        b.category AS cat_name,
                                    IF
                                        (
                                            a.category = 1,
                                            CONCAT( b.category, '<br>Billing Id: <b>#', a.billing_id, '</b> by ', c.nama_lengkap ),
                                        IF
                                            (
                                                a.category = 2,
                                                CONCAT( b.category, '<br>Billing Id: <b>#', a.billing_id, '</b> by ', c.nama_lengkap ),
                                            IF
                                                (
                                                    a.category = 3,
                                                    CONCAT( b.category, '<br>Order Id: <b>#', a.billing_id, '</b> by ', c.nama_lengkap ),
                                                IF
                                                    (
                                                        a.category = 4,
                                                        CONCAT(
                                                            b.category,
                                                            '<br>Ticket ID: <b>',
                                                            a.billing_id,
                                                            '</b> note <i>',
                                                        IF
                                                            (
                                                                d.check_update = 'C',
                                                                'CORRUPTED',
                                                            IF
                                                            ( d.check_update = 'L', 'LOST', 'INVALID' )),
                                                            '</i> by ',
                                                            c.nama_lengkap 
                                                        ),
                                                    IF
                                                        ( a.category = 5, CONCAT( b.category, '<br>Billing Id: <b>#', a.billing_id, '</b> by ', c.nama_lengkap ), 'not defined' ) 
                                                    ) 
                                                ) 
                                            ) 
                                        ) AS cat_name2,
                                        DATE_FORMAT( a.update_time, '%d %M %Y, %l:%i %p' ) AS `date`,
                                        c.nama_lengkap 
                                    FROM
                                        `tb_history` a
                                        LEFT JOIN tb_cat_history b ON a.category = b.id
                                        LEFT JOIN rb_users c ON a.user_update = c.id_user
                                        LEFT JOIN trx_ticket d ON a.billing_id = d.ticket_id 
                                    ORDER BY
                                        id DESC
                                        $lim
                                    ");
        return $sql;
    }

    function diagram()
    {
        return $this->db->query("SELECT
                                    a.desttype,
                                    c.desttype AS destname,
                                    COUNT( b.ticket_id ) AS total 
                                FROM
                                    stp_dest a
                                    LEFT JOIN trx_ticket b ON b.destid = a.destid
                                    LEFT JOIN stp_dest_type c ON a.desttype = c.id 
                                GROUP BY
                                    a.desttype 
                                ORDER BY
                                    a.desttype ASC
                                ");
    }

    function sales_comparison($start, $end)
    {
        return $this->db->query("SELECT MONTH
                                    ( createdate ) AS `month`,
                                    COUNT( receipt_id ) AS total 
                                FROM
                                    trx_receipt 
                                WHERE
                                    createdate >= '$start' 
                                    AND createdate <= '$end' 
                                GROUP BY
                                    MONTH ( createdate ) 
                                ORDER BY
                                    MONTH ( createdate ) ASC");
    }

    function visitor_chart()
    {
        return $this->db->query("SELECT
                                    b.country,
                                    COUNT( a.ticket_id ) AS total 
                                FROM
                                    trx_ticket a
                                    LEFT JOIN trx_billing b ON a.billing_id = b.billing_id 
                                GROUP BY
                                    b.country");
    }
}
