<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_report extends CI_Model
{
    // MEMBER
    var $column_order = array('a.id', 'a.first_name', 'a.last_name', 'a.country', 'a.email', 'a.phone');
    var $column_search = array('a.id', 'a.first_name', 'a.last_name', 'a.country', 'a.email', 'a.phone');
    var $order = array('a.first_name' => 'desc');

    public function _sql_member()
    {
        $this->db->select('a.*, b.country_name');
        $this->db->from('stp_member a');
        $this->db->join('stp_country b', 'b.id = a.country', 'left');
        $this->db->where('a.`status`', 1);
        $i = 0;
        foreach ($this->column_search as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_member()
    {
        $this->_sql_member();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_sql_member();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all()
    {
        $this->db->from('stp_member');
        return $this->db->count_all_results();
    }
    // MEMBER


    // ORDER/BOOKING
    var $col_order_odr = array('a.id', 'a.billing_id', 'b.createdate', 'd.destname', 'b.ticketdatefrom', 'a.total_pax', 'a.first_name', 'a.last_name', 'a.`status`');
    var $col_search_odr = array('a.id', 'a.billing_id', 'b.createdate', 'd.destname', 'b.ticketdatefrom', 'a.total_pax', 'a.first_name', 'a.last_name');
    var $order_odr = array('a.id' => 'asc');

    public function _sql_order($date_togo)
    {
        $this->db->select("a.id,
                             b.id as id_update,
                             a.billing_id,
                             a.total_pax,
                             a.first_name,
                             a.last_name,
                             a.`status` AS id_status,
                         IF
                             (
                                 a.`status` = 1,
                                 'UNPAID',
                             IF
                             ( a.`status` = 2, 'PAID', 'CANCEL' )) AS `status`,
                             max( b.ticketdatefrom ) AS ticketdatefrom,
                             max( b.createdate ) AS createdate,
                             d.destid,
                             max( d.destname ) AS destname");
        $this->db->from('trx_billing a');
        $this->db->join('trx_billing_dtl b', 'a.billing_id = b.billing_id', 'left');
        $this->db->join('stp_member c', 'a.buyerid = c.buyerid', 'left');
        $this->db->join('stp_dest d', 'b.destid = d.destid', 'left');

        if ($date_togo !== '0') {
            $replace = explode('%20', $date_togo);
            $d1 = $replace[0];
            $d2 = $replace[2];
            $this->db->where("b.ticketdatefrom BETWEEN '$d1' AND '$d2'");
        }
        $i = 0;
        foreach ($this->col_search_odr as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->col_search_odr) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        $this->db->group_by(['a.id', 'a.billing_id', 'a.`status`']);

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->col_order_odr[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_odr)) {
            $order = $this->order_odr;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    public function get_datatables_odr($date_togo)
    {
        $this->_sql_order($date_togo);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_odr($date_togo)
    {
        $this->_sql_order($date_togo);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_odr()
    {
        $this->db->from('trx_billing');
        return $this->db->count_all_results();
    }
    // ORDER/BOOKING


    // HOLDER-APPROVE
    var $col_order_happv = array('a.id', 'a.billing_id', 'b.createdate', 'd.destname', 'b.ticketdatefrom', 'a.total_pax', 'a.first_name', 'a.last_name', 'a.`status_approved`');
    var $col_search_happv = array('a.id', 'a.billing_id', 'a.total_pax', 'a.first_name', 'a.last_name', 'b.ticketdatefrom', 'b.createdate', 'd.destname');
    var $order_happv = array('b.ticketdatefrom' => 'desc', 'a.id' => 'desc');

    public function _sql_holder_appv($date_togo)
    {
        $this->db->select("a.id,
                            a.billing_id,
                            a.total_pax,
                            a.first_name,
                            a.last_name,
                            a.status_approved,
                        IF
                            (
                                a.`status_approved` = 1,
                                '-',
                            IF
                            ( a.`status_approved` = 2, 'APPROVED', 'REJECTED' )) AS `ket_approve`,
                            a.`status`,
                            max( b.ticketdatefrom ) AS ticketdatefrom,
                            max( b.createdate ) AS createdate,
                            max( d.destname ) AS destname");
        $this->db->from('trx_billing a');
        $this->db->join('trx_billing_dtl b', 'a.billing_id = b.billing_id', 'left');
        $this->db->join('stp_member c', 'a.buyerid = c.buyerid', 'left');
        $this->db->join('stp_dest d', 'b.destid = d.destid', 'left');
        $this->db->where('a.pr_holder', 1);

        if ($date_togo !== '0') {
            $replace = explode('%20', $date_togo);
            $d1 = $replace[0];
            $d2 = $replace[2];
            $this->db->where("b.ticketdatefrom BETWEEN '$d1' AND '$d2'");
        }
        $i = 0;
        foreach ($this->col_search_happv as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->col_search_happv) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        $this->db->group_by(['a.id', 'a.billing_id', 'a.`status`']);

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->col_order_happv[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_happv)) {
            $order = $this->order_happv;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    public function get_datatables_happv($date_togo)
    {
        $this->_sql_holder_appv($date_togo);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_happv($date_togo)
    {
        $this->_sql_holder_appv($date_togo);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_happv()
    {
        $this->db->from('trx_billing');
        $this->db->where('pr_holder', 1);
        return $this->db->count_all_results();
    }
    // HOLDER-APPROVE


    // BILLING
    var $col_order_bill = array('a.id', 'a.billing_id', 'b.createdate', 'a.first_name', 'a.last_name', 'a.country', 'a.address', 'a.email', 'a.phone', 'd.destname', 'b.ticketdatefrom', 'c.receipt_id', 'c.createdate', 'a.`status`');
    var $col_search_bill = array('a.id', 'a.billing_id', 'b.createdate', 'a.first_name', 'a.last_name', 'a.country', 'a.address', 'a.email', 'a.phone', 'c.receipt_id', 'c.createdate', 'b.ticketdatefrom');
    var $order_bill = array('a.id' => 'asc');

    private function _sql_holder_bill($date_togo)
    {
        $this->db->select("a.id,
                            a.billing_id,
                            max( b.createdate ) AS createdate,
                            max( a.first_name ) AS first_name,
                            max( a.last_name ) AS last_name,
                            max( a.country ) AS country,
                            max( a.address ) AS address,
                            max( a.email ) AS email,
                            max( a.phone ) AS phone,
                            a.`status`,
                        IF
                            (
                                a.`status` = 1,
                                'UNPAID',
                            IF
                            ( a.`status` = 2, 'PAID', 'CANCEL' )) AS ket_status,
                            max( d.destname ) AS destname,
                            c.receipt_id,
                            max( c.createdate ) AS pay_date,
                            max( b.ticketdatefrom ) AS ticketdatefrom");
        $this->db->from('trx_billing a');
        $this->db->join('trx_billing_dtl b', 'a.billing_id = b.billing_id AND a.buyerid = b.buyerid', 'left');
        $this->db->join('trx_receipt c', 'a.billing_id = c.billing_id', 'left');
        $this->db->join('stp_dest d', 'b.destid = d.destid', 'left');
        if ($date_togo !== '0') {
            $replace = explode('%20', $date_togo);
            $d1 = $replace[0];
            $d2 = $replace[2];
            $this->db->where("b.ticketdatefrom BETWEEN '$d1' AND '$d2'");
        }
        $i = 0;
        foreach ($this->col_search_bill as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->col_search_bill) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        $this->db->group_by(['a.id', 'a.billing_id', 'a.`status`', 'c.receipt_id']);

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->col_order_bill[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_bill)) {
            $order = $this->order_bill;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_bill($date_togo)
    {
        $this->_sql_holder_bill($date_togo);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_bill($date_togo)
    {
        $this->_sql_holder_bill($date_togo);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_bill()
    {
        $this->db->from('trx_billing');
        return $this->db->count_all_results();
    }
    // BILLING

    // REPORT SALES
    var $col_order_sales = array('a.id', 'b.billing_id', 'a.receipt_id', 'd.destname', 'a.bank_name', 'a.bank_no', 'a.bank_acc', 'a.amount', 'a.remark');
    var $col_search_sales = array('a.id', 'b.billing_id', 'a.receipt_id', 'd.destname', 'a.bank_name', 'a.bank_no', 'a.bank_acc', 'a.amount', 'a.remark');
    var $order_sales = array('a.receipt_id' => 'desc');

    private function _sql_sales($date_togo)
    {
        $this->db->select("a.*,
                            d.destname,
                            b.country,
                            ( c.loc_qty_18above + c.int_qty_18above ) AS age_18above,
                            ( c.loc_qty_18below + c.int_qty_18below ) AS age_18below");
        $this->db->from('trx_receipt a');
        $this->db->join('trx_billing b', 'a.billing_id = b.billing_id', 'left');
        $this->db->join('trx_billing_dtl c', 'a.billing_id = c.billing_id', 'left');
        $this->db->join('stp_dest d', 'c.destid = d.destid', 'left');
        $this->db->where('a.receipt_id !=', 'TXXXXXXXXX725');
        if ($date_togo !== '0') {
            $replace = explode('%20', $date_togo);
            $d1 = $replace[0];
            $d2 = $replace[2];
            // $this->db->where("a.createdate BETWEEN '$d1' AND '$d2'");
            $this->db->where("a.createdate BETWEEN '$d1' AND '$d2'");
        }
        $i = 0;
        foreach ($this->col_search_sales as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->col_search_sales) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->col_order_sales[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_sales)) {
            $order = $this->order_sales;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_sales($date_togo)
    {
        $this->_sql_sales($date_togo);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_sales($date_togo)
    {
        $this->_sql_sales($date_togo);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_sales()
    {
        $this->db->from('trx_receipt');
        $this->db->where('trx_receipt.receipt_id !=', 'TXXXXXXXXX725');
        return $this->db->count_all_results();
    }
    // REPORT SALES


    // REPORT IPAY88
    var $col_order_ipay88 = array('trx_responseipay88.PaymentId', 'trx_responseipay88.RefNo', 'trx_responseipay88.Amount', 'trx_responseipay88.Currency', 'trx_responseipay88.Remark', 'trx_responseipay88.TransId', 'trx_responseipay88.AuthCode', 'trx_responseipay88.ErrDesc', 'trx_responseipay88.Signature', 'trx_responseipay88.CCName', 'trx_responseipay88.CCNo', 'trx_responseipay88.S_bankname', 'trx_responseipay88.S_country', 'trx_responseipay88.`Status`');
    var $col_search_ipay88 = array('trx_responseipay88.PaymentId', 'trx_responseipay88.RefNo', 'trx_responseipay88.Amount', 'trx_responseipay88.Currency', 'trx_responseipay88.Remark', 'trx_responseipay88.TransId', 'trx_responseipay88.AuthCode', 'trx_responseipay88.ErrDesc', 'trx_responseipay88.Signature', 'trx_responseipay88.CCName', 'trx_responseipay88.CCNo', 'trx_responseipay88.S_bankname', 'trx_responseipay88.S_country');
    var $order_ipay88 = array('trx_responseipay88.TransId' => 'asc');

    private function _sql_ipay88($date_togo)
    {
        $this->db->select("IF
                            ( trx_responseipay88.`Status` = 1, 'SUCCESS', 'FAILED' ) AS ket_status,
                            MAX( trx_receipt.createdate ) AS createdate,
                            trx_responseipay88.PaymentId,
                            trx_responseipay88.RefNo,
                            trx_responseipay88.Amount,
                            trx_responseipay88.Currency,
                            trx_responseipay88.Remark,
                            trx_responseipay88.TransId,
                            trx_responseipay88.AuthCode,
                            trx_responseipay88.`Status`,
                            trx_responseipay88.ErrDesc,
                            trx_responseipay88.Signature,
                            trx_responseipay88.CCName,
                            trx_responseipay88.CCNo,
                            trx_responseipay88.S_bankname,
                            trx_responseipay88.S_country");
        $this->db->from('trx_responseipay88');
        $this->db->join('trx_receipt', 'trx_responseipay88.TransId = trx_receipt.receipt_id ', 'left');
        if ($date_togo !== '0') {
            $replace = explode('%20', $date_togo);
            $d1 = $replace[0];
            $d2 = $replace[2];
            $this->db->where("trx_receipt.createdate BETWEEN '$d1' AND '$d2'");
        }
        $i = 0;
        foreach ($this->col_search_ipay88 as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->col_search_ipay88) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        $this->db->group_by(['trx_responseipay88.PaymentId', 'trx_responseipay88.RefNo', 'trx_responseipay88.Amount', 'trx_responseipay88.Currency', 'trx_responseipay88.Remark', 'trx_responseipay88.TransId', 'trx_responseipay88.AuthCode', 'trx_responseipay88.`Status`', 'trx_responseipay88.ErrDesc', 'trx_responseipay88.Signature', 'trx_responseipay88.CCName', 'trx_responseipay88.CCNo', 'trx_responseipay88.S_bankname', 'trx_responseipay88.S_country']);

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->col_order_ipay88[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_ipay88)) {
            $order = $this->order_ipay88;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_ipay88($date_togo)
    {
        $this->_sql_ipay88($date_togo);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_ipay88($date_togo)
    {
        $this->_sql_ipay88($date_togo);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_ipay88()
    {
        $this->db->from('trx_responseipay88');
        return $this->db->count_all_results();
    }
    // REPORT IPAY88


    // PAID
    var $col_order_paid = array('a.id', 'a.billing_id', 'b.createdate', 'd.destname', 'b.ticketdatefrom', 'a.total_pax', 'a.first_name', 'a.last_name', 'a.`status`');
    var $col_search_paid = array('a.id', 'a.billing_id', 'b.createdate', 'd.destname', 'b.ticketdatefrom', 'a.total_pax', 'a.first_name', 'a.last_name');
    var $order_paid = array('a.id' => 'asc');

    private function _sql_paid($option, $date_togo)
    {
        $this->db->select("a.id,
                            b.id as id_update,
                            a.billing_id,
                            a.total_pax,
                            a.first_name,
                            a.last_name,
                            a.`status` AS id_status,
                        IF
                            (
                                a.`status` = 1,
                                'UNPAID',
                            IF
                                (
                                    a.`status` = 2 && a.receipt IS NULL,
                                    'PAID IPAY88',
                                IF
                                    ( a.`status` = 2 && a.receipt IS NOT NULL, 'PAID (Manual)', 'CANCEL' ) 
                                )) AS `status`,
                            a.receipt,
	                        a.note,
                            max( b.ticketdatefrom ) AS ticketdatefrom,
                            max( b.createdate ) AS createdate,
                            d.destid,
                            max( d.destname ) AS destname");
        $this->db->from('trx_billing a');
        $this->db->join('trx_billing_dtl b', 'a.billing_id = b.billing_id', 'left');
        $this->db->join('stp_member c', 'a.buyerid = c.buyerid', 'left');
        $this->db->join('stp_dest d', 'b.destid = d.destid', 'left');
        if ($option == 'PM') {
            $this->db->where("a.`status` = '2' && a.receipt IS NOT NULL");
        }
        if ($option == 'PI') {
            $this->db->where("a.`status` = '2' && a.receipt IS NULL");
        }
        if ($date_togo !== '0') {
            $replace = explode('%20', $date_togo);
            $d1 = $replace[0];
            $d2 = $replace[2];
            $this->db->where("b.ticketdatefrom BETWEEN '$d1' AND '$d2'");
        }
        $i = 0;
        foreach ($this->col_search_paid as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->col_search_paid) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        $this->db->group_by(['a.id', 'a.billing_id', 'a.`status`']);

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->col_order_paid[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_paid)) {
            $order = $this->order_paid;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_paid($option, $date_togo)
    {
        $this->_sql_paid($option, $date_togo);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_paid($option, $date_togo)
    {
        $this->_sql_paid($option, $date_togo);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_paid()
    {
        $this->db->from('trx_billing');
        return $this->db->count_all_results();
    }
    // PAID

    // CLIMBER CHANGE
    var $col_order_change = array('a.id', 'b.billing_id', 'b.createdate', 'e.destname', 'a.date_before', 'a.date_togo', 'b.visitor_name', 'd.status');
    var $col_search_change = array('a.id', 'b.billing_id', 'b.createdate', 'e.destname', 'a.date_before', 'a.date_togo', 'b.visitor_name', 'd.status');
    var $order_change = array('a.id' => 'desc');

    private function _sql_change($date_togo)
    {
        $this->db->select("CAST( SUBSTRING_INDEX( a.note, '|',- 1 ) AS UNSIGNED ) AS id_tvisitor,
                                SUBSTRING_INDEX( b.visitor_name, ' (', 1 ) AS n_visname,
                                c.billing_id,
                                d.ticket_id,
                            IF
                                (
                                    d.`status` = 1,
                                    'OPEN',
                                IF
                                ( d.`status` = 2, 'CHECK IN', 'EXPIRED' )) AS `ticket_status`,
                                b.visitor_name,
                                c.createdate,
                                a.date_before,
                                a.date_togo,
                                e.destid,
	                            e.destname,
                                a.note,
                                a.user_update");
        $this->db->from('`tb_history` a');
        $this->db->join("trx_billing_visitor b", "CONCAT('#',a.billing_id) = b.billing_id", "left");
        $this->db->join('trx_billing_dtl c', 'b.billing_id = c.billing_id', 'left');
        $this->db->join('trx_ticket d', 'b.id_number = d.id_number 
        AND b.ticketdatefrom = DATE( d.ticketdatefrom )', 'left');
        $this->db->join('stp_dest e', 'c.destid = e.destid', 'left');
        $this->db->where('a.category ', 1);
        $this->db->like('a.note', 'new-change', 'after');
        $this->db->like('d.ticket_id', 'NT', 'after');
        if ($date_togo !== '0') {
            $replace = explode('%20', $date_togo);
            $d1 = $replace[0];
            $d2 = $replace[2];
            $this->db->where("a.date_togo BETWEEN '$d1' AND '$d2'");
        }

        $i = 0;
        foreach ($this->col_search_change as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->col_search_change) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        $this->db->group_by('b.id');

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->col_order_change[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_change)) {
            $order = $this->order_change;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_change($date_togo)
    {
        $this->_sql_change($date_togo);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_change($date_togo)
    {
        $this->_sql_change($date_togo);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_change()
    {
        $this->db->from('tb_history');
        $this->db->where('category ', 1);
        return $this->db->count_all_results();
    }
    // CLIMBER CHANGE


    // CANCEL BY PERSONAL
    var $col_order_cancel = array('a.id');
    var $col_search_cancel = array('a.id');
    var $order_cancel = array('a.id' => 'desc');

    private function _sql_cancel_ps($date_togo)
    {
        $this->db->select("b.billing_id,
                            b.destid,
                            c.destname,
                            b.ticketdatefrom AS date_togo,
                            b2.createdate AS date_create,
                            b.visitor_name,
                            SUBSTRING_INDEX( b.visitor_name, ' (', 1 ) AS new_name,
                            a.note,
                            b.`status`,
                            DATE_FORMAT( a.update_time, '%d %b %Y, %H:%i %p' ) AS upd_time,
                            d.nama_lengkap,
                            d.email");
        $this->db->from('`tb_history` a');
        $this->db->join("trx_billing_visitor b", "CONCAT( '#', a.billing_id ) = b.billing_id", "left");
        $this->db->join("( SELECT * FROM trx_billing_dtl GROUP BY billing_id ) b2", "b.billing_id = b2.billing_id", "left");
        $this->db->join('stp_dest c', 'b.destid = c.destid', 'left');
        $this->db->join('rb_users d', 'a.user_update = d.id_user', 'left');
        $this->db->where('a.category', 5);
        $this->db->where('b.`status`', 3);
        $this->db->like('b.visitor_name', 'cancel', 'both');
        if ($date_togo !== '0') {
            $replace = explode('%20', $date_togo);
            $d1 = $replace[0];
            $d2 = $replace[2];
            $this->db->where("b.ticketdatefrom BETWEEN '$d1' AND '$d2'");
        }

        $i = 0;
        foreach ($this->col_search_cancel as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->col_search_cancel) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        $this->db->group_by('b.id_number');

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->col_order_cancel[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_cancel)) {
            $order = $this->order_cancel;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_cancel($date_togo)
    {
        $this->_sql_cancel_ps($date_togo);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_cancel($date_togo)
    {
        $this->_sql_cancel_ps($date_togo);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_cancel()
    {
        $this->db->from('tb_history');
        $this->db->where('category ', 5);
        return $this->db->count_all_results();
    }
    // CANCEL BY PERSONAL
}
