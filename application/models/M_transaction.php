<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_transaction extends CI_Model
{
    // MEMBER
    var $column_order = array('a.id', 'a.first_name', 'a.last_name', 'a.country', 'a.email', 'a.phone');
    var $column_search = array('a.id', 'a.first_name', 'a.last_name', 'a.country', 'a.email', 'a.phone');
    var $order = array('a.first_name' => 'desc');

    private function _sql_member()
    {
        $this->db->select('a.*, b.country_name');
        $this->db->from('stp_member a');
        $this->db->join('stp_country b', 'b.id = a.country', 'left');
        $this->db->where('a.`status`', 1);
        $i = 0;
        foreach ($this->column_search as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_member()
    {
        $this->_sql_member();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_sql_member();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all()
    {
        $this->db->from('stp_member');
        return $this->db->count_all_results();
    }
    // MEMBER


    // ORDER/BOOKING
    var $col_order_odr = array('a.id', 'a.id', 'a.billing_id', 'b.createdate', 'd.destname', 'b.ticketdatefrom', 'a.total_pax', 'a.first_name', 'a.last_name', 'a.`status`');
    var $col_search_odr = array('a.id', 'a.billing_id', 'b.createdate', 'd.destname', 'b.ticketdatefrom', 'a.total_pax', 'a.first_name', 'a.last_name');
    var $order_odr = array('a.id' => 'asc');

    private function _sql_order($year, $date_togo)
    {
        $this->db->select("a.id,
                            b.id as id_update,
                            a.billing_id,
                            a.total_pax,
                            a.first_name,
                            a.last_name,
                            a.`status` AS id_status,
                        IF
                            (
                                a.`status` = 1,
                                'UNPAID',
                            IF
                                (
                                    a.`status` = 2 && a.receipt IS NULL,
                                    'PAID IPAY88',
                                IF
                                    ( a.`status` = 2 && a.receipt IS NOT NULL, 'PAID (Manual)', 'CANCEL' ) 
                                )) AS `status`,
                            a.receipt,
	                        a.note,
                            max( b.ticketdatefrom ) AS ticketdatefrom,
                            max( b.createdate ) AS createdate,
                            d.destid,
                            max( d.destname ) AS destname");
        $this->db->from('trx_billing a');
        $this->db->join('trx_billing_dtl b', 'a.billing_id = b.billing_id', 'left');
        $this->db->join('stp_member c', 'a.buyerid = c.buyerid', 'left');
        $this->db->join('stp_dest d', 'b.destid = d.destid', 'left');
        // $this->db->where('a.billing_id', '#ZIMBUKQX');
        if ($year !== '0' && $year !== 'all') {
            $this->db->where("YEAR(b.ticketdatefrom) = '$year'");
        }
        if ($date_togo !== '0') {
            $replace = explode('%20', $date_togo);
            if (in_array('to', $replace)) {
                $d1 = $replace[0];
                $d2 = $replace[2];
                $this->db->where("b.ticketdatefrom BETWEEN '$d1' AND '$d2'");
            } else {
                $d1 = $replace[0];
                $this->db->where("b.ticketdatefrom = '$d1'");
            }
        }
        $i = 0;
        foreach ($this->col_search_odr as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->col_search_odr) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        $this->db->group_by(['a.id', 'a.billing_id', 'a.`status`']);

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->col_order_odr[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_odr)) {
            $order = $this->order_odr;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_odr($year, $date_togo)
    {
        $this->_sql_order($year, $date_togo);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_odr($year, $date_togo)
    {
        $this->_sql_order($year, $date_togo);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_odr()
    {
        $this->db->from('trx_billing');
        return $this->db->count_all_results();
    }
    // ORDER/BOOKING


    // HOLDER-APPROVE
    var $col_order_happv = array('a.id', 'a.`status_approved`', 'a.billing_id', 'b.createdate', 'd.destname', 'b.ticketdatefrom', 'a.total_pax', 'a.first_name', 'a.last_name');
    var $col_search_happv = array('a.id', 'a.billing_id', 'a.total_pax', 'a.first_name', 'a.last_name', 'b.ticketdatefrom', 'b.createdate', 'd.destname');
    var $order_happv = array('b.ticketdatefrom' => 'desc', 'a.id' => 'desc');

    private function _sql_holder_appv($year, $date_togo)
    {
        $this->db->select("a.id,
                            a.billing_id,
                            a.total_pax,
                            a.first_name,
                            a.last_name,
                            a.status_approved,
                        IF
                            (
                                a.`status_approved` = 1,
                                '-',
                            IF
                            ( a.`status_approved` = 2, 'APPROVED', 'REJECTED' )) AS `ket_approve`,
                            a.`status`,
                            max( b.ticketdatefrom ) AS ticketdatefrom,
                            max( b.createdate ) AS createdate,
                            max( d.destname ) AS destname");
        $this->db->from('trx_billing a');
        $this->db->join('trx_billing_dtl b', 'a.billing_id = b.billing_id', 'left');
        $this->db->join('stp_member c', 'a.buyerid = c.buyerid', 'left');
        $this->db->join('stp_dest d', 'b.destid = d.destid', 'left');
        $this->db->where('a.pr_holder', 1);
        if ($year !== '0' && $year !== 'all') {
            $this->db->where("YEAR(b.ticketdatefrom) = '$year'");
        }
        if ($date_togo !== '0') {
            $replace = explode('%20', $date_togo);
            if (in_array('to', $replace)) {
                $d1 = $replace[0];
                $d2 = $replace[2];
                $this->db->where("b.ticketdatefrom BETWEEN '$d1' AND '$d2'");
            } else {
                $d1 = $replace[0];
                $this->db->where("b.ticketdatefrom = '$d1'");
            }
        }
        $i = 0;
        foreach ($this->col_search_happv as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->col_search_happv) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        $this->db->group_by(['a.id', 'a.billing_id', 'a.`status`']);

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->col_order_happv[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_happv)) {
            $order = $this->order_happv;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_happv($year, $date_togo)
    {
        $this->_sql_holder_appv($year, $date_togo);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_happv($year, $date_togo)
    {
        $this->_sql_holder_appv($year, $date_togo);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_happv()
    {
        $this->db->from('trx_billing');
        $this->db->where('pr_holder', 1);
        return $this->db->count_all_results();
    }
    // HOLDER-APPROVE

    // BILLING
    var $col_order_bill = array('a.id', 'a.id', 'a.billing_id', 'b.createdate', 'a.first_name', 'a.last_name', '(b.loc_qty_18above+b.loc_qty_18below)+(b.int_qty_18above+b.int_qty_18below)', 'a.country', 'a.address', 'a.email', 'a.phone', 'd.destname', 'b.ticketdatefrom', 'c.receipt_id', 'c.createdate');
    var $col_search_bill = array('a.id', 'a.billing_id', 'b.createdate', 'a.first_name', 'a.last_name', 'a.country', 'a.address', 'a.email', 'a.phone', 'c.receipt_id', 'c.createdate', 'b.ticketdatefrom');
    var $order_bill = array('a.id' => 'asc');

    private function _sql_holder_bill($year, $date_togo, $dest, $status)
    {
        $this->db->select("a.id,
                            b.id as id_update,
                            (b.loc_qty_18above+b.loc_qty_18below) as loc_booked,
	                        (b.int_qty_18above+b.int_qty_18below) as int_booked,
	                        ((b.loc_qty_18above+b.loc_qty_18below)+(b.int_qty_18above+b.int_qty_18below)) as tot_booked,
                            a.billing_id,
                            max( b.createdate ) AS createdate,
                            max( a.first_name ) AS first_name,
                            max( a.last_name ) AS last_name,
                            max( a.country ) AS country,
                            max( a.address ) AS address,
                            max( a.email ) AS email,
                            max( a.phone ) AS phone,
                            a.`status`,
                            a.note,
                            a.note_cancel,
                            a.note_lock_quota,
                        IF
                            (
                                a.`status` = 1,
                                'UNPAID',
                            IF
                                (
                                    a.`status` = 2 && a.receipt IS NULL,
                                    'PAID IPAY88',
                                IF
                                    ( a.`status` = 2 && a.receipt IS NOT NULL, 'PAID (Manual)', 'CANCEL' ) 
                                )) AS `ket_status`,
                            max( d.destname ) AS destname,
                            d.destid,
                            c.receipt_id,
                            max( c.createdate ) AS pay_date,
                            max( b.ticketdatefrom ) AS ticketdatefrom");
        $this->db->from('trx_billing a');
        $this->db->join('trx_billing_dtl b', 'a.billing_id = b.billing_id AND a.buyerid = b.buyerid', 'left');
        $this->db->join('trx_receipt c', 'a.billing_id = c.billing_id', 'left');
        $this->db->join('stp_dest d', 'b.destid = d.destid', 'left');
        // $this->db->where('a.billing_id', '#KHOUB23A');
        if ($year !== '0' && $year !== 'all') {
            $this->db->where("YEAR(b.ticketdatefrom) = '$year'");
        }
        if ($date_togo !== '0') {
            $replace = explode('%20', $date_togo);
            if (in_array('to', $replace)) {
                $d1 = $replace[0];
                $d2 = $replace[2];
                $this->db->where("b.ticketdatefrom BETWEEN '$d1' AND '$d2'");
            } else {
                $d1 = $replace[0];
                $this->db->where("b.ticketdatefrom = '$d1'");
            }
        }
        if ($dest !== '0') {
            $this->db->where("b.destid", $dest);
        }
        if ($status !== '0') {
            $this->db->where("a.status", $status);
        }
        $i = 0;
        foreach ($this->col_search_bill as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->col_search_bill) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        $this->db->group_by(['a.id', 'a.billing_id', 'a.`status`', 'c.receipt_id']);

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->col_order_bill[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_bill)) {
            $order = $this->order_bill;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_bill($year, $date_togo, $dest, $status)
    {
        $this->_sql_holder_bill($year, $date_togo, $dest, $status);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_bill($year, $date_togo, $dest, $status)
    {
        $this->_sql_holder_bill($year, $date_togo, $dest, $status);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_bill()
    {
        $this->db->from('trx_billing');
        return $this->db->count_all_results();
    }
    // BILLING


    // TICKET
    var $col_order_tkt = array('a.id', 'a.id', 'a.ticketdatefrom', 'a.ticket_id', 'a.billing_id', 'b.destname', 'a.visitor_name', 'a.phone', 'a.email', 'a.nationality', 'c.pr_holder', 'a.`status`', 'a.ticketdatefrom');
    var $col_search_tkt = array('a.id', 'a.ticketdatefrom', 'a.ticket_id', 'a.billing_id', 'b.destname', 'a.visitor_name', 'a.phone', 'a.email', 'a.nationality', 'c.pr_holder', 'a.`status`', 'a.ticketdatefrom');
    var $order_tkt = array('a.ticket_id' => 'asc');

    private function _sql_tkt($year, $date_togo)
    {
        $this->db->select("a.billing_id,
                            a.ticket_id,
                            DATE(a.ticketdatefrom) AS ticketdatefrom,
                            a.`status` AS id_status,
                        IF
                            (
                                a.`status` = 1,
                                'OPEN',
                            IF
                            ( a.`status` = 2, 'CHECK IN', 'EXPIRED' )) AS `status`,
                        IF
                            ( a.billing_id IS NOT NULL, a.billing_id, '' ) AS billing_id,
                        IF
                            ( a.visitor_name IS NOT NULL, a.visitor_name, '' ) AS visitor_name,
                        IF
                            ( a.phone IS NOT NULL, a.phone, '' ) AS phone,
                        IF
                            ( a.email IS NOT NULL, a.email, '' ) AS email,
                        IF
                            ( a.nationality IS NOT NULL, a.nationality, '' ) AS nationality,
                        IF
                            ( b.destname IS NOT NULL, b.destname, '' ) AS destname,
                        IF
                            ( c.country IS NOT NULL, c.country, '' ) AS country,
                        IF
                            ( c.pr_holder IS NOT NULL, c.pr_holder, '' ) AS pr_holder,
                        IF
                            ( a.ticketdatefrom IS NOT NULL, DATE(a.ticketdatefrom), '' ) AS date_togo");
        $this->db->from('trx_ticket a');
        $this->db->join('stp_dest b', 'b.destid = a.destid', 'left');
        $this->db->join('trx_billing c', 'c.billing_id = a.billing_id', 'left');
        $this->db->join('trx_billing_dtl d', 'd.billing_id = a.billing_id', 'left');
        if ($year !== '0' && $year !== 'all') {
            $this->db->where("YEAR(a.ticketdatefrom) = '$year'");
        }
        if ($date_togo !== '0') {
            $replace = explode('%20', $date_togo);
            if (in_array('to', $replace)) {
                $d1 = $replace[0];
                $d2 = $replace[2];
                $this->db->where("a.ticketdatefrom BETWEEN '$d1' AND '$d2'");
            } else {
                $d1 = $replace[0];
                $this->db->where("a.ticketdatefrom = '$d1'");
            }
        }
        $this->db->distinct();
        $i = 0;
        foreach ($this->col_search_tkt as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->col_search_tkt) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->col_order_tkt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_tkt)) {
            $order = $this->order_tkt;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_tkt($year, $date_togo)
    {
        $this->_sql_tkt($year, $date_togo);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_tkt($year, $date_togo)
    {
        $this->_sql_tkt($year, $date_togo);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_tkt()
    {
        $this->db->from('trx_ticket');
        return $this->db->count_all_results();
    }
    // TICKET


    // VISITOR
    var $col_order_vst = array('a.id', 'a.id', 'a.visitor_name', 'a.phone', 'a.email', 'a.nationality', 'b.pr_holder', 'COUNT( a.visitor_name )');
    var $col_search_vst = array('a.id', 'a.billing_id', 'a.visitor_name', 'a.phone', 'a.phone', 'a.email', 'a.nationality', 'b.pr_holder');
    var $order_vst = array('a.id' => 'desc');

    private function _sql_visitor($params)
    {
        $this->db->select("a.billing_id,
                            a.ticketdateto,
                            a.visitor_name,
                            a.phone,
                            a.email,
                            a.nationality,
                            a.id,
                            a.`status`,
                            b.pr_holder,
                            COUNT( a.visitor_name ) AS total_attend");
        $this->db->from('trx_billing_visitor a');
        $this->db->join('trx_billing b', 'a.billing_id = b.billing_id', 'left');
        $this->db->join('trx_ticket c', 'a.billing_id = c.billing_id 
        AND a.visitor_name = c.visitor_name', 'left');
        $this->db->where('c.`status`', 2);
        $this->db->where('a.buyerid !=', 'lock');
        if ($params !== '0' && $params !== 'all') {
            $this->db->where("YEAR ( a.ticketdateto ) = '$params'");
        }

        $i = 0;
        foreach ($this->col_search_vst as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->col_search_vst) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        $this->db->group_by(['a.visitor_name', 'a.phone', 'a.email', 'a.nationality', 'b.pr_holder', 'a.`status`', 'a.id']);

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->col_order_vst[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_vst)) {
            $order = $this->order_vst;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_vst($params)
    {
        $this->_sql_visitor($params);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_vst($params)
    {
        $this->_sql_visitor($params);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_vst()
    {
        $this->db->from('trx_billing_visitor');
        return $this->db->count_all_results();
    }
    // VISITOR


    // LOCK-QUOTA
    var $col_order_lock = array('a.id', 'a.visitor_name', 'a.phone', 'a.phone', 'a.email', 'a.nationality', 'b.pr_holder');
    var $col_search_lock = array('a.id', 'a.visitor_name', 'a.phone', 'a.phone', 'a.email', 'a.nationality', 'b.pr_holder');
    var $order_lock = array('a.id' => 'desc');

    private function _sql_lock($params)
    {
        $this->db->select("a.buyerid,
                            a.ticketdateto,
                            a.visitor_name,
                            a.phone,
                            a.email,
                            a.nationality,
                            a.id,
                            a.`status`,
                            b.pr_holder,
                            COUNT( a.visitor_name ) AS total_attend");
        $this->db->from('trx_billing_visitor a');
        $this->db->join('trx_billing b', 'a.billing_id = b.billing_id', 'left');
        $this->db->join('trx_ticket c', 'a.billing_id = c.billing_id 
        AND a.visitor_name = c.visitor_name', 'left');
        $this->db->where('c.`status`', 2);
        $this->db->where('a.buyerid', 'lock');
        $this->db->like('a.visitor_name', 'Sabah Parks', 'after');
        if ($params !== '0' && $params !== 'all') {
            $this->db->where("YEAR ( a.ticketdateto ) = '$params'");
        }

        $i = 0;
        foreach ($this->col_search_lock as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->col_search_lock) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        $this->db->group_by(['a.visitor_name', 'a.phone', 'a.email', 'a.nationality', 'b.pr_holder', 'a.`status`', 'a.id']);

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->col_order_lock[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_lock)) {
            $order = $this->order_lock;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_lock($params)
    {
        $this->_sql_lock($params);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_lock($params)
    {
        $this->_sql_lock($params);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_lock()
    {
        $this->db->from('trx_billing_visitor');
        return $this->db->count_all_results();
    }
    // LOCK-QUOTA


    // CHECK IN
    var $col_order_chk = array('a.id', 'a.ticketdatefrom', 'b.destname', 'a.visitor_name', 'a.id_number', 'a.phone', 'a.email', 'a.nationality', 'c.pr_holder');
    var $col_search_chk = array('a.id', 'a.ticketdatefrom', 'b.destname', 'a.visitor_name', 'a.id_number', 'a.phone', 'a.email', 'a.nationality');
    var $order_chk = array('a.id' => 'asc');

    private function _sql_checkin($ticketid)
    {
        $this->db->select("a.*,
                            b.destname,
                            c.country,
                            c.pr_holder,
                            DATE( a.ticketdatefrom ) AS date_togo,
                        IF
	                        ( c.pr_holder = 1, 'Y', 'N' ) pr_status");
        $this->db->from('trx_ticket a');
        $this->db->join('stp_dest b', 'b.destid = a.destid', 'left');
        $this->db->join('trx_billing c', 'c.billing_id = a.billing_id', 'left');
        $this->db->join('trx_billing_dtl d', 'd.billing_id = a.billing_id', 'left');
        // $this->db->where("d.ticketdatefrom", date('Y-m-d'));
        if ($ticketid !== '0') {
            $this->db->where("a.ticket_id", $ticketid);
        }
        $i = 0;
        foreach ($this->col_search_chk as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->col_search_chk) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->col_order_chk[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_chk)) {
            $order = $this->order_chk;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_chk($ticketid)
    {
        $this->_sql_checkin($ticketid);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_chk($ticketid)
    {
        $this->_sql_checkin($ticketid);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_chk()
    {
        $this->db->from('trx_ticket');
        return $this->db->count_all_results();
    }
    // CHECK IN


    // RE-QUERY IPAY
    var $col_order_rqr = array('a.id', 'a.id', 'a.billing_id', 'b.createdate', 'a.first_name', 'a.last_name', 'a.country', 'a.address', 'a.email', 'a.phone', 'd.destname', 'b.ticketdatefrom', 'a.status');
    var $col_search_rqr = array('a.id', 'a.billing_id', 'b.createdate', 'a.first_name', 'a.last_name', 'a.country', 'a.address', 'a.email', 'a.phone', 'd.destname', 'b.ticketdatefrom', 'a.status');
    var $order_rqr = array('b.createdate' => 'DESC');

    private function _sql_requery($date_togo)
    {
        $this->db->select("a.id,
                            a.billing_id,
                            max( b.createdate ) AS createdate,
                            max( a.first_name ) AS first_name,
                            max( a.last_name ) AS last_name,
                            max( a.country ) AS country,
                            max( a.address ) AS address,
                            max( a.email ) AS email,
                            max( a.phone ) AS phone,
                            a.`status`,
                        IF
                            (
                                a.`status` = 1,
                                'UNPAID',
                            IF
                            ( a.`status` = 2, 'PAID', 'EXPIRED' )) AS ket_status,               
                            max( d.destname ) AS destname,
                            c.receipt_id,
                            max( c.createdate ) AS pay_date,
                            max( b.ticketdatefrom ) AS ticketdatefrom,
                            a.total_cost");
        $this->db->from('trx_billing a');
        $this->db->join('trx_billing_dtl b', 'a.billing_id = b.billing_id AND a.buyerid = b.buyerid', 'left');
        $this->db->join('trx_receipt c', 'a.billing_id = c.billing_id', 'left');
        $this->db->join('stp_dest d', 'b.destid = d.destid', 'left');
        $this->db->where('a.`status` !=', 2);
        $this->db->where('a.status_approved', 2);
        if ($date_togo !== '0') {
            $replace = explode('%20', $date_togo);
            if (in_array('to', $replace)) {
                $d1 = $replace[0];
                $d2 = $replace[2];
                $this->db->where("b.ticketdatefrom BETWEEN '$d1' AND '$d2'");
            } else {
                $d1 = $replace[0];
                $this->db->where("b.ticketdatefrom = '$d1'");
            }
        }
        $i = 0;
        foreach ($this->col_search_rqr as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->col_search_rqr) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        $this->db->group_by(['a.id', 'a.billing_id', 'a.`status`', 'c.receipt_id', 'a.total_cost']);

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->col_order_rqr[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_rqr)) {
            $order = $this->order_rqr;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_rqr($date_togo)
    {
        $this->_sql_requery($date_togo);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_rqr($date_togo)
    {
        $this->_sql_requery($date_togo);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_rqr()
    {
        $this->db->from('trx_billing');
        $this->db->where('`status` !=', 2);
        $this->db->where('status_approved', 2);
        return $this->db->count_all_results();
    }
    // RE-QUERY IPAY


    function detailOrder_byId($id)
    {
        return $this->db->query("SELECT
                            a.id,
                            a.billing_id,
                            a.total_pax,
                            a.first_name,
                            a.last_name,
                            a.`status` AS id_status,
                        IF
                            (
                                a.`status` = 1,
                                'UNPAID',
                            IF
                            ( a.`status` = 2, 'PAID', 'CANCEL' )) AS `status`,
                            max( b.ticketdatefrom ) AS ticketdatefrom,
                            max( b.createdate ) AS createdate,
                            max( d.destname ) AS destname,
                            a.receipt,
                            a.note,
                            a.note_cancel
                        FROM
                            trx_billing a
                            LEFT JOIN trx_billing_dtl b ON a.billing_id = b.billing_id
                            LEFT JOIN stp_member c ON a.buyerid = c.buyerid
                            LEFT JOIN stp_dest d ON b.destid = d.destid 
                        WHERE
                            a.id = '$id' 
                        GROUP BY
                            a.id,
                            a.billing_id,
                            a.`status`
                        ");
    }

    function detailBilling_byId($id)
    {
        return $this->db->query("SELECT
                                a.id,
                                a.billing_id,
                                max( b.createdate ) AS createdate,
                                max( a.first_name ) AS first_name,
                                max( a.last_name ) AS last_name,
                                max( a.country ) AS country,
                                max( a.address ) AS address,
                                max( a.email ) AS email,
                                max( a.phone ) AS phone,
                                a.`status`,
                            IF
                            (
                                a.`status` = 1,
                                'UNPAID',
                            IF
                                (
                                    a.`status` = 2 && a.receipt IS NULL,
                                    'PAID IPAY88',
                                IF
                                    ( a.`status` = 2 && a.receipt IS NOT NULL, 'PAID (Manual)', 'CANCEL' ) 
                                )) AS `ket_status`,
                                d.destid,
                                max( d.destname ) AS destname,
                                a.receipt,
                                a.note,
                                a.note_cancel,
                                a.note_lock_quota,
                                -- c.receipt_id,
                                -- max( c.createdate ) AS pay_date,
                                IF ( c.receipt_id IS NULL || c.receipt_id = '', '-', c.receipt_id ) AS receipt_id,
                                IF(max( c.createdate ) IS NULL || max( c.createdate) = '','-',max( c.createdate )) AS pay_date,
                                max( b.ticketdatefrom ) AS ticketdatefrom 
                            FROM
                                trx_billing a
                                LEFT JOIN trx_billing_dtl b ON a.billing_id = b.billing_id 
                                AND a.buyerid = b.buyerid
                                LEFT JOIN trx_receipt c ON a.billing_id = c.billing_id
                                LEFT JOIN stp_dest d ON b.destid = d.destid 
                            WHERE
                                a.id = '$id'
                            GROUP BY
                                a.id,
                                a.billing_id,
                                a.`status`,
                                c.receipt_id
                            ");
    }

    function detailVisitor_byId($id)
    {
        return $this->db->query("SELECT
                            `a`.`ticketdateto`,
                            `a`.`visitor_name`,
                            `a`.`phone`,
                            `a`.`email`,
                            `a`.`nationality`,
                            `a`.`id`,
                            `a`.`status`,
                            `b`.`pr_holder`,
                            COUNT( a.visitor_name ) AS total_attend 
                        FROM
                            `trx_billing_visitor` `a`
                            LEFT JOIN `trx_billing` `b` ON `a`.`billing_id` = `b`.`billing_id`
                            LEFT JOIN `trx_ticket` `c` ON `a`.`billing_id` = `c`.`billing_id` 
                            AND `a`.`visitor_name` = `c`.`visitor_name` 
                        WHERE
                            a.id = '$id' 
                        GROUP BY
                            `a`.`visitor_name`,
                            `a`.`phone`,
                            `a`.`email`,
                            `a`.`nationality`,
                            `b`.`pr_holder`,
                            `a`.`status`,
                            `a`.`id`");
    }

    function getDetailOrder($id)
    {
        return $this->db->query("SELECT
                                    a.id,
                                    b.id as id_update,
                                    a.billing_id,
                                    a.total_pax,
                                    a.first_name,
                                    a.last_name,
                                    a.`status` AS id_status,
                                IF
                                    (
                                        a.`status` = 1,
                                        'UNPAID',
                                    IF
                                    ( a.`status` = 2, 'PAID', 'CANCEL' )) AS `status`,
                                    max( b.ticketdatefrom ) AS ticketdatefrom,
                                    max( b.createdate ) AS createdate,
                                    max( d.destname ) AS destname 
                                FROM
                                    trx_billing a
                                    LEFT JOIN trx_billing_dtl b ON a.billing_id = b.billing_id
                                    LEFT JOIN stp_member c ON a.buyerid = c.buyerid
                                    LEFT JOIN stp_dest d ON b.destid = d.destid 
                                WHERE b.id = '$id'
                                GROUP BY
                                    a.id,
                                    a.billing_id,
                                    a.`status`");
    }

    function get_history_changedate($bill_id)
    {
        return $this->db->query("SELECT
                                    a.*,
                                    b.category AS name_cat,
                                    DATE_FORMAT( DATE( a.update_time ), '%d %b %Y, ' ) AS tahun,
                                    DATE_FORMAT( TIME( a.update_time ), '%H:%i %p' ) AS time,
                                    c.nama_lengkap 
                                FROM
                                    `tb_history` a
                                    LEFT JOIN tb_cat_history b ON a.category = b.id
                                    LEFT JOIN rb_users c ON a.user_update = c.id_user 
                                WHERE
                                    a.billing_id = '$bill_id' 
                                    AND a.category = 1
                                ORDER BY
                                    a.id ASC
                                ");
    }

    function get_visitor_byId($id)
    {
        return $this->db->query("SELECT
                                a.buyerid,
                                a.billing_id,
                                a.visitor_name,
                                a.phone,
                                a.email,
                                a.nationality,
                                a.id,
                                a.`status`,
                                c.`status` AS sts_ticket,
                                b.pr_holder,
                                COUNT( a.visitor_name ) AS total_attend 
                            FROM
                                trx_billing_visitor a
                                LEFT JOIN trx_billing b ON a.billing_id = b.billing_id
                                LEFT JOIN trx_ticket c ON a.billing_id = c.billing_id 
                                AND a.visitor_name = c.visitor_name 
                            WHERE
                                a.id = '$id'");
    }

    function get_billing_byId($id)
    {
        return $this->db->query("SELECT
                                a.id,
                                b.id AS id_update,
                                a.billing_id,
                                max( b.createdate ) AS createdate,
                                max( a.first_name ) AS first_name,
                                max( a.last_name ) AS last_name,
                                max( a.country ) AS country,
                                max( a.address ) AS address,
                                max( a.email ) AS email,
                                max( a.phone ) AS phone,
                                a.`status`,
                                a.note,
                                a.note_cancel,
                            IF
                                (
                                    a.`status` = 1,
                                    'UNPAID',
                                IF
                                    (
                                        a.`status` = 2 && a.receipt IS NULL,
                                        'PAID IPAY88',
                                    IF
                                        ( a.`status` = 2 && a.receipt IS NOT NULL, 'PAID (Manual)', 'CANCEL' ) 
                                    )) AS `ket_status`,
                                max( d.destname ) AS destname,
                                d.destid,
                                c.receipt_id,
                                max( c.createdate ) AS pay_date,
                                max( b.ticketdatefrom ) AS ticketdatefrom 
                            FROM
                                trx_billing a
                                LEFT JOIN trx_billing_dtl b ON a.billing_id = b.billing_id 
                                AND a.buyerid = b.buyerid
                                LEFT JOIN trx_receipt c ON a.billing_id = c.billing_id
                                LEFT JOIN stp_dest d ON b.destid = d.destid 
                            WHERE
                                a.id = '$id'");
    }

    function get_member()
    {
        return $this->db->query("SELECT
                                    a.email,
                                    max( a.visitor_name ) AS visitor_name 
                                FROM
                                    trx_ticket a 
                                WHERE
                                    a.email IN ( 'atokkecenk@gmail.com', 'sobatcoding@gmail.com' ) 
                                GROUP BY
                                    a.id 
                                    LIMIT 2");
    }
}
