<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'Home';
$route['dashboard'] = 'Home/index';

$route['datatables'] = 'Home/datatables';

// AUTH
$route['auth'] = 'Auth/index';
$route['logout'] = 'Auth/logout';
$route['cek-auth'] = 'Auth/cek_login';

// DASHBORAD
$route['test'] = 'Home/visitor_chart';

// TRANSACTION
// member
$route['member'] = 'Transaction/vw_member';
$route['get-datamember'] = 'Transaction/all_member';
$route['get-datamember-json'] = 'Transaction/get_member_json';
$route['detail-member/(:num)'] = 'Transaction/detailMember_byId/$1';
// order
$route['order'] = 'Transaction/vw_booking';
$route['get-dataorder/(.+)/(.+)'] = 'Transaction/all_order/$1/$2';
$route['detail-order/(:num)'] = 'Transaction/detailOrder_byId/$1';
$route['change-date-togo'] = 'Transaction/chg_date_togo';
$route['get-hist-changedate/(.+)'] = 'Transaction/get_history_changedate/$1';
$route['upload-receipt'] = 'Transaction/upload_receipt';
$route['cancel-booking'] = 'Transaction/cancel_booking';

$route['get-climber/(.+)'] = 'Transaction/get_climber_by_billingid/$1';

// pr-holder-approve
$route['booking'] = 'Transaction/vw_holder_appv';
$route['get-dataholder-appv/(.+)/(.+)'] = 'Transaction/all_holder_approve/$1/$2';
$route['holder-approve'] = 'Transaction/holder_approve';
// billing
$route['billing'] = 'Transaction/vw_billing';
$route['get-databilling/(.+)/(.+)'] = 'Transaction/all_billing/$1/$2/$3/$4';
$route['detail-billing/(:num)'] = 'Transaction/detailBilling_byId/$1';
$route['edit-billing/(.+)'] = 'Transaction/get_billing_byId/$1';
$route['save-edit-biling'] = 'Transaction/save_edit_billing';
$route['cek-billing'] = 'Transaction/cek_billing';
// ticket
$route['ticket'] = 'Transaction/vw_ticket';
$route['get-dataticket/(.+)/(.+)'] = 'Transaction/all_ticket/$1/$2';
// check in
$route['scan'] = 'Transaction/vw_checkin';
$route['get-datacheckin/(.+)'] = 'Transaction/scan_checkin/$1';
$route['update-ticket'] = 'Transaction/update_ticket';
// check in
$route['r-query'] = 'Transaction/vw_requery';
$route['get-data-requery/(.+)'] = 'Transaction/requery/$1';

// paid manual
$route['paid-manual'] = 'Transaction/vw_paid_manual';

// visitor
$route['visitor'] = 'Transaction/vw_visitor';
$route['get-datavisitor/(.+)'] = 'Transaction/all_visitor/$1';
$route['detail-visitor/(:num)'] = 'Transaction/detailVisitor_byId/$1';
$route['visitor-cancel'] = 'Transaction/visitor_cancel';
$route['edit-visitor/(.+)'] = 'Transaction/get_visitor_byId/$1';
$route['save-edit-visitor'] = 'Transaction/save_edit_visitor';

// news,blogs
$route['news'] = 'Transaction/vw_news';
$route['delete-news'] = 'Transaction/delete_news';
$route['upload-news'] = 'Transaction/upload_news';
$route['save-edit-news'] = 'Transaction/save_edit_news';
$route['edit-news/(:num)'] = 'Transaction/edit_news/$1';
$route['send-inbox'] = 'Transaction/send_inbox';

// broadcast
$route['broadcast'] = 'Transaction/vw_broadcast';
$route['delete-broadcast'] = 'Transaction/delete_broadcast';
$route['upload-broadcast'] = 'Transaction/upload_broadcast';
$route['edit-broadcast/(:num)'] = 'Transaction/edit_broadcast/$1';
$route['save-edit-broadcast'] = 'Transaction/save_edit_broadcast';

// lock-quota
$route['l-quota'] = 'Transaction/vw_lock';
$route['get-lock-quota/(.+)'] = 'Transaction/all_lock_quota/$1';
$route['save-lock-quota'] = 'Transaction/save_lock_quota';

// send document manual
$route['send-doc'] = 'Transaction/vw_send_doc';
$route['insert-ticket'] = 'Transaction/insert_ticket';

// SETUP
$route['destination'] = 'Setup/vw_destination';
$route['lock-quota'] = 'Setup/lock_quota';
$route['edit-destination/(.+)'] = 'Setup/edit_destination/$1';
$route['save-edit-dest'] = 'Setup/save_edit_destination';
$route['gallery'] = 'Setup/vw_gallery';
$route['upload-gallery'] = 'Setup/upload_gallery';
$route['edit-gallery/(:num)'] = 'Setup/edit_gallery/$1';
$route['delete-gallery/(:num)'] = 'Setup/delete_gallery/$1';
$route['hitung-avb/(.+)/(.+)'] = 'Setup/hitung_avb/$1/$2';
$route['hitung-quota-dest/(.+)'] = 'Setup/hitung_total_destbyId/$1';

// USER
$route['user'] = 'Setup/vw_user';
$route['save-user'] = 'Setup/save_user';
$route['edit-user/(:num)'] = 'Setup/edit_user/$1';
$route['delete-user/(:num)'] = 'Setup/delete_user/$1';
$route['update-profile'] = 'Setup/update_profile';

// USER ROLE
$route['user-role'] = 'Setup/vw_user_role';
$route['data-user-role/(:num)'] = 'Setup/set_user_role/$1';
$route['data-user'] = 'Setup/data_user_role';
$route['edit-user-role/(:num)'] = 'Setup/edit_user_role/$1';
$route['cek-active-menu'] = 'Setup/cek_active_menu';
$route['save-menu'] = 'Setup/save_menu';
$route['delete-role/(:num)'] = 'Setup/delete_user_role/$1';
$route['save-user-role'] = 'Setup/save_user_role';

// PROFILE
$route['profile'] = 'Setup/vw_profile';

// SEND EMAIL
$route['send-email'] = 'SendEmail/send';

// REPORT
// excel
// $route['trial-excel'] = 'Excel/trial';
$route['excel-member'] = 'Excel/report_member';
$route['excel-order/(.+)'] = 'Excel/report_order/$1';
$route['excel-booking/(.+)'] = 'Excel/report_pr_holder/$1';
$route['excel-billing/(.+)'] = 'Excel/report_billing/$1';
$route['excel-sales/(.+)'] = 'Excel/report_sales/$1';
$route['excel-ipay/(.+)'] = 'Excel/report_ipay/$1';
$route['excel-paid/(.+)/(.+)'] = 'Excel/report_paid/$1/$2';
$route['excel-cilmberchange/(.+)'] = 'Excel/report_climbchange/$1';
$route['excel-cancelps/(.+)'] = 'Excel/report_cancel_ps/$1';
# ---------------------------------------------------------- #
$route['r-member'] = 'Report/rp_member';
$route['rpt-data-member'] = 'Report/all_member';
$route['r-order'] = 'Report/rp_order';
$route['rpt-data-booking/(.+)'] = 'Report/all_order/$1';
$route['r-booking'] = 'Report/rp_booking_holder';
$route['rpt-data-pr-holder/(.+)'] = 'Report/all_holder_approve/$1';
$route['r-billing'] = 'Report/rp_billing';
$route['rpt-data-billing/(.+)'] = 'Report/all_billing/$1';
$route['r-sales'] = 'Report/rp_sales';
$route['rpt-data-sales/(.+)'] = 'Report/all_sales/$1';
$route['r-ipay'] = 'Report/rp_ipay';
$route['rpt-data-ipay/(.+)'] = 'Report/all_ipay/$1';
$route['r-paid'] = 'Report/rp_paid';
$route['rpt-data-paid/(.+)/(.+)'] = 'Report/all_paid/$1/$2';
$route['r-climbchange'] = 'Report/rp_climb_change';
$route['rpt-data-climbchange/(.+)'] = 'Report/all_climber/$1';
$route['r-cancel-climber'] = 'Report/rp_cancel_climber';
$route['rpt-data-cancelclimb/(.+)'] = 'Report/all_cancel_ps/$1';

// tambahan
$route['get-climber-by-bill/(.+)'] = 'Transaction/get_visitor_by_billingid/$1';

// pdf
// $route['trial-pdf'] = 'Pdf/trial';

$route['404_override'] = '_404';
$route['translate_uri_dashes'] = FALSE;
