<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_system', 'mSys');
        if (!$this->session->userdata('authenticated') && !$this->session->userdata('sabahparks'))
            redirect('auth');
    }

    public function renderPage($content, $data = NULL)
    {
        $uri1 = $this->uri->segment(1);
        $data['uri1'] = $uri1;
        $data['history'] = $this->mSys->getHistory('20', '')->result();
        if (!empty($uri1) && $uri1 !== 'dashboard' && $uri1 !== 'profile') {
            $get = $this->db->get_where('stp_menu', ['url' => $uri1])->row();
            $data['id_menu'] = $get->id;
            $data['id_root'] = $get->id_root;
        } elseif ($uri1 == 'profile') {
            $data['id_menu'] = 2;
            $data['id_root'] = 14;
        } else {
            $data['id_menu'] = 0;
            $data['id_root'] = 0;
        }
        $iduser = $this->session->id_user;
        $data['dataUser'] = $this->db->query("SELECT
                                                b.role_name,
                                                a.* 
                                            FROM
                                                rb_users a
                                                LEFT JOIN rb_users_role b ON a.id_role = b.id 
                                            WHERE
                                                a.id_user = '$iduser'")->row();

        $data['header'] = $this->load->view('template/header', $data, TRUE);
        $data['content'] = $this->load->view($content, $data, TRUE);
        $data['footer'] = $this->load->view('template/footer', $data, TRUE);

        $this->load->view('index', $data);
    }
}
