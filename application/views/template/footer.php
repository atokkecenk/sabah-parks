</div>
</div>
</div>

<div class="footer-wrapper">
    <div class="footer-section f-section-1">
        <!-- <p class="">Copyright © 2020 <a target="_blank" href="https://designreset.com">DesignReset</a>, All rights reserved.</p> -->
    </div>
    <div class="footer-section f-section-2">
        <p class="">&copy; Sabah Parks - Back Office <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart">
                <path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path>
            </svg> V2.0.3</p>
    </div>
</div>
<!-- <a onclick="openChat()" role="button" class="float">
    <i class="fas fa-comment fa-2x my-float"></i>
</a> -->
<!-- <div class="btn-chat" id="livechat-compact-container" style="visibility: visible; opacity: 1;">
    <div class="btn-holder">
        <a onclick="openChat()" role="button" class="link">Live Chat</a>
    </div>
</div> -->
</div>
<!--  END CONTENT AREA  -->

</div>
<!-- END MAIN CONTAINER -->

<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="assets/themes/assets/js/libs/jquery-3.1.1.min.js"></script>
<script src="assets/themes/bootstrap/js/popper.min.js"></script>
<script src="assets/themes/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/themes/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="assets/themes/assets/js/app.js"></script>
<script>
    $(document).ready(function() {
        App.init();
    });
</script>
<script src="assets/themes/assets/js/custom.js"></script>
<!-- END GLOBAL MANDATORY SCRIPTS -->

<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
<script src="assets/themes/plugins/apex/apexcharts.min.js"></script>
<!-- <script src="assets/themes/assets/js/dashboard/dash_1.js"></script> -->
<script src="assets/themes/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="assets/themes/plugins/flatpickr/flatpickr.js"></script>
<!-- <script src="assets/themes/plugins/flatpickr/custom-flatpickr.js"></script> -->
<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->

<!-- ADDED -->
<script src="assets/fontawesome-free-5.15.4/js/all.min.js"></script>
<script src="assets/fontawesome-free-5.15.4/js/fontawesome.min.js"></script>
<script src="assets/my-file/my-js.js"></script>
<script src="assets/plugin/summernote-0.8.18/summernote.min.js"></script>


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/themes/plugins/table/datatable/datatables.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>