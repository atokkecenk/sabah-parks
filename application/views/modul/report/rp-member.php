<style>
    .dataTables_filter {
        display: none !important;
    }
</style>
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <div class="widget widget-table-three addColor-widget">
        <div class="widget-heading">
            <h5 class="">REPORT MEMBER</h5>
        </div>
        <div class="widget-content">
            <div class="table-responsive mb-4 mt-4">
                <table class="table table-hover" id="tbReportMember" style="width: 100%;">
                    <thead>
                        <tr>
                            <th colspan="6"><a href="excel-member" role="button" class="btn btn-excel" target="_blank"><i class="fas fa-file-excel mr-2"></i>Export to Excel</a></th>
                        </tr>
                        <tr>
                            <th>NO.</th>
                            <th>FIRST NAME</th>
                            <th>LAST NAME</th>
                            <th>NATIONALITY</th>
                            <th>EMAIL</th>
                            <th>PHONE</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>