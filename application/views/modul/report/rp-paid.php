<style>
    .dataTables_filter {
        display: none !important;
    }

    .tbReportPaid>tbody>tr>td {
        white-space: nowrap !important;
    }
</style>
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <div class="widget widget-table-three addColor-widget">
        <div class="widget-heading">
            <h5 class="">REPORT PAID BOOKING</h5>
        </div>
        <div class="widget-content">
            <div class="card">
                <div class="card-body" style="padding: 0.8rem !important;">
                    <?= form_open('r-paid', 'method="post"') ?>
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <label>Option</label>
                            <select class="form-control" name="option">
                                <option value="">- Select One -</option>
                                <option value="PM" <?php if ($this->input->post('option') == 'PM') echo 'selected' ?>>PAID (Manual)</option>
                                <option value="PI" <?php if ($this->input->post('option') == 'PI') echo 'selected' ?>>PAID IPAY88</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-3">
                            <label>Date To Go</label>
                            <input class="form-control flatpickr-input active rangeCalendarFlatpickr" type="text" name="date_togo" value="<?php if (isset($_POST['date_togo'])) echo $_POST['date_togo'] ?>" placeholder="Select Date.." readonly="readonly">
                        </div>
                        <div class="form-group col-sm-6">
                            <label style="color: transparent;">x</label>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>

            <div class="table-scrollable mb-4 mt-4">
                <table class="table table-hover tbReportPaid" style="width: 100%;">
                    <thead>
                        <tr>
                            <th colspan="6"><button type="button" class="btn btn-excel" id="reportPaid"><i class="fas fa-file-excel mr-2"></i>Export to Excel</button></th>
                        </tr>
                        <tr>
                            <th>NO.</th>
                            <th>ORDER ID</th>
                            <th>ORDER DATE</th>
                            <th>DESTINATION</th>
                            <th>DATE TO GO</th>
                            <th>QTY</th>
                            <th>FIRSTNAME</th>
                            <th>LASTNAME</th>
                            <th>STATUS</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>