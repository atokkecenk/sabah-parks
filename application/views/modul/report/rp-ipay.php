<style>
    .dataTables_filter {
        display: none !important;
    }
</style>
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <div class="widget widget-table-three addColor-widget">
        <div class="widget-heading">
            <h5 class="">REPORT PAYMENT RESPONSE IPAY88</h5>
        </div>
        <div class="widget-content">
            <div class="card">
                <div class="card-body" style="padding: 0.8rem !important;">
                    <?= form_open('r-ipay', 'method="post"') ?>
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <label>Date Range</label>
                            <input class="form-control flatpickr-input active rangeCalendarFlatpickr" type="text" name="date_togo" value="<?php if (isset($_POST['date_togo'])) echo $_POST['date_togo'] ?>" placeholder="Select Date.." readonly="readonly">
                        </div>
                        <div class="form-group col-sm-6">
                            <label style="color: transparent;">x</label>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>

            <div class="table-scrollable mb-4 mt-4">
                <table class="table table-hover tbReportIpay" style="width: 100%;">
                    <thead>
                        <tr>
                            <th colspan="6"><button type="button" class="btn btn-excel" id="reportIpay"><i class="fas fa-file-excel mr-2"></i>Export to Excel</button></th>
                        </tr>
                        <tr>
                            <th>No</th>
                            <th>REF.NO</th>
                            <th>AMOUNT</th>
                            <th>CURRENCY</th>
                            <th>REMARK</th>
                            <th>TRANS.ID</th>
                            <th>AUTH. CODE</th>
                            <th>ERROR. DESC</th>
                            <th>SIGNATURE</th>
                            <th>CC.NAME</th>
                            <th>CC.NO</th>
                            <th>BANKNAME</th>
                            <th>COUNTRY</th>
                            <th>STATUS</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>