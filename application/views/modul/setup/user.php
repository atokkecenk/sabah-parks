<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <?php

    if ($this->session->flashdata('success')) {
        echo '<div class="alert alert-success mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('success') . '</button>
            </div>';
    } elseif ($this->session->flashdata('error')) {
        echo '<div class="alert alert-danger mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('error') . '</button>
            </div>';
    } elseif (validation_errors()) {
        echo '<div class="alert alert-danger mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . validation_errors() . '</button>
            </div>';
    }
    ?>

    <div class="widget widget-chart-one">
        <div class="widget-heading">
            <h5 class="">SETUP USER</h5>
            <ul class="tabs tab-pills">
                <li><a href="#" data-toggle="modal" data-target="#modalAddUser" class="tabmenu"><i class="fas fa-plus mr-2"></i>Add New</a></li>
            </ul>
        </div>
        <div class="widget-content">
            <div class="table-responsive mb-4 mt-4">
                <table class="table table-hover zero-config" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>NO.</th>
                            <th>FULLNAME</th>
                            <th>USERNAME</th>
                            <th>EMAIL</th>
                            <th>PHONE</th>
                            <th>USER ROLE</th>
                            <th>STATUS</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($data->result() as $dt) {
                            $sts = $dt->aktif == 'Y' ? '<span class="text-success">ACTIVE</span>' : '<span class="text-danger">NON-ACTIVE</span>';
                        ?>
                            <tr>
                                <td align="center"><?= $no ?></td>
                                <td><?= $dt->nama_lengkap ?></td>
                                <td><?= $dt->username ?></td>
                                <td><?= $dt->email ?></td>
                                <td><?= $dt->no_telpon ?></td>
                                <td><?= $dt->role_name ?></td>
                                <td align="center"><?= $sts ?></td>
                                <td align="center">
                                    <i class="fas fa-edit fa-2x text-secondary mr-2 editUser" title="Edit" role="button" data-id="<?= $dt->id_user ?>"></i>
                                    <a href="delete-user/<?= $dt->id_user ?>"><i class="fas fa-trash fa-2x text-danger"></i></a>
                                </td>
                            </tr>
                        <?php
                            $no++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Modal -->
        <div id="modalAddUser" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Setup User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </button>
                    </div>
                    <?= form_open('save-user', 'method="post"') ?>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="hidden" name="iduser">
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">User Role</label>
                                        <select class="form-control" name="role" id="role">
                                            <option value="0">- Select Role -</option>
                                            <?php
                                            foreach ($role->result() as $tp) {
                                                echo '<option value="' . $tp->id . '">' . $tp->role_name . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">Username</label>
                                        <input type="text" class="form-control" name="username" placeholder="username">
                                    </div>
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">Fullname</label>
                                        <input type="text" class="form-control" name="fullname" placeholder="fullname">
                                    </div>
                                    <div class="form-group d-block text-left">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck1" name="active" value="1">
                                            <label class="custom-control-label" for="customCheck1">Active</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">Email</label>
                                        <input type="email" class="form-control" name="email" placeholder="email">
                                    </div>
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">Phone</label>
                                        <input type="text" class="form-control" name="phone" placeholder="phone number">
                                    </div>
                                    <div class="form-group d-block text-left lbPass">
                                        <label class="d-block">Password</label>
                                        <input type="password" class="form-control" name="password" placeholder="password">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>

    </div>
</div>