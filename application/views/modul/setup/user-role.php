<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <?php

    if ($this->session->flashdata('success')) {
        echo '<div class="alert alert-success mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('success') . '</button>
            </div>';
    } elseif ($this->session->flashdata('error')) {
        echo '<div class="alert alert-danger mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('error') . '</button>
            </div>';
    } elseif (validation_errors()) {
        echo '<div class="alert alert-danger mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . validation_errors() . '</button>
            </div>';
    }
    ?>

    <div class="widget widget-chart-one">
        <div class="widget-heading">
            <h5 class="">SETUP CREDENTIAL</h5>
            <!-- <ul class="tabs tab-pills">
                <li><a href="#" data-toggle="modal" data-target="#modalAddUser" class="tabmenu"><i class="fas fa-plus mr-2"></i>Add New</a></li>
            </ul> -->
        </div>
        <div class="widget-content">
            <div class="table-responsive mb-4 mt-4">
                <div class="row">
                    <div class="col-sm-5">
                        <table class="table table-hover" style="width: 100%;">
                            <thead>
                                <tr>
                                    <td colspan="3">
                                        <a href="#" data-toggle="modal" data-target="#modalAddUser" class="btn btn-primary"><i class="fas fa-plus mr-2"></i>Add New</a>
                                    </td>
                                </tr>
                                <tr class="text-center">
                                    <th>NO.</th>
                                    <th>CREDENTIAL</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($role->result() as $rl) {
                                ?>
                                    <tr>
                                        <td align="center"><?= $no ?></td>
                                        <td><?= $rl->role_name ?></td>
                                        <td align="center">
                                            <i class="fas fa-edit fa-2x text-secondary mr-2 editUserRole" title="Edit" role="button" data-id="<?= $rl->id ?>"></i>
                                            <a href="delete-role/<?= $rl->id ?>" title="Delete"><i class="fas fa-trash fa-2x text-danger mr-2"></i></a>
                                            <a role="button" title="Manage menu"><i class="fas fa-database fa-2x text-warning menuByRole" data-id="<?= $rl->id ?>" data-menu="<?= $rl->role_name ?>"></i></a>
                                        </td>
                                    </tr>
                                <?php
                                    $no++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-7">
                        <h6 class="h6-userrole">Setup Credential for <span class="role_menu">....</span></h6>
                        <?= form_open('save-menu', 'method="post" class="myform"') ?>
                        <input type="hidden" name="id_role">
                        <table class="table table-hover" style="width: 100%;">
                            <thead>
                                <tr>
                                    <td colspan="3">
                                        <button type="submit" class="btn btn-primary btnSaveUser"><i class="fas fa-save mr-2"></i>Save</button>
                                    </td>
                                </tr>
                                <tr class="text-center">
                                    <th>NO.</th>
                                    <th>MENU</th>
                                    <th>ACTIVE</th>
                                </tr>
                            </thead>
                            <tbody id="tbUserRole">
                                <tr>
                                    <td colspan="3" align="center">Please Select Load data by click icon <i class="fas fa-database text-warning"></i> on left table.</td>
                                </tr>
                            </tbody>
                        </table>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div id="modalAddUser" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Setup User Role Credential</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </button>
                    </div>
                    <?= form_open('save-user-role', 'method="post"') ?>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <input type="hidden" name="iduserRole">
                            <div class="form-group d-block text-left">
                                <label class="d-block">User Role Credential</label>
                                <input type="text" class="form-control" name="userrole" placeholder="User role credential">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>

    </div>
</div>