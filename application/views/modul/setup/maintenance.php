
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <?php
    if ($this->session->flashdata('success')) {
        echo '<div class="alert alert-success mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('success') . '</button>
            </div>';
    } elseif ($this->session->flashdata('error')) {
        echo '<div class="alert alert-danger mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('error') . '</button>
            </div>';
    }
    ?>
    <div class="alert-after-update"></div>
    <div class="widget widget-table-three">
        <div class="widget-heading">
            <h5 class="">DESTINATION</h5>
        </div>
        <div class="widget-content">
            <div class="table-responsive mb-4 mt-4">
                
            </div>

            

            <!-- Modal -->
            <div id="modalEditDest02" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Update Destination</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" id="dest_id2">
                            <!-- <div class="form-group">
                                <label class="d-block">Thumbnail</label>
                                <div class="custom-file mb-3 col-sm-5">
                                    <input type="file" name="file" class="custom-file-input" id="changeThumb">
                                    <label class="custom-file-label" for="changeThumb">Choose a file..</label>
                                </div> -->
                            <?php
                            $url = explode('.', $SB002->destphoto_cover);
                            $extension = end($url);
                            $filename = substr($SB002->destphoto_cover, 0, strpos($SB002->destphoto_cover, '.' . $extension));
                            $image = 'https://www.apisptix.sabahparks.org.my/public/images/' . $filename . '/' . $extension;
                            // echo '<img class="view-thumb mt-2" src="' . $image . '">';
                            ?>
                            <!-- <span class="view-thumb2"></span> 
                            </div>-->
                            <div class="form-group d-block text-left">
                                <label class="d-block">Destination Name</label>
                                <input type="text" class="form-control" placeholder="Destination name" id="nm_dst2" value="<?= $SB002->destname ?>">
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">Local Price 18 Above <i class="fas fa-arrow-up" style="color: #00e000;"></i></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text" id="btnGroupAddon2">RM</div>
                                            </div>
                                            <input type="text" class="form-control" id="rm_dst2_a" placeholder="ex: 10.00" value="<?= $SB002->loc_price_18above ?>" aria-label="Input group example" aria-describedby="btnGroupAddon2">
                                        </div>
                                        <small class="text-danger">Use point (.)</small>
                                    </div>
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">Local Quota</label>
                                        <input type="number" min="0" max="100" class="form-control" placeholder="0" id="loc_q_dst2" value="<?= $SB002->loc_quota ?>">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">Local Price 18 Below <i class="fas fa-arrow-down" style="color: #ff3333;"></i></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text" id="btnGroupAddon2">RM</div>
                                            </div>
                                            <input type="text" class="form-control" id="rm_dst2_b" placeholder="ex: 10.00" value="<?= $SB002->loc_price_18below ?>" aria-label="Input group example" aria-describedby="btnGroupAddon2">
                                        </div>
                                        <small class="text-danger">Use point (.)</small>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">International Price 18 Above <i class="fas fa-arrow-up" style="color: #00e000;"></i></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text" id="btnGroupAddon2">RM</div>
                                            </div>
                                            <input type="text" class="form-control" id="rm_dst2_c" placeholder="ex: 10.00" value="<?= $SB002->int_price_18above ?>" aria-label="Input group example" aria-describedby="btnGroupAddon2">
                                        </div>
                                        <small class="text-danger">Use point (.)</small>
                                    </div>
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">International Quota</label>
                                        <input type="number" min="0" max="100" class="form-control" placeholder="0" id="int_q_dst2" value="<?= $SB002->int_quota ?>">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">International Price 18 Below <i class="fas fa-arrow-down" style="color: #ff3333;"></i></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text" id="btnGroupAddon2">RM</div>
                                            </div>
                                            <input type="text" class="form-control" id="rm_dst2_d" placeholder="ex: 10.00" value="<?= $SB002->int_price_18below ?>" aria-label="Input group example" aria-describedby="btnGroupAddon2">
                                        </div>
                                        <small class="text-danger">Use point (.)</small>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <h4>Description</h4>
                                <textarea class="editordata" rows="8" name="desc" style="width: 100%;"></textarea>
                            </div>
                            <div class="form-group mt-3">
                                <table class="table table-hover">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th>NO.</th>
                                            <th>DESCRIPTION</th>
                                            <th>MALAYSIAN<br>18 YEARS OLD AND ABOVE</th>
                                            <th>MALAYSIAN<br>BELOW 18 YEARS OLD</th>
                                            <th>INTERNATIONAL<br>18 YEARS OLD AND ABOVE</th>
                                            <th>INTERNATIONAL<br>BELOW 18 YEARS OLD</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($detailsb2 as $sb02) {
                                        ?>
                                            <tr>
                                                <td align="center"><?= $no ?></td>
                                                <td><b><?= $sb02->price_type ?></b></td>
                                                <td style="align-items: center;" align="right"><b class="mr-2">RM</b> <input type="text" class="form-control w200 <?= $sb02->id . 'a' ?>" value="<?= $sb02->loc_price_18above ?>"></td>
                                                <td style="align-items: center;" align="right"><b class="mr-2">RM</b> <input type="text" class="form-control w200 <?= $sb02->id . 'b' ?>" value="<?= $sb02->loc_price_18below ?>"></td>
                                                <td style="align-items: center;" align="right"><b class="mr-2">RM</b> <input type="text" class="form-control w200 <?= $sb02->id . 'c' ?>" value="<?= $sb02->int_price_18above ?>"></td>
                                                <td style="align-items: center;" align="right"><b class="mr-2">RM</b> <input type="text" class="form-control w200 <?= $sb02->id . 'd' ?>" value="<?= $sb02->int_price_18below ?>"></td>
                                            </tr>
                                        <?php
                                            $no++;
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancel</button>
                            <button type="button" class="btn btn-primary formDest2">Save</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Cancel Booking -->
            <div id="modalLockquota" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Setup Lock Quota</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <?= form_open('lock-quota', 'method="post"') ?>
                        <div class="modal-body">
                            <input type="hidden" name="id">
                            <input type="hidden" name="destid">
                            <input type="hidden" name="sumquota">
                            <!-- <div class="row"> -->
                            <!-- <div class="col-sm-6"> -->
                            <div class="form-group d-block text-left">
                                <label class="d-block">Destination Name</label>
                                <span id="destName"></span>
                            </div>
                            <div class="form-group d-block text-left">
                                <label class="d-block">Date To Go</label>
                                <input class="form-control flatpickr flatpickr-input active cariTgl" type="text" name="date" placeholder="Select Date.." readonly="readonly">
                            </div>
                            <div class="form-group d-block text-left">
                                <label class="d-block">Qty Person</label>
                                <select class="form-control col-sm-6" name="jumlah" id="jumlah" required>
                                    <option value="">- Select One -</option>
                                </select>
                                <small class="text-danger">Available quota <span id="avb"></span> for this day <b><span id="dateAvb"></span></b></small>
                            </div>
                            <!-- </div> -->
                            <!-- </div> -->
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>