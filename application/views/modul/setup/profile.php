<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <?php
    if ($this->session->flashdata('success')) {
        echo '<div class="alert alert-success mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('success') . '</button>
            </div>';
    } elseif ($this->session->flashdata('error')) {
        echo '<div class="alert alert-danger mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('error') . '</button>
            </div>';
    } elseif (validation_errors()) {
        echo '<div class="alert alert-danger mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . validation_errors() . '</button>
                </div>';
    }
    ?>

    <div class="widget widget-chart-one">
        <div class="widget-heading">
            <h5 class="">PROFILE</h5>
        </div>
        <div class="widget-content">
            <?php
            error_reporting(0);
            $act = $_GET['act'];
            ?>
            <!-- <div id="tabsSimple" class="col-lg-12 col-12 layout-spacing"> -->
            <!-- <div class="statbox widget box box-shadow"> -->
            <!-- <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>Simple Tabs</h4>
                            </div>
                        </div>
                    </div> -->
            <!-- <div class="widget-content widget-content-area simple-tab"> -->
            <ul class="nav nav-tabs  mb-3 mt-3" id="simpletab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link <?php if (empty($act) || $act == NULL && $act !== '2') echo 'active'; ?>" id="home-tab" data-toggle="tab" href="#personal" role="tab" aria-controls="home" aria-selected="true">Personal Data</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if (!empty($act) || $act !== NULL && $act == '2') echo 'active'; ?>" id="contact-tab" data-toggle="tab" href="#pass" role="tab" aria-controls="contact" aria-selected="false">Password</a>
                </li>
            </ul>
            <div class="tab-content" id="simpletabContent">
                <div class="tab-pane fade <?php if (empty($act) || $act == NULL && $act !== '2') echo 'show active'; ?>" id="personal" role="tabpanel" aria-labelledby="home-tab">
                    <?= form_open_multipart('update-profile', 'method="post"') ?>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-4 col-form-label">Full Name</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="nama" placeholder="nama lengkap" value="<?= $data->nama_lengkap ?>">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-4 col-form-label">Email</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="email" placeholder="nama lengkap" value="<?= $data->email ?>">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-4 col-form-label">Telp</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="telp" placeholder="nama lengkap" value="<?= $data->no_telpon ?>">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-4 col-form-label">Photo</label>
                                <div class="col-sm-8">
                                    <div class="custom-file mb-3">
                                        <input type="file" class="custom-file-input" name="foto" id="customFile" onchange="return fotoProfile()">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                    <small class="text-danger mt-4">Extention file accept: JPEG, JPG, PNG.</small>
                                    <div id="messageFoto"></div>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-4 col-form-label" style="color: transparent;">x</label>
                                <div class="col-sm-8">
                                    <button type="submit" class="btn btn-primary btn-lg">Update</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row mb-4">
                                <?php
                                $id = $this->session->id_user;
                                $foto = $this->db->query("SELECT * FROM rb_users WHERE id_user = '$id'")->row();
                                ?>
                                <img src="assets/img/photo/<?= $foto->foto ?>" width="180" class="photo-profile">
                            </div>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
                <div class="tab-pane fade <?php if (!empty($act) || $act !== NULL && $act == '2') echo 'show active'; ?>" id="pass" role="tabpanel" aria-labelledby="contact-tab">
                    <p class="my-alert-danger">
                        After update password, <strong>system automatic close and logout</strong> from this page. <br>Please login again with New Password !!
                    </p>
                    <?= form_open('update-profile', 'method="post"') ?>
                    <input type="hidden" name="category" value="pwd">
                    <div class="form-group row mb-4">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label">New Password</label>
                        <div class="col-sm-5">
                            <input type="password" name="password" class="form-control" placeholder="new password">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label">Confirm Password</label>
                        <div class="col-sm-5">
                            <input type="password" name="cfpassword" class="form-control" placeholder="confirm password">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label" style="color: transparent;">x</label>
                        <div class="col-sm-5">
                            <button type="submit" class="btn btn-primary btn-lg">Update</button>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
            <!-- </div> -->
            <!-- </div> -->
            <!-- </div> -->

            <!-- <div class="profile-page">
                <?= form_open('update-profile', 'method="post"') ?>
                <input type="hidden" name="id" value="<?= $this->session->id_user ?>">
                <input type="hidden" name="set" value="profile">
                <div class="form-group row mb-3">
                    <label class="col-sm-2 col-form-label">Username</label>
                    <div class="col-sm-5">
                        <h5><b><?= $data->username ?></b><span role="button" class="badge badge-primary ml-2 editProfile"><i class="fas fa-edit"></i> Update Data</span><span role="button" class="badge badge-info ml-2 editPassword"><i class="fas fa-key"></i> Change Password</span><span class="cancelProfile"></span></h5>
                        <input type="text" class="form-control mt-2 hide profile" name="username" value="<?= $data->username ?>">
                    </div>
                </div>
                <div class="form-group row mb-3">
                    <label class="col-sm-2 col-form-label">Full Name</label>
                    <div class="col-sm-5">
                        <p class="lb-profile"><?= $data->nama_lengkap ?></p>
                        <input type="text" class="form-control hide profile" name="fname" value="<?= $data->nama_lengkap ?>">
                    </div>
                </div>
                <div class="form-group row mb-3">
                    <label class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-5">
                        <p class="lb-profile"><?= $data->nama_lengkap ?></p>
                        <input type="email" class="form-control hide profile" name="email" placeholder="Email" value="<?= $data->email ?>">
                    </div>
                </div>
                <div class="form-group row mb-3">
                    <label class="col-sm-2 col-form-label">Phone</label>
                    <div class="col-sm-5">
                        <p class="lb-profile"><?= $data->no_telpon ?></p>
                        <input type="text" class="form-control hide profile" name="phone" placeholder="phone" value="<?= $data->no_telpon ?>">
                    </div>
                </div>
                <div class="form-group row mb-3 btnProfile">
                </div>
                <?= form_close() ?>
            </div> -->
            <!-- <div class="password-page hide">
                <?= form_open('update-profile', 'method="post"') ?>
                <input type="hidden" name="set" value="password">
                <div class="form-group row mb-1">
                    <label class="col-sm-2 col-form-label" style="color: transparent;">x</label>
                    <div class="col-sm-5">
                        <h5>
                            <span role="button" class="badge badge-danger cancPassword"><i class="fas fa-times"></i> Cancel</span>
                        </h5>
                    </div>
                </div>
                <div class="form-group row mb-3">
                    <label class="col-sm-2 col-form-label">New Password</label>
                    <div class="col-sm-5">
                        <input type="password" class="form-control" name="password" placeholder="New password">
                    </div>
                </div>
                <div class="form-group row mb-3">
                    <label class="col-sm-2 col-form-label">Confirm Password</label>
                    <div class="col-sm-5">
                        <input type="password" class="form-control" name="c_password" placeholder="Confirm password">
                    </div>
                </div>
                <div class="form-group row mb-3">
                    <label class="col-sm-2 col-form-label" style="color: transparent;">Phone</label>
                    <div class="col-sm-5">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
                <?= form_close() ?>
            </div> -->
        </div>
    </div>
</div>
<script>
    function fotoProfile() {
        var messageFoto = document.getElementById('messageFoto');
        messageFoto.innerHTML = '<span class="text-success"><i class="fas fa-upload mr-2"></i><b>File ready to upload.</b></span>';
    }
</script>