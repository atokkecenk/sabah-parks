<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <?php
    if ($this->session->flashdata('success')) {
        echo '<div class="alert alert-success mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('success') . '</button>
            </div>';
    } elseif ($this->session->flashdata('error')) {
        echo '<div class="alert alert-danger mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('error') . '</button>
            </div>';
    }
    ?>

    <div class="widget widget-chart-one">
        <div class="widget-heading">
            <h5 class="">SETUP GALLERY</h5>
            <ul class="tabs tab-pills">
                <li><a href="#" data-toggle="modal" data-target="#modalAddGallery" class="tabmenu"><i class="fas fa-plus mr-2"></i>Add New</a></li>
            </ul>
        </div>
        <div class="widget-content">
            <div class="table-responsive mb-4 mt-4">
                <table class="table table-hover zero-config" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>NO.</th>
                            <th>IMAGE</th>
                            <th>DESCRIPTION</th>
                            <th>TYPE</th>
                            <th>STATUS</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($data->result() as $dt) {
                            $sts = $dt->status == '1' ? '<span class="text-success">ACTIVE</span>' : '<span class="text-danger">NON-ACTIVE</span>';
                        ?>
                            <tr>
                                <td align="center"><?= $no ?></td>
                                <td>
                                    <img src="assets/img/<?= $dt->path ?>" width="150">
                                </td>
                                <td><?= $dt->description ?></td>
                                <td><?= $dt->desttype ?></td>
                                <td align="center"><?= $sts  ?></td>
                                <td align="center">
                                    <i class="fas fa-edit fa-2x text-secondary mr-2 editGallery" title="Edit" role="button" data-id="<?= $dt->id ?>"></i>
                                    <a href="delete-gallery/<?= $dt->id ?>" title="Delete"><i class="fas fa-trash fa-2x text-danger"></i></a>
                                </td>
                            </tr>
                        <?php
                            $no++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Modal -->
        <div id="modalAddGallery" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Setup Gallery</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </button>
                    </div>
                    <?= form_open('upload-gallery', 'method="post" enctype="multipart/form-data"') ?>
                    <div class="modal-body">
                        <input type="hidden" name="id" id="idGallery">
                        <div class="form-group d-block text-left">
                            <label class="d-block">Upload</label>
                            <div class="custom-file mb-3">
                                <input type="file" name="file" class="custom-file-input" id="uploadGallery">
                                <label class="custom-file-label" for="uploadGallery">Choose a file..</label>
                            </div>
                            <div class="galleryView"></div>
                        </div>
                        <div class="form-group d-block text-left">
                            <label class="d-block">Type</label>
                            <select class="form-control" name="type" id="type">
                                <option value="0">- Select Type -</option>
                                <?php
                                foreach ($type->result() as $tp) {
                                    echo '<option value="' . $tp->id . '">' . $tp->desttype . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group d-block text-left">
                            <label class="d-block">Description</label>
                            <textarea class="form-control" name="desc" rows="4"></textarea>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck1" name="active" value="1">
                            <label class="custom-control-label" for="customCheck1">Active</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>

    </div>
</div>