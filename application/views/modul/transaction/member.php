<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <div class="widget widget-table-three addColor-widget">
        <div class="widget-heading">
            <h5 class="">MEMBER</h5>
        </div>
        <div class="widget-content">
            <div class="table-responsive mb-4 mt-4">
                <table class="table table-hover" id="tbMember" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>NO.</th>
                            <th>FIRST NAME</th>
                            <th>LAST NAME</th>
                            <th>NATIONALITY</th>
                            <th>EMAIL</th>
                            <th>PHONE</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <!-- Modal -->
            <div id="mdMember" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Detail Info</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12">
                                <div class="row memberDetailBody">
                                    
                                </div>
                            </div>
                        </div>
                        <!-- <div class="modal-footer md-button">
                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                            <button type="button" class="btn btn-primary">Save</button>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>