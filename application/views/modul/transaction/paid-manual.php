<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <?php
    if ($this->session->flashdata('success')) {
        echo '<div class="alert alert-success mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('success') . '</button>
            </div>';
    } elseif ($this->session->flashdata('error')) {
        echo '<div class="alert alert-danger mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('error') . '</button>
            </div>';
    }
    ?>
    <div class="alert-after-update"></div>
    <div class="widget widget-table-three">
        <div class="widget-heading">
            <h5 class="">PAID MANUAL <span class="badge badge-success">NEW FEATURE</span></h5>
        </div>
        <div class="widget-content">
            <?= form_open('paid-manual', 'method="post"') ?>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group d-block text-left">
                        <label class="d-block">Order ID</label>
                        <input type="text" class="form-control" name="orderid" placeholder="ex: #ED81Q7LE" required>
                        <small class="text-danger">Paste order ID and check</small>
                    </div>
                </div>
                <div class="col-sm-2">
                    <label class="d-block" style="color: transparent">x</label>
                    <button type="submit" class="btn btn-primary btn-lg"><i class="fas fa-search mr-2"></i>Check</button>
                </div>
            </div>
            <?= form_close() ?>
            <?php if ($cek !== '') { ?>
                <div class="card mt-4" style="background-color: #4b4b4b;">
                    <div class="card-body">
                        <!-- <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group d-block text-left">
                                    <label class="d-block">Transaction ID</label>
                                    <input type="text" class="form-control" name="receipt_id" id="transid" placeholder="ex: T122236488922" value="' . $transid . '" readonly>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group d-block text-left">
                                    <label class="d-block">Order ID</label>
                                    <input type="text" class="form-control" name="order_id" id="orderid" placeholder="ex: #XAC2B9..." value="' . $bill->billing_id . '" readonly>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <label class="d-block" style="color: transparent">x</label>
                                <button type="submit" class="btn btn-warning btn-lg" onclick="return eticket()"><i class="fas fa-ticket-alt mr-2"></i>Send Ticket</button>
                                <button type="submit" class="btn btn-warning btn-lg ml-1" onclick="return invoice()"><i class="fas fa-paper-plane mr-2"></i>Send Invoice</button>
                            </div>
                        </div> -->

                        <?php
                        $bill = $cek->billing_id;
                        if ($cek->status == '1') {
                            $nbill = str_replace('#', '', $bill);
                        ?>
                            <div class="card">
                                <div class="card-body">
                                    <?= form_open('upload-receipt', 'method="post" enctype="multipart/form-data"') ?>
                                    <input type="hidden" name="id" value="<?= $cek->id ?>">
                                    <input type="hidden" name="billing" value="<?= $nbill ?>">
                                    <input type="hidden" name="redirect" value="paid-manual">
                                    <div class="row">
                                        <div class="form-group col-sm-4">
                                            <label>Note</label>
                                            <textarea class="form-control" name="note" cols="10" rows="2" placeholder="Optional"></textarea>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label>Upload Resit</label>
                                            <div class="custom-file mb-3">
                                                <input type="file" name="file" class="custom-file-input" name="foto" id="customFile" onchange="return fotoProfile()" required>
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                            </div>
                                            <small class="text-danger mt-4">Extention file accept: JPEG, JPG, PNG.</small>
                                            <div id="messageFoto"></div>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label style="color: transparent;">x</label>
                                            <div>
                                                <button type="submit" class="btn btn-primary btn-lg"><i class="fas fa-paper-plane mr-2"></i>Save</button>
                                            </div>
                                        </div>
                                    </div>
                                    <?= form_close() ?>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="card mt-2">
                            <div class="card-body">
                                <h5>Order ID : <span class="badge badge-danger" style="font-size: 15px;"><?= $cek->billing_id ?></span></h5>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr class="text-center">
                                                <!-- <th>ORDER ID</th> -->
                                                <th>FIRST NAME</th>
                                                <th>LAST NAME</th>
                                                <th>EMAIL</th>
                                                <th>TOTAL PAX</th>
                                                <th>TOTAL COST</th>
                                                <th>STATUS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <!-- <td align="center"><?= '<b>' . $cek->billing_id . '</b>' ?></td> -->
                                                <td><?= $cek->first_name ?></td>
                                                <td><?= $cek->last_name ?></td>
                                                <td><?= $cek->email ?></td>
                                                <td align="center"><?= $cek->total_pax ?></td>
                                                <td align="right">RM <?= $cek->total_cost ?>&nbsp;</td>
                                                <td align="center">
                                                    <?php
                                                    if ($cek->status == '1') {
                                                        echo '<span class="text-danger">UNPAID</span>';
                                                    } elseif ($cek->status == '2') {
                                                        if ($cek->status == '2' && $cek->receipt == '' || empty($cek->receipt)) {
                                                            echo '<span class="text-success">PAID (IPAY 88)</span>';
                                                        } else {
                                                            echo '<span class="text-success">PAID (Manual)</span>';
                                                        }
                                                    } else {
                                                        echo '<span class="text-danger">CANCEL</span>';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="card mt-2">
                            <div class="card-body">
                                <h5>Climber Details :</h5>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr class="text-center">
                                                <th>NO.</th>
                                                <th>NAME</th>
                                                <th>DATE TO GO</thclass=>
                                                <th>ID NUMBER</th>
                                                <th>EMAIL</th>
                                                <th>NATIONALITY</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $get_details = $this->db->query("SELECT
                                                                                * 
                                                                            FROM
                                                                                trx_billing_visitor 
                                                                            WHERE
                                                                                billing_id = '$bill'")->result();
                                            $no = 1;
                                            foreach ($get_details as $dt) {
                                            ?>
                                                <tr>
                                                    <td align="center"><?= $no++ ?></td>
                                                    <td><?= $dt->visitor_name ?></td>
                                                    <td align="center"><?= $dt->ticketdatefrom ?></td>
                                                    <td><?= $dt->id_number ?></td>
                                                    <td><?= $dt->email ?></td>
                                                    <td><?= $dt->nationality ?></td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script>
    function fotoProfile() {
        var messageFoto = document.getElementById('messageFoto');
        messageFoto.innerHTML = '<span class="text-success"><i class="fas fa-upload mr-2"></i><b>File ready to upload.</b></span>';
    }
</script>