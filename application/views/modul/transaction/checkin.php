<style>
    .tbCheckin>tbody>tr>td {
        white-space: nowrap !important;
    }
</style>
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <?php
    if ($this->session->flashdata('success')) {
        echo '<div class="alert alert-success mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('success') . '</button>
            </div>';
    } elseif ($this->session->flashdata('error')) {
        echo '<div class="alert alert-danger mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('error') . '</button>
            </div>';
    }
    ?>

    <div class="widget widget-table-three addColor-widget">
        <div class="widget-heading">
            <h5 class="">CHECK IN</h5>
        </div>
        <div class="widget-content">
            <div class="card">
                <div class="card-body" style="padding: 0.8rem !important;">
                    <?= form_open('scan', 'method="post"') ?>
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <label>Ticket ID</label>
                            <input type="text" class="form-control" name="ticketId" placeholder="ex: T09297730172117387" value="<?php if (isset($_POST['ticketId'])) echo $_POST['ticketId'] ?>">
                            <small class="text-danger">Search by Ticket ID number</small>
                        </div>
                        <div class="form-group col-sm-6">
                            <label style="color: transparent;">x</label>
                            <div class="form-group">
                                <button type="button" class="btn btn-danger btn-lg" onclick="location.href='scan'"><i class="fas fa-undo mr-2"></i>Reset Filter</button>
                                <button type="submit" class="btn btn-primary btn-lg"><i class="fas fa-search mr-2"></i>Showing Data</button>
                            </div>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>

            <div class="table-scrollable mb-4 mt-4">
                <table class="table table-hover tbCheckin" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>NO.</th>
                            <th>ACTION</th>
                            <th>DATE TO GO</th>
                            <th>DESTINATION</th>
                            <th>NAME</th>
                            <th>ID NUMBER</th>
                            <th>PHONE</th>
                            <th>EMAIL</th>
                            <th>NATIONALITY</th>
                            <th>PR.STATUS</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <!-- Cancel Booking -->
            <div id="modalCheckin" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Check In</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <?= form_open('update-ticket', 'method="post"') ?>
                        <div class="modal-body">
                            <input type="hidden" name="id">
                            <input type="hidden" name="redirect" value="scan">
                            <div class="form-group d-block text-left">
                                <label class="d-block">Ticket Status</label>
                                <select class="form-control" name="status">
                                    <option value="">- Select One -</option>
                                    <option value="C">Corrupted</option>
                                    <option value="L">Lost</option>
                                    <option value="I">Invalid</option>
                                </select>
                            </div>
                            <div class="form-group d-block text-left">
                                <label class="d-block">Note</label>
                                <textarea class="form-control" name="note" rows="6" required placeholder="Input a notes.."></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancel</button>
                            <button type="submit" class="btn btn-primary">Check In</button>
                        </div>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>