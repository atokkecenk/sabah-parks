<?php
$doc = isset($_GET['doc']) ? 1 : 0;
$receipt = $_GET['rc'];
if ($doc == 1) {
    echo "<script>
        function back() {
            document.location='send-doc?rc=$receipt';
        }
        setTimeout(back, 900);
        </script>";
}
?>
<style>
    .tbBilling>tbody>tr>td {
        white-space: nowrap !important;
    }
</style>
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <?php
    if ($this->session->flashdata('success')) {
        echo '<div class="alert alert-success mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('success') . '</button>
            </div>';
    } elseif ($this->session->flashdata('error')) {
        echo '<div class="alert alert-danger mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('error') . '</button>
            </div>';
    }
    ?>

    <div class="widget widget-table-three addColor-widget">
        <div class="widget-heading">
            <h5 class="">BILLING</h5>
        </div>
        <div class="widget-content">
            <div class="card">
                <div class="card-body" style="padding: 0.8rem !important;">
                    <?= form_open('billing', 'method="post"') ?>
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <label>Periodic</label>
                            <select class="form-control" name="year">
                                <option value="0">- Select One -</option>
                                <option value="all" <?php if ($this->input->post('year') == 'all') echo 'selected' ?>>All Data</option>
                                <?php
                                for ($i = (date('Y') + 1); $i >= 2020; $i--) {
                                    $slc = $i == $this->input->post('year') ? 'selected' : '';
                                    echo '<option value="' . $i . '" ' . $slc . '>' . $i . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-3">
                            <label>Date To Go</label>
                            <input class="form-control flatpickr-input active rangeCalendarFlatpickr" type="text" name="date_togo" value="<?php if (isset($_POST['date_togo'])) echo $_POST['date_togo'] ?>" placeholder="Select Date.." readonly="readonly">
                        </div>
                        <div class="form-group col-sm-3">
                            <label>Destination</label>
                            <select class="form-control" name="dest" id="selectDestBilling">
                                <option value="0">- Select One -</option>
                                <?php
                                foreach ($dest as $dst) {
                                    $slc = ($_POST['dest'] && $_POST['dest'] == $dst->destid) ? 'selected' : null;
                                    echo '<option value="' . $dst->destid . '" ' . $slc . '>' . $dst->destname . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-3">
                            <label>Status</label>
                            <select class="form-control" name="status" id="selectStatusBilling">
                                <option value="0">- Select One -</option>
                                <option value="2" <?php if ($_POST['status'] == '2') echo 'selected'; ?>>PAID</option>
                                <option value="1" <?php if ($_POST['status'] == '1') echo 'selected'; ?>>UNPAID</option>
                                <option value="3" <?php if ($_POST['status'] == '3') echo 'selected'; ?>>CANCEL</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12 text-center">
                            <label style="color: transparent;">x</label>
                            <div class="form-group">
                                <button type="button" class="btn btn-danger btn-lg" onclick="location.href='billing'"><i class="fas fa-undo mr-2"></i>Reset Filter</button>
                                <button type="submit" class="btn btn-primary btn-lg"><i class="fas fa-search mr-2"></i>Showing Data</button>
                            </div>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
            <div class="table-scrollable mb-4 mt-4">
                <table class="table table-hover tbBilling" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>NO.</th>
                            <th>ACTION</th>
                            <th>ORDER ID</th>
                            <th>ORDER DATE</th>
                            <th>FIRST NAME</th>
                            <th>LAST NAME</th>
                            <th>TOTAL PAX</th>
                            <th>COUNTRY</th>
                            <th>ADDRESS</th>
                            <th>EMAIL</th>
                            <th>PHONE</th>
                            <th>DESTINATION</th>
                            <th>DATE TO GO</th>
                            <th>PAYMENT REF.NO</th>
                            <th>PAYMENT DATE</th>
                            <th>STATUS</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <!-- Modal -->
            <div id="mdBilling" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Detail Info</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="destId" id="destIdBilling">
                            <div class="col-md-12">
                                <div class="row billingDetailBody">
                                </div>
                            </div>
                            <div class="alertCekDate"></div>
                            <input type="hidden" name="billId" id="billIdBilling">
                            <div class="mt-3 historyChange" style="display: none;">
                                <table id="tblHsitoryDate" style="width: 100%;border: 1px solid #e4e4e4;">
                                    <thead>
                                        <tr class="text-center">
                                            <th rowspan="2">No.</th>
                                            <th colspan="2">Date To go</th>
                                            <th colspan="2"></th>
                                        </tr>
                                        <tr class="text-center">
                                            <th>Before</th>
                                            <th>After</th>
                                            <th>Change By</th>
                                            <th>Change Date</th>
                                        </tr>
                                    </thead>
                                    <tbody id="historyChgTable">
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12 mt-3" id="changeDate" style="display: none;">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label style="font-weight: 700;color: #51536b;font-size: 18px;">Select Date</label>
                                        <input type="hidden" name="idDatetogo">
                                        <input type="hidden" name="dateBefore">
                                        <!-- <input type="text" name="fullDatetogo"> -->
                                        <input class="form-control flatpickr flatpickr-input active pull-left" id="date_togo" name="date_togo" type="text" placeholder="Year-Month-Day" readonly="readonly">
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="info-cdtg">
                                            <h5>Information :</h5>
                                            <p><span class="badge badge-danger" style="font-size:9px; border-radius: 30px;color: #e7515a;">0</span> &rarr; Date <b>Full</b></p>
                                            <p><span class="badge badge-success" style="font-size:9px; border-radius: 30px;color: #8dbf42;">0</span> &rarr; Date <b>Free</b></p>
                                            <p><span class="badge" style="font-size:9px; background-color: #0032ff;border-radius: 30px;color: #0032ff">0</span> &rarr; <b>Available</b> Quota</p>
                                        </div>
                                        <div class="info-cdtg mt-3">
                                            <h5>View Date :</h5>
                                            <div class="loading" style="margin-top: 8px;"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="info-cdtg">
                                            <h5>Climber :</h5>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="radioAll" name="customRadioInline1" class="custom-control-input">
                                                <label class="custom-control-label" for="radioAll">Update All</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline mb-2">
                                                <input type="radio" id="radioSelect" name="customRadioInline1" class="custom-control-input">
                                                <label class="custom-control-label" for="radioSelect">Update By Select Climber</label>
                                            </div>
                                            <div id="showDataClimber"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3" id="receipt" style="display: none;">

                            </div>
                            <?= form_open('change-date-togo', 'method="post" id="formBilling" style="display: none;"') ?>
                            <input type="text" name="destId">
                            <input type="text" name="idDatetogo">
                            <input type="text" name="dateBefore">
                            <input type="text" name="dateAfter">
                            <input type="text" name="personid" id="personid">
                            <button type="submit" class="submitBilling">save</button>
                            <?= form_close() ?>
                        </div>
                        <div class="modal-footer md-button">
                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancel</button>
                            <button type="button" class="btn btn-primary cekSek">Save</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal Edit -->
            <div id="mdEditBilling" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Edit Data Billing</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <?= form_open('save-edit-biling', 'method="post"') ?>
                        <div class="modal-body">
                            <div class="col-md-12">
                                <div class="row">
                                    <input type="hidden" name="id" class="form-control">
                                    <div class="col-sm-6">
                                        <div class="form-group d-block text-left">
                                            <label class="d-block">First Name</label>
                                            <input type="text" name="fname" class="form-control">
                                        </div>
                                        <div class="form-group d-block text-left">
                                            <label class="d-block">Country</label>
                                            <input type="text" name="country" class="form-control">
                                        </div>
                                        <div class="form-group d-block text-left">
                                            <label class="d-block">Email</label>
                                            <input type="email" name="email" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group d-block text-left">
                                            <label class="d-block">Last Name</label>
                                            <input type="text" name="lname" class="form-control">
                                        </div>
                                        <div class="form-group d-block text-left">
                                            <label class="d-block">Address</label>
                                            <textarea class="form-control" name="address" rows="2"></textarea>
                                        </div>
                                        <div class="form-group d-block text-left">
                                            <label class="d-block">Phone</label>
                                            <input type="text" name="phone" max="15" min="0" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer md-button">
                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div id="modalUploadResit" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Upload Resit</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <?= form_open('upload-receipt', 'method="post" enctype="multipart/form-data"') ?>
                        <div class="modal-body">
                            <input type="hidden" name="id" id="idResit">
                            <input type="hidden" name="redirect" value="billing">
                            <div class="form-group d-block text-left">
                                <label class="d-block">Upload</label>
                                <div class="custom-file mb-3">
                                    <input type="file" name="file" class="custom-file-input" id="uploadGallery">
                                    <label class="custom-file-label" for="uploadGallery">Choose a file resit..</label>
                                </div>
                                <small class="text-danger">File accept: JPG, JPEG, PNG</small>
                                <div class="galleryView"></div>
                            </div>
                            <div class="form-group d-block text-left">
                                <label class="d-block">Note</label>
                                <textarea class="form-control" name="note" rows="4" placeholder="Optional"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>

            <!-- Cancel Booking -->
            <div id="modalCancelBooking" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Cancel Confirmation</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <?= form_open('cancel-booking', 'method="post"') ?>
                        <input type="hidden" name="billing_id" id="billIdCancel">
                        <input type="hidden" name="method" id="cancelMethod">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <button type="button" class="btn btn-secondary btn-block btn-rounded" onclick="return cPersonal()">Cancel Personal</button>
                                </div>
                                <div class="col-sm-6">
                                    <button type="button" class="btn btn-secondary btn-block btn-rounded" onclick="return cAll()">Cancel All</button>
                                </div>
                            </div>
                            <div id="cPersonal" class="mt-3" style="display: none;">
                            </div>
                            <div id="cAll" class="mt-3" style="display: none;">
                                <input type="hidden" name="id">
                                <input type="hidden" name="redirect" value="billing">
                                <h6><span class="text-danger">Cancel this Order ID &rarr; <b><span id="orderIdCancel"></span></b></span></h6>
                                <div class="form-group d-block text-left">
                                    <label class="d-block">Remark</label>
                                    <textarea class="form-control" name="remarkAll" id="remarkAll" rows="8" placeholder="ex: Slot is full.."></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    function cAll() {
        const all = document.querySelector('#cAll');
        const person = document.querySelector('#cPersonal');
        document.querySelector('#cancelMethod').value = 'all';
        all.style.cssText = 'display: block;';
        person.style.cssText = 'display: none;';
    }

    function cPersonal() {
        const person = document.querySelector('#cPersonal');
        const all = document.querySelector('#cAll');

        all.style.cssText = 'display: none;';

        var billing = document.getElementById('billIdCancel').value;
        var bill = billing.replace("#", "");
        try {
            fetch('http://localhost/project/booking-sabah/get-climber-by-bill/' + bill, {
                    method: "GET",
                    headers: {
                        "Content-type": "application/json;charset=UTF-8"
                    }
                    // body: formData,
                })
                .then(response => response.json())
                .then(json => {
                    if (json[0]['msg'] == 'error') {
                        person.style.cssText = 'display: block;';
                        person.innerHTML = '<p class="text-danger text-center"><b>No climber found on this Order ID !!</b></p>';
                    } else {
                        person.style.cssText = 'display: block;overflow-y: scroll; height: 380px;';
                        document.querySelector('#cancelMethod').value = 'personal';
                        var sts, inp, css, html = '';
                        html += '<div class="form-group d-block text-left mb-3">' +
                            '<label class="d-block">Remark</label>' +
                            '<textarea class="form-control" name="remarkPersonal" rows="2" placeholder="ex: Slot is full.."></textarea>' +
                            '</div>';
                        html += '<span style="font-size: 15px;" class="mb-2">Select Climber</span>'
                        for (let i = 0; i < json.length; i++) {
                            if (json[i]['status'] == '3') {
                                css = '#ff7777';
                                sts = 'CANCEL';
                                inp = '';
                            } else {
                                css = '#e5e5e5';
                                sts = 'ACTIVE';
                                inp = '<input type="checkbox" name="climber[]" class="mt-2" value="' + json[i]['id'] + '"> Cancel this';
                            }
                            // console.log(json[i]['visitor']);
                            html += '<div style="background-color: ' + css + '; color: #000; padding: 3px 2px 2px 20px; border-radius: 4px; margin-bottom: 5px; margin-right: 3px;"><b>' + (parseInt(i) + 1) + '). </b>' + inp + '<br><b>Visitor Name : </b>' + json[i]['visitor'] + '<br><b>Email : </b>' + json[i]['email'] + '<br><b>Date To Go : </b>' + json[i]['datetogo'] + '<br><b>Status : </b>' + sts + '</div>';
                        }
                        person.innerHTML = html;
                    }
                });
        } catch (error) {
            console.log(error);
        }
    }
</script>