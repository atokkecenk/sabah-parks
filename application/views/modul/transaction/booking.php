<style>
    .tbOrder>tbody>tr>td {
        white-space: nowrap !important;
    }
</style>
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <?php
    if ($this->session->flashdata('success')) {
        echo '<div class="alert alert-success mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('success') . '</button>
            </div>';
    } elseif ($this->session->flashdata('error')) {
        echo '<div class="alert alert-danger mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('error') . '</button>
            </div>';
    }
    ?>

    <div class="widget widget-table-three addColor-widget">
        <div class="widget-heading">
            <h5 class="">BOOKING</h5>
        </div>
        <div class="widget-content">
            <div class="card">
                <div class="card-body" style="padding: 0.8rem !important;">
                    <?= form_open('order', 'method="post"') ?>
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <label>Periodic</label>
                            <select class="form-control" name="year">
                                <option value="0">- Select One -</option>
                                <option value="all" <?php if ($this->input->post('year') == 'all') echo 'selected' ?>>All Data</option>
                                <?php
                                for ($i = (date('Y') + 1); $i >= 2020; $i--) {
                                    $slc = $i == $this->input->post('year') ? 'selected' : '';
                                    echo '<option value="' . $i . '" ' . $slc . '>' . $i . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-3">
                            <label>Date To Go</label>
                            <input class="form-control flatpickr-input active rangeCalendarFlatpickr" type="text" name="date_togo" value="<?php if (isset($_POST['date_togo'])) echo $_POST['date_togo'] ?>" placeholder="Select Date.." readonly="readonly">
                        </div>
                        <!-- <div class="form-group col-sm-2">
                            <label>STATUS</label>
                            <select class="form-control" name="year">
                                <option value="0">- Select One -</option>
                                <option value="1">UNPAID</option>
                                <option value="2">PAID</option>
                                <option value="3">CANCEL</option>
                            </select>
                        </div> -->
                        <div class="form-group col-sm-6">
                            <label style="color: transparent;">x</label>
                            <div class="form-group">
                                <button type="button" class="btn btn-danger btn-lg" onclick="location.href='order'"><i class="fas fa-undo mr-2"></i>Reset Filter</button>
                                <button type="submit" class="btn btn-primary btn-lg"><i class="fas fa-search mr-2"></i>Showing Data</button>
                            </div>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>

            <div class="table-scrollable mb-4 mt-4">
                <table class="table table-hover tbOrder" style="width: 100%;">
                    <!-- <table class="table table-hover" border="1"> -->
                    <thead>
                        <tr>
                            <th>NO.</th>
                            <th>ACTION</th>
                            <th>ORDER ID</th>
                            <th>ORDER DATE</th>
                            <th>DESTINATION</th>
                            <th>DATE TO GO</th>
                            <th>QTY</th>
                            <th>FIRSTNAME</th>
                            <th>LASTNAME</th>
                            <th>STATUS</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <!-- Modal -->
            <div id="mdOrder" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Detail Info</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <?= form_open('#', 'method="post" id="formChangeDate"') ?>
                        <div class="modal-body">
                            <!-- <div class="col-md-12 mb-2">
                                <span class="badge badge-warning">History change date to go</span>
                                <span class="badge badge-danger">Change date to go</span>
                            </div> -->
                            <div class="col-md-12">
                                <div class="row orderDetailBody"></div>
                            </div>
                            <div class="alertCekDate"></div>
                            <input type="hidden" name="billId">
                            <div class="mt-3 historyChange" style="display: none;">
                                <table id="tblHsitoryDate" style="width: 100%;border: 1px solid #e4e4e4;">
                                    <thead>
                                        <tr class="text-center">
                                            <th rowspan="2">No.</th>
                                            <th colspan="2">Date To go</th>
                                            <th colspan="2"></th>
                                        </tr>
                                        <tr class="text-center">
                                            <th>Before</th>
                                            <th>After</th>
                                            <th>Change By</th>
                                            <th>Change Date</th>
                                        </tr>
                                    </thead>
                                    <tbody id="historyChgTable">
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12 mt-3" id="changeDate" style="display: none;">
                                <input type="hidden" name="destId">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="hidden" name="idDatetogo">
                                        <input type="hidden" name="dateBefore">
                                        <!-- <input type="text" name="fullDatetogo"> -->
                                        <input class="form-control flatpickr flatpickr-input active pull-left" id="date_togo" name="date_togo" type="text" placeholder="Select Date.." readonly="readonly">
                                    </div>
                                    <div class="col-sm-6">
                                        <h4>Date Full :</h4>
                                        <div class="loading" style="margin-top: 8px;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-3" id="receipt" style="display: none;">

                            </div>
                        </div>
                        <div class="modal-footer md-button">
                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                            <button type="button" class="btn btn-primary cekSek">Save</button>
                        </div>
                        <?= form_close() ?>
                    </div>

                </div>
            </div>

            <!-- Modal -->
            <div id="modalUploadResit" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Upload Resit</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <?= form_open('upload-receipt', 'method="post" enctype="multipart/form-data"') ?>
                        <div class="modal-body">
                            <input type="hidden" name="id" id="idResit">
                            <input type="hidden" name="redirect" value="order">
                            <div class="form-group d-block text-left">
                                <label class="d-block">Upload</label>
                                <div class="custom-file mb-3">
                                    <input type="file" name="file" class="custom-file-input" id="uploadGallery">
                                    <label class="custom-file-label" for="uploadGallery">Choose a file resit..</label>
                                </div>
                                <small class="text-danger">File accept: JPG, JPEG, PNG</small>
                                <div class="galleryView"></div>
                            </div>
                            <div class="form-group d-block text-left">
                                <label class="d-block">Note</label>
                                <textarea class="form-control" name="note" rows="4" placeholder="Optional"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>

            <!-- Cancel Booking -->
            <div id="modalCancelBooking" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Cancel Confirmation</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <?= form_open('cancel-booking', 'method="post"') ?>
                        <div class="modal-body">
                            <input type="hidden" name="id">
                            <input type="hidden" name="redirect" value="order">
                            <h6><span class="text-danger">Cancel this Order ID &rarr; <b><span id="orderIdCancel"></span></b></span></h6>
                            <div class="form-group d-block text-left">
                                <label class="d-block">Remark</label>
                                <textarea class="form-control" name="remark" rows="8" required placeholder="ex: Slot is full.."></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>