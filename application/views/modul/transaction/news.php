<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <?php
    if ($this->session->flashdata('success')) {
        echo '<div class="alert alert-success mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('success') . '</button>
            </div>';
    } elseif ($this->session->flashdata('error')) {
        echo '<div class="alert alert-danger mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('error') . '</button>
            </div>';
    } elseif (validation_errors()) {
        echo '<div class="alert alert-danger mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . validation_errors() . '</button>
            </div>';
    }
    ?>

    <div class="widget widget-chart-one">
        <div class="widget-heading">
            <h5 class="">NEWS, BLOGS & EVENTS</h5>
            <ul class="tabs tab-pills">
                <li><a href="#" data-toggle="modal" data-target="#modalAddNews" class="tabmenu"><i class="fas fa-plus mr-2"></i>Add New</a></li>
            </ul>
        </div>
        <div class="widget-content">
            <div class="table-responsive mb-4 mt-4">
                <table class="table table-hover zero-config" style="width: 100%;">
                    <thead>
                        <tr style="text-align: center;">
                            <th>NO.</th>
                            <th>DATE</th>
                            <th>TYPE</th>
                            <th>TITLE</th>
                            <th>IMAGE</th>
                            <th>STATUS</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($data->result() as $dt) {
                            $url = explode('.', $dt->path_cover);
                            $extension = end($url);
                            $filename = substr($dt->path_cover, 0, strpos($dt->path_cover, '.' . $extension));
                            $image = 'https://www.apisptix.sabahparks.org.my/public/images/' . $filename . '/' . $extension;
                            switch ($dt->type) {
                                case 1:
                                    $tp = 'News';
                                    break;
                                case 2:
                                    $tp = 'Promotion';
                                    break;
                                case 3:
                                    $tp = 'Events';
                                    break;
                                case 4:
                                    $tp = 'Blogs';
                                    break;
                            }

                            switch ($dt->status) {
                                case '1':
                                    $sts = '<span class="text-success">' . $dt->ket_status . '</span>';
                                    break;
                                case '0':
                                    $sts = '<span class="text-danger">' . $dt->ket_status . '</span>';
                                    break;
                            }
                        ?>
                            <tr>
                                <td align="center"><?= $no ?></td>
                                <td align="center"><?= $dt->createdate ?></td>
                                <td><?= $tp ?></td>
                                <td><?= $dt->blogtitle ?></td>
                                <td><img src="<?= $image ?>" class="img-news"></td>
                                <td align="center"><?= $sts ?></td>
                                <td align="center">
                                    <i class="fas fa-edit fa-2x text-secondary editNews" title="Edit" role="button" data-id="<?= $dt->id ?>"></i>
                                    <i class="fa fa-trash fa-2x ml-1 text-danger deleteNews" title="Delete" role="button" data-id="<?= $dt->id ?>"></i>
                                    <i class="fas fa-paper-plane fa-2x ml-1 text-info" title="Send to email" role="button" data-id="<?= $dt->id ?>"></i>
                                    <i class="fas fa-envelope fa-2x ml-1 text-success modalSendToInbox" title="Send to inbox" role="button" data-id="<?= $dt->id ?>"></i>
                                </td>
                            </tr>
                        <?php
                            $no++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>

            <!-- add new -->
            <div id="modalAddNews" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">NEWS, BLOGS & EVENTS</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <?= form_open('upload-news', 'method="post" enctype="multipart/form-data"') ?>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">Type</label>
                                        <select class="form-control" name="type">
                                            <option value="">- Select One -</option>
                                            <option value="1">News</option>
                                            <option value="2">Promotion</option>
                                            <option value="3">Events</option>
                                            <option value="4">Blogs</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">Date</label>
                                        <input class="form-control flatpickr flatpickr-input active" type="text" name="date" placeholder="Select Date.." readonly="readonly">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group d-block text-left">
                                <label class="d-block">Title</label>
                                <input type="text" class="form-control" placeholder="Title" name="title">
                            </div>
                            <div class="form-group d-block text-left">
                                <label class="d-block">Upload File</label>
                                <div class="custom-file mb-3">
                                    <input type="file" name="file" class="custom-file-input" id="uploadGallery">
                                    <label class="custom-file-label" for="uploadGallery">Choose a file..</label>
                                </div>
                                <small class="text-danger">File accept: JPG, JPEG, PNG</small>
                                <div class="galleryView"></div>
                            </div>
                            <div class="form-group d-block text-left">
                                <label class="d-block">Description</label>
                                <textarea class="form-control" name="desc" rows="4" placeholder="ex: Start your description here.."></textarea>
                            </div>
                            <div class="form-group d-block text-left">
                                <input type="checkbox" name="active"> Active
                                <!-- <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1" name="active" value="1">
                                    <label class="custom-control-label" for="customCheck1">Active</label>
                                </div> -->
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>

            <!-- modal edit -->
            <div id="modalEditNews" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">NEWS, BLOGS & EVENTS</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <?= form_open('save-edit-news', 'method="post" enctype="multipart/form-data"') ?>
                        <div class="modal-body">
                            <input type="hidden" name="id">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">Type</label>
                                        <select class="form-control" name="type" id="type">
                                            <option value="">- Select One -</option>
                                            <option value="1">News</option>
                                            <option value="2">Promotion</option>
                                            <option value="3">Events</option>
                                            <option value="4">Blogs</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">Date</label>
                                        <input class="form-control flatpickr flatpickr-input active" type="text" name="date" placeholder="Select Date.." readonly="readonly">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group d-block text-left">
                                <label class="d-block">Title</label>
                                <input type="text" class="form-control" placeholder="Title" name="title">
                            </div>
                            <div class="form-group d-block text-left">
                                <label class="d-block">Upload File</label>
                                <div class="custom-file mb-3">
                                    <input type="file" name="file" class="custom-file-input" id="uploadGallery">
                                    <label class="custom-file-label" for="uploadGallery">Choose a file..</label>
                                </div>
                                <div class="galleryView"></div>
                            </div>
                            <div class="form-group d-block text-left">
                                <label class="d-block">Description</label>
                                <textarea class="form-control" name="desc" rows="4" placeholder="ex: Start your description here.."></textarea>
                            </div>
                            <div class="form-group d-block text-left ">
                                <div class="checkNews"></div>
                                <!-- <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1" name="active" value="1" checked>
                                    <label class="custom-control-label" for="customCheck1">Active</label>
                                </div> -->
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>

            <!-- confirm -->
            <div id="modalDelete" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Confirmation</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?= form_open('delete-news', 'method="post"') ?>
                            <div id="msgRequery"></div>
                            <div class="col-md-12 text-center mb-3">
                                <input type="hidden" name="id" id="id">
                                <h5><i class="fas fa-question-circle text-warning"></i> Are you sure ?</h5>
                            </div>
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-success btn-sm">Yes</button>
                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">No</button>
                            </div>
                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- send to inbox -->
            <div id="modalSendToInbox" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Confirmation</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?= form_open('send-inbox', 'method="post"') ?>
                            <div id="msgRequery"></div>
                            <div class="col-md-12 text-center mb-3">
                                <input type="hidden" name="id" id="id">
                                <h5><i class="fas fa-question-circle text-warning"></i> Send Broadcast to inbox?</h5>
                            </div>
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-success btn-sm">Yes</button>
                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">No</button>
                            </div>
                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>