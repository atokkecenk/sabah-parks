<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <?php
    if ($this->session->flashdata('success')) {
        echo '<div class="alert alert-success mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('success') . '</button>
            </div>';
    } elseif ($this->session->flashdata('error')) {
        echo '<div class="alert alert-danger mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('error') . '</button>
            </div>';
    } elseif (validation_errors()) {
        echo '<div class="alert alert-danger mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . validation_errors() . '</button>
            </div>';
    }
    ?>

    <div class="widget widget-chart-one">
        <div class="widget-heading">
            <h5 class="">BROADCAST</h5>
            <ul class="tabs tab-pills">
                <li><a href="#" data-toggle="modal" data-target="#modalAddBroadcast" class="tabmenu"><i class="fas fa-plus mr-2"></i>Add New</a></li>
            </ul>
        </div>
        <div class="widget-content">
            <div class="table-responsive mb-4 mt-4">
                <table class="table table-hover zero-config" style="width: 100%;">
                    <thead>
                        <tr style="text-align: center;">
                            <th>NO.</th>
                            <th>DATE CREATED</th>
                            <th>DATE EXPIRED</th>
                            <th>TITLE</th>
                            <th>IMAGE</th>
                            <th>STATUS</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($data->result() as $dt) {
                            if (empty($dt->path)) {
                                $image = 'https://www.apisptix.sabahparks.org.my/public/default/default-photo/png';
                            } else {
                                $url = explode('.', $dt->path);
                                $extension = end($url);
                                $filename = substr($dt->path, 0, strpos($dt->path, '.' . $extension));
                                $image = 'https://www.apisptix.sabahparks.org.my/public/images/' . $filename . '/' . $extension;
                            }

                            switch ($dt->status) {
                                case 'Y':
                                    $sts = '<span class="text-success">ACTIVE</span>';
                                    break;
                                case 'N':
                                    $sts = '<span class="text-danger">NON-ACTIVE</span>';
                                    break;
                            }
                        ?>
                            <tr>
                                <td align="center"><?= $no ?></td>
                                <td align="center"><?= $dt->new_date ?></td>
                                <td align="center"><?= $dt->new_date_exp ?></td>
                                <td><?= $dt->title ?></td>
                                <td><img src="<?= $image ?>" class="img-news"></td>
                                <td align="center"><?= $sts ?></td>
                                <td align="center">
                                    <i class="fas fa-edit fa-2x text-secondary editBroadcast" role="button" data-id="<?= $dt->id ?>"></i>
                                    <i class="fa fa-trash fa-2x ml-1 text-danger deleteBroadcast" role="button" data-id="<?= $dt->id ?>"></i>
                                </td>
                            </tr>
                        <?php
                            $no++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>

            <!-- add new -->
            <div id="modalAddBroadcast" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">BROADCAST</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <?= form_open('upload-broadcast', 'method="post" enctype="multipart/form-data"') ?>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">Title</label>
                                        <input type="text" class="form-control" placeholder="Title" name="title">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">Expired Date</label>
                                        <input class="form-control flatpickr flatpickr-input active" type="text" name="date" placeholder="Select Date.." readonly="readonly">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group d-block text-left">
                                <label class="d-block">Upload File</label>
                                <div class="custom-file mb-3">
                                    <input type="file" name="file" class="custom-file-input" id="uploadGallery">
                                    <label class="custom-file-label" for="uploadGallery">Choose a file..</label>
                                </div>
                                <small class="text-danger">File accept: JPG, JPEG, PNG</small>
                                <div class="galleryView"></div>
                            </div>
                            <div class="form-group d-block text-left">
                                <label class="d-block">Description</label>
                                <textarea class="form-control" name="desc" rows="4" placeholder="ex: Start your description here.."></textarea>
                            </div>
                            <div class="form-group d-block text-left">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1" name="active" value="1">
                                    <label class="custom-control-label" for="customCheck1">Active</label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>

            <!-- modal edit -->
            <div id="modalEditBroadcast" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">BROADCAST</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <?= form_open('save-edit-broadcast', 'method="post" enctype="multipart/form-data"') ?>
                        <div class="modal-body">
                            <input type="hidden" name="id">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">Title</label>
                                        <input type="text" class="form-control" placeholder="Title" name="title">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group d-block text-left">
                                        <label class="d-block">Date Expired</label>
                                        <input class="form-control flatpickr flatpickr-input active" type="text" name="date" placeholder="Select Date.." readonly="readonly">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group d-block text-left">
                                <label class="d-block">Upload File</label>
                                <div class="custom-file mb-3">
                                    <input type="file" name="file" class="custom-file-input" id="uploadGallery">
                                    <label class="custom-file-label" for="uploadGallery">Choose a file..</label>
                                </div>
                                <small class="text-danger">File accept: JPG, JPEG, PNG</small>
                                <div class="galleryView"></div>
                            </div>
                            <div class="form-group d-block text-left">
                                <label class="d-block">Description</label>
                                <textarea class="form-control" name="desc" rows="4" placeholder="ex: Start your description here.."></textarea>
                            </div>
                            <div class="form-group d-block text-left">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1" name="active" value="1">
                                    <label class="custom-control-label" for="customCheck1">Active</label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>

            <!-- confirm -->
            <div id="modalDelete" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Confirmation</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?= form_open('delete-broadcast', 'method="post"') ?>
                            <div id="msgRequery"></div>
                            <div class="col-md-12 text-center mb-3">
                                <input type="hidden" name="id" id="id">
                                <h5><i class="fas fa-question-circle text-warning"></i> Are you sure ?</h5>
                            </div>
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-success btn-sm">Yes</button>
                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">No</button>
                            </div>
                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>