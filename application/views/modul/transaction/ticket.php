<style>
    .tbTicket>tbody>tr>td {
        white-space: nowrap !important;
    }
</style>
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <div class="widget widget-table-three addColor-widget">
        <div class="widget-heading">
            <h5 class="">TICKET</h5>
        </div>
        <div class="widget-content">
            <div class="card">
                <div class="card-body" style="padding: 0.8rem !important;">
                    <?= form_open('ticket', 'method="post"') ?>
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <label>Periodic</label>
                            <select class="form-control" name="year">
                                <option value="0">- Select One -</option>
                                <option value="all" <?php if ($this->input->post('year') == 'all') echo 'selected' ?>>All Data</option>
                                <?php
                                for ($i = (date('Y') + 1); $i >= 2020; $i--) {
                                    $slc = $i == $this->input->post('year') ? 'selected' : '';
                                    echo '<option value="' . $i . '" ' . $slc . '>' . $i . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-3">
                            <label>Date To Go</label>
                            <input class="form-control flatpickr-input active rangeCalendarFlatpickr" type="text" name="date_togo" value="<?php if (isset($_POST['date_togo'])) echo $_POST['date_togo'] ?>" placeholder="Select Date.." readonly="readonly">
                        </div>
                        <div class="form-group col-sm-6">
                            <label style="color: transparent;">x</label>
                            <div class="form-group">
                                <button type="button" class="btn btn-danger btn-lg" onclick="location.href='ticket'"><i class="fas fa-undo mr-2"></i>Reset Filter</button>
                                <button type="submit" class="btn btn-primary btn-lg"><i class="fas fa-search mr-2"></i>Showing Data</button>
                            </div>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
            <div class="table-scrollable mb-4 mt-4">
                <table class="table table-hover tbTicket" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>NO.</th>
                            <th>ACTION</th>
                            <th>DATE TO GO</th>
                            <th>TICKET ID</th>
                            <th>BILLING ID</th>
                            <th>DESTINATION</th>
                            <th>NAME</th>
                            <th>PHONE</th>
                            <th>EMAIL</th>
                            <th>NATIONALITY</th>
                            <th>PR.STATUS</th>
                            <th>STATUS</th>
                            <th>CHECK IN DATE</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <p class="ml-3 mt-2" style="margin-bottom: 0 !important;"><i class="fas fa-angle-right"></i> [<span class="text-success">OPEN</span>] Status indicates that the ticket is still available.</p>
                <p class="ml-3" style="margin-bottom: 0 !important;"><i class="fas fa-angle-right"></i> [<span class="text-primary">CHECK IN</span>] Status indicates that the ticket has been scanned and is no longer valid.</p>
                <p class="ml-3" style="margin-bottom: 0 !important;"><i class="fas fa-angle-right"></i> [<span class="text-danger">EXPIRED</span>] Status indicates that the ticket has expired and is no longer valid.</p>
            </div>

            <!-- Modal -->
            <div id="mdBilling" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Detail Info</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12">
                                <div class="row billingDetailBody">
                                </div>
                            </div>
                        </div>
                        <!-- <div class="modal-footer md-button">
                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                            <button type="button" class="btn btn-primary">Save</button>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>