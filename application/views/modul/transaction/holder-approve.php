<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <?php
    if ($this->session->flashdata('success')) {
        echo '<div class="alert alert-success mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('success') . '</button>
            </div>';
    } elseif ($this->session->flashdata('error')) {
        echo '<div class="alert alert-danger mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('error') . '</button>
            </div>';
    }
    ?>

    <div class="widget widget-table-three addColor-widget">
        <div class="widget-heading">
            <h5 class="">PR HOLDER APPROVAL</h5>
        </div>
        <div class="widget-content">
            <div class="card">
                <div class="card-body" style="padding: 0.8rem !important;">
                    <?= form_open('booking', 'method="post"') ?>
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <label>Periodic</label>
                            <select class="form-control" name="year">
                                <option value="0">- Select One -</option>
                                <option value="all" <?php if ($this->input->post('year') == 'all') echo 'selected' ?>>All Data</option>
                                <?php
                                for ($i = (date('Y') + 1); $i >= 2020; $i--) {
                                    $slc = $i == $this->input->post('year') ? 'selected' : '';
                                    echo '<option value="' . $i . '" ' . $slc . '>' . $i . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-3">
                            <label>Date To Go</label>
                            <input class="form-control flatpickr-input active rangeCalendarFlatpickr" type="text" name="date_togo" value="<?php if (isset($_POST['date_togo'])) echo $_POST['date_togo'] ?>" placeholder="Select Date.." readonly="readonly">
                        </div>
                        <div class="form-group col-sm-6">
                            <label style="color: transparent;">x</label>
                            <div class="form-group">
                                <button type="button" class="btn btn-danger btn-lg" onclick="location.href='booking'"><i class="fas fa-undo mr-2"></i>Reset Filter</button>
                                <button type="submit" class="btn btn-primary btn-lg"><i class="fas fa-search mr-2"></i>Showing Data</button>
                            </div>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
            <div class="table-responsive mb-4 mt-4">
                <table class="table table-hover tbHoldAppv">
                    <thead>
                        <tr class="text-center">
                            <th>NO.</th>
                            <th>APPROVED</th>
                            <th>ORDER ID</th>
                            <th>ORDER DATE</th>
                            <th>DESTINATION</th>
                            <th>DATE TO GO</th>
                            <th>QTY</th>
                            <th>FIRSTNAME</th>
                            <th>LASTNAME</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <!-- Modal -->
            <div id="mdHoldAppv" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Detail Info</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12">
                                <div class="row orderDetailBody">

                                </div>
                            </div>
                        </div>
                        <!-- <div class="modal-footer md-button">
                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                            <button type="button" class="btn btn-primary">Save</button>
                        </div> -->
                    </div>
                </div>
            </div>

            <!-- Modal Approved -->
            <div id="modalApprove" class="modal animated fadeInUp custo-fadeInUp" role="dialog">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Confirmation <span id="headHolderApprove"></span></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?= form_open('holder-approve', 'method="post"') ?>
                            <div class="col-md-12 text-center mb-3">
                                <input type="hidden" name="id" id="idHolderApprove">
                                <input type="hidden" name="status" id="statusHolderApprove">
                                <h5><i class="fas fa-question-circle text-warning"></i> Are you sure ?</h5>
                            </div>
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-success btn-sm">Yes</button>
                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">No</button>
                            </div>
                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>