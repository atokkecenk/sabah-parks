<?php
$receipt = isset($_GET['rc']) ? $_GET['rc'] : null;
?>
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <?php
    if ($this->session->flashdata('success')) {
        echo '<div class="alert alert-success mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('success') . '</button>
            </div>';
    } elseif ($this->session->flashdata('error')) {
        echo '<div class="alert alert-danger mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                ' . $this->session->flashdata('error') . '</button>
            </div>';
    }
    ?>
    <div class="alert-after-update"></div>
    <div class="widget widget-table-three">
        <div class="widget-heading">
            <h5 class="">SEND DOCUMENT MANUAL <span class="badge badge-success">NEW FEATURE</span></h5>
        </div>
        <div class="widget-content">
            <?= form_open('send-doc', 'method="post" id="formSendDoc"') ?>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group d-block text-left">
                        <label class="d-block">Transaction ID</label>
                        <input type="text" class="form-control" name="transid" placeholder="ex: T122236488922" value="<?= $receipt ?>" required>
                        <small class="text-danger">Get from responses email (Payment Confirmation) ipay88</small>
                    </div>
                </div>
                <div class="col-sm-2">
                    <label class="d-block" style="color: transparent">x</label>
                    <button type="submit" class="btn btn-primary btn-lg" id="btnCheck"><i class="fas fa-search mr-2"></i>Check</button>
                </div>
            </div>
            <?= form_close() ?>
            <?php
            if ($receipt !== null) {
                echo '<script type="text/javascript">document.getElementById("btnCheck").click();</script>';
            }
            ?>
            <div id="message"></div>
            <?php
            $transid = $this->input->post('transid');
            if ($transid !== null && $transid !== '') {
                $cek = $this->db->query("SELECT COUNT(id) as hitung FROM `trx_ticket` where receipt_id = '$transid'")->row();
                if ($cek->hitung == '0') {
                    echo '<div class="alert alert-danger mb-4" role="alert"><strong>Please,</strong> process & save by Transaction ID first !!</button>
                </div>';
                    echo form_open('insert-ticket', 'method="post"');
                    echo '
                    <div class="card mt-4" style="background-color: #4b4b4b;">
                        <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group d-block text-left">
                                            <label class="d-block">Transaction ID</label>
                                            <input type="text" class="form-control" name="receipt_id" placeholder="ex: T122236488922">
                                            <small style="color: #fff;">Get from responses email (Payment Confirmation) ipay88</small>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group d-block text-left">
                                            <label class="d-block">Order ID</label>
                                            <input type="text" class="form-control" name="order_id" placeholder="ex: #XAC2B9...">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="d-block" style="color: transparent">x</label>
                                        <button type="submit" class="btn btn-warning btn-lg"><i class="fas fa-paper-plane mr-2"></i>Process & Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>';
                    echo form_close();
                } else {
                    $bill = $this->db->query("SELECT * FROM `trx_ticket` where receipt_id = '$transid' limit 1")->row();
                    // echo form_open('#', 'method="post"');
                    echo '
                    <div class="card mt-4" style="background-color: #4b4b4b;">
                        <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group d-block text-left">
                                            <label class="d-block">Transaction ID</label>
                                            <input type="text" class="form-control" name="receipt_id" id="transid" placeholder="ex: T122236488922" value="' . $transid . '" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group d-block text-left">
                                            <label class="d-block">Order ID</label>
                                            <input type="text" class="form-control" name="order_id" id="orderid" placeholder="ex: #XAC2B9..." value="' . $bill->billing_id . '" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <label class="d-block" style="color: transparent">x</label>
                                        <button type="submit" class="btn btn-warning btn-lg" onclick="return eticket()"><i class="fas fa-ticket-alt mr-2"></i>Send Ticket</button>
                                        <button type="submit" class="btn btn-warning btn-lg ml-1" onclick="return invoice()"><i class="fas fa-paper-plane mr-2"></i>Send Invoice</button>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <h5>Climber Details :</h5>
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead> 
                                                    <tr class="text-center">
                                                        <th>NO.</th>
                                                        <th>NAME</th>
                                                        <th>DATE TO GO</thclass=>
                                                        <th>ID NUMBER</th>
                                                        <th>EMAIL</th>
                                                        <th>NATIONALITY</th>
                                                    </tr>
                                                </thead>
                                                <tbody>';
                    $get =  $this->db->query("SELECT
                                                                        b.destname,
                                                                        a.* 
                                                                    FROM
                                                                        `trx_ticket` a
                                                                        LEFT JOIN stp_dest b ON a.destid = b.destid 
                                                                    WHERE
                                                                        a.receipt_id = '$transid'");
                    echo '<tr>
                                                    <td colspan="6" align="center"><strong>' . $get->row()->destname . '</strong></td>
                                            </tr>';
                    $no = 1;
                    foreach ($get->result() as $rc) {
                        $date = date_create($rc->ticketdatefrom);
                        echo '<tr>
                                                        <td width="20" align="center">' . $no++ . '</td>
                                                        <td>' . $rc->visitor_name . '</td>
                                                        <td align="center">' . date_format($date, 'Y-m-d') . '</td>
                                                        <td>' . $rc->id_number . '</td>
                                                        <td>' . $rc->email . '</td>
                                                        <td>' . $rc->nationality . '</td>
                                                </tr>';
                    }
                    echo '</tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>';
                    // echo form_close();
                }
            }
            ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    function eticket() {
        var transid = document.getElementById('transid').value;
        var bill_id = document.getElementById('orderid').value;
        var message = document.getElementById('message');

        let formData = new FormData();
        formData.append('transid', transid);

        try {
            fetch('https://booking.sabahparks.org.my/e-ticket-backend.php', {
                    method: "POST",
                    body: formData,
                })
                .then(
                    message.innerHTML = '<div class="alert alert-success text-center" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button><b>Success</b> generate ticket <b>' + transid + '</b>.<br>Please wait for the delivery process and check the destination email on Inbox,Spam,etc.</div>'
                )
                .then(
                    window.open(
                        "https://booking.sabahparks.org.my/qrcode/" + transid + ".pdf", "_blank"));
        } catch (error) {
            console.log(error);
        }
    }

    function invoice() {
        var transid = document.getElementById('transid').value;
        var bill_id = document.getElementById('orderid').value;
        var message = document.getElementById('message');

        let formData = new FormData();
        formData.append('transid', transid);
        formData.append('bill', bill_id);

        try {
            fetch('https://booking.sabahparks.org.my/invoice-backend.php', {
                    method: "POST",
                    body: formData,
                })
                .then(
                    message.innerHTML = '<div class="alert alert-success text-center" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button><b>Success</b> send Invoice number Booking Ref <b>' + bill_id + '</b><br>Please wait for the delivery process and check the destination email on Inbox,Spam,etc.</div>'
                )

                .then(console.log('success 2'));
        } catch (error) {
            console.log(error);
        }
    }
</script>