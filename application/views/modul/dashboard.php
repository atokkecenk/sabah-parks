﻿<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <div class="row mb-2">
        <div class="col-md-6">
            <div class="my-note c1">
                <p>
                    <b>ATTENTION : </b><br><br>
                    <b>1.</b> First, Please <i class="far fa-trash-alt" style="color: #fffa00;"></i> clear cache your browser<br>
                    <b>2.</b> This one <i class="far fa-newspaper" style="color: #fffa00;"></i> article relate for tutorial clear cache on browser ( <a href="https://its.uiowa.edu/support/article/719" target="_blank" title="Read this article">Read!!</a> )<br>
                    <b>3.</b> After <i class="far fa-check-circle" style="color: #fffa00;"></i> successfully clear cache, reload/refresh page<br>
                    <b>4.</b> Done, enjoyed use the system
                </p>
                <p style="text-align: end;font-size: 11px;font-style: italic;">- Developer Team<br></p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="my-note c2">
                <p>
                    <b>UPDATE SYSTEM : </b>
                </p>
                <?php
                $get = $this->db->query("SELECT
                                        IF
                                            ( b.id IS NOT NULL, 'LATEST', NULL ) AS `status`,
                                            a.*,
                                            DATE_FORMAT( a.tanggal, '%Y %b %d, %H:%i %p' ) AS ntime 
                                        FROM
                                            tb_update_info a
                                            LEFT JOIN ( SELECT MAX( id ) AS id FROM tb_update_info ) b ON a.id = b.id 
                                        WHERE
                                            a.`active` = 'Y' 
                                        ORDER BY
                                            a.`tanggal` DESC")->result();
                foreach ($get as $val) {
                    $lts = '';
                    if ($val->status == 'LATEST') {
                        $lts = '<span class="latest ml-2">LATEST</span>';
                    }
                    echo '<p>' . $val->update . ' / ' . $val->ntime . $lts . '</p>';
                }
                ?>
            </div>
        </div>
    </div>

    <div class="widget widget-table-one">
        <div class="widget-heading">
            <h5 class="">Dashboard</h5>
        </div>

        <div class="widget-content">
            <div class="row">
                <div class="col-sm-4">
                    <div class="transactions-list mb-2">
                        <div class="t-item">
                            <div class="t-company-name">
                                <div class="t-icon">
                                    <div class="icon">
                                        <i class="fas fa-users"></i>
                                    </div>
                                </div>
                                <div class="t-name">
                                    <h4>Member registered</h4>
                                    <p class="meta-date"><a href="r-member" class="text-primary">&rarr; Info</a></p>
                                </div>

                            </div>
                            <div class="t-rate">
                                <h4 style="font-weight: bold;"><?= number_format($jml_member, 0) ?></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="transactions-list mb-2">
                        <div class="t-item">
                            <div class="t-company-name">
                                <div class="t-icon">
                                    <div class="icon">
                                        <i class="fas fa-money-bill"></i>
                                    </div>
                                </div>
                                <div class="t-name">
                                    <h4>Sales (order-payment)</h4>
                                    <p class="meta-date"><a href="r-sales" class="text-primary">&rarr; Info</a></p>
                                </div>
                            </div>
                            <div class="t-rate">
                                <h4 style="font-weight: bold;"><?= number_format($jml_sales, 0) ?></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="transactions-list mb-2">
                        <div class="t-item">
                            <div class="t-company-name">
                                <div class="t-icon">
                                    <div class="icon">
                                        <i class="fas fa-ticket-alt"></i>
                                    </div>
                                </div>
                                <div class="t-name">
                                    <h4>Ticket released</h4>
                                    <p class="meta-date"><a href="ticket" class="text-primary">&rarr; Info</a></p>
                                </div>

                            </div>
                            <div class="t-rate">
                                <h4 style="font-weight: bold;"><?= number_format($jml_ticket, 0) ?></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
                Tooltip on top
            </button> -->
            <!-- <input id="rangeCalendarFlatpickr" class="form-control flatpickr flatpickr-input active" type="text" placeholder="Select Date.." readonly="readonly"> -->
        </div>
    </div>



    <!--Start of Tawk.to Script-->
    <!-- <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/6272855e7b967b11798dc210/1g27lhuo2';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script> -->
    <!--End of Tawk.to Script-->
</div>

<div class="col-xl-6 col-lg-6 col-sm-6  layout-spacing">
    <div class="widget widget-table-one">
        <div class="widget-content">
            <center>
                <h5 class="mb-3">Graph of Destination comparison</h5>
            </center>
            <div style="height: 276.8px;">
                <canvas id="barChart" width="200" height="200"></canvas>
            </div>
        </div>
    </div>
</div>

<div class="col-xl-6 col-lg-6 col-sm-6  layout-spacing">
    <div class="widget widget-table-one">
        <div class="widget-content">
            <center>
                <h5 class="mb-3">Graph of Sales growth</h5>
            </center>
            <canvas id="lineChart"></canvas>
        </div>
    </div>
</div>

<div class="col-xl-6 col-lg-6 col-sm-6  layout-spacing">
    <div class="widget widget-table-one">
        <div class="widget-content">
            <center>
                <h5 class="mb-3">Graph of Order vs payment</h5>
            </center>
            <div style="width: 276.8px;height: 276.8px;margin: 0 auto;">
                <canvas id="pieChart" width="276.8" height="276.8"></canvas>
            </div>
        </div>
    </div>
</div>

<div class="col-xl-6 col-lg-6 col-sm-6  layout-spacing">
    <div class="widget widget-table-one">
        <div class="widget-content">
            <center>
                <h5 class="mb-3">Graph of Visitor comparison</h5>
            </center>
            <canvas id="visitorChart"></canvas>
        </div>
    </div>
</div>


<script>
    var chartColors = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(75, 192, 192)',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(231,233,237)'
    };

    const ctx = document.getElementById("barChart");
    const myChart = new Chart(ctx, {
        type: "bar",
        data: {
            labels: [<?= $destname ?>],
            datasets: [{
                // barPercentage: 0.1,
                // barThickness: 6,
                // maxBarThickness: 8,
                // minBarLength: 5,
                label: "Summary Qty, Group by Destination Type",
                data: [<?= $jumlah ?>],
                backgroundColor: [
                    chartColors.yellow,
                    // "rgba(255, 99, 132, 0.2)",
                    // "rgba(54, 162, 235, 0.2)",
                    // "rgba(255, 206, 86, 0.2)",
                ],
                borderColor: [
                    chartColors.yellow,
                    // "rgba(255, 99, 132, 1)",
                    // "rgba(54, 162, 235, 1)",
                    // "rgba(255, 206, 86, 1)",
                ],
                borderWidth: 1,
            }, ],
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true,
                },
            },
            maintainAspectRatio: false,
        },
    });

    // line chart
    // var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var MONTHS = [<?= $labels ?>];
    var config = {
        type: 'line',
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            datasets: [{
                label: "2021",
                backgroundColor: chartColors.red,
                borderColor: chartColors.red,
                fill: false,
                tension: 0.2,
                data: [<?= $before ?>],
                fill: false,
            }, {
                label: "2022",
                backgroundColor: chartColors.blue,
                borderColor: chartColors.blue,
                fill: false,
                tension: 0.2,
                data: [<?= $current ?>],
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Chart.js Line Chart'
            },
            tooltips: {
                mode: 'label',
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Month'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]
            }
        }
    };

    const lineChart = document.getElementById("lineChart").getContext("2d");
    window.myLine = new Chart(lineChart, config);
    // line chart

    // PIE CHART
    const data = {
        labels: [<?= $labels_pie ?>],
        datasets: [{
            label: 'My First Dataset',
            data: [<?= $order ?>],
            backgroundColor: [
                chartColors.red,
                chartColors.blue,
            ],
            hoverOffset: 4
        }],
        options: {
            legend: {
                display: false // <- the important part
            },
            // responsive: true,
            maintainAspectRatio: false,
            // weight: 1,
        },
    };
    const cfg = {
        type: 'pie',
        data: data,
    };

    const zz = document.getElementById("pieChart").getContext("2d");
    window.myLine = new Chart(zz, cfg);
    // PIE CHART


    // visitor chart
    var MONTHS = [<?= $vs_labels ?>];
    var config = {
        type: 'line',
        data: {
            labels: [<?= $vs_labels ?>],
            datasets: [{
                label: "Visitors",
                backgroundColor: chartColors.purple,
                borderColor: chartColors.purple,
                fill: false,
                tension: 0.2,
                data: [<?= $vs_data ?>],
                fill: false,
            }, ]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Chart.js Line Chart'
            },
            tooltips: {
                mode: 'label',
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Month'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]
            }
        }
    };

    const visitorChart = document.getElementById("visitorChart").getContext("2d");
    window.myLine = new Chart(visitorChart, config);
    // visitor chart
</script>